
### Composite

#### When to use

To have a hierarchical collection of primitive and composite entities.
To create a structure in a way that the objects in the structure can be treated the same way.

#### Intent

Compose objects into tree structures to represent part-whole hierarchies. Composite lets clients treat individual objects and compositions of objects uniformly.

#### Components

An interface for all objects in the composition
A leaf element which is the building block of the composition
A composite element which can contain leaf elements and/or composites

#### Implementation

Typical example is the File system which contains directories and files. A directory can contain files or sub directories but both have to be handled in the same way.
In the following example, boxes and products are implemented using the Composite pattern. A box can contain many products and boxes. We need a common way to interact with both entities.

1. Create a common interface which declares a method to print the item.

```java
package com.jaypeesoft.dp.composite;

public interface Item {

  public void print(int level);

}
```

2. Create the leaf object that implements the interface.

```java
package com.jaypeesoft.dp.composite;

public class Product implements Item {

  int id;

  public Product(int id) {
    this.id = id;
  }

  public void print(int level) {
    for (int i = 0; i < level; i++)
      System.out.print("   ");
    System.out.println("Product" + id);
  }
}
```

3. Create the composite object that implements the interface.

```java
package com.jaypeesoft.dp.composite;

import java.util.ArrayList;
import java.util.List;

public class Box implements Item {

  int id;

  private List items = new ArrayList();

  public Box(int id) {
    this.id = id;
  }

  public void print(int level) {
    for (int i = 0; i < level; i++)
      System.out.print("   ");
    System.out.println("Box" + id);
    for (Item item : items) {
      item.print(level + 1);
    }
  }

  public void add(Item item) {
    items.add(item);
  }

  public void remove(Item item) {
    items.remove(item);
  }
}
```

4. The client code. The client creates leaf elements and composite elements and interact with them using the common interface.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.composite.Box;
import com.jaypeesoft.dp.composite.Product;

public class CompositeClient {

  public static void main(String[] args) {

    // Initialize four products
    Product product1 = new Product(1);
    Product product2 = new Product(2);
    Product product3 = new Product(3);
    Product product4 = new Product(4);

    // Initialize three boxes
    Box box1 = new Box(1);
    Box box2 = new Box(2);
    Box box3 = new Box(3);

    // Put 3 items in box1
    box1.add(product1);
    box1.add(product2);
    box1.add(product3);

    // Put item4 in box2
    box2.add(product4);

    // Put box1 and box2 in box3
    box3.add(box1);
    box3.add(box2);

    // Print the contents of box3
    box3.print(0);
  }
}
```

Output

```
Box3
   Box1
      Product1
      Product2
      Product3
    Box2
        Product4
```

#### Benefits

Simplifies the representation of part-whole hierarchies.
Clients can treat all objects in the composite structure uniformly.

#### Drawbacks

Strict restrictions need to be enforced otherwise the tree structure may become overly generalized.

#### Real World Examples

Organization structure with manager and reportees. The reportees could be managers who may have their own reportees.

#### Software Examples

File system (directories and files)

#### Java SDK Examples

java.awt Container#add(Component) (practically all over Swing thus)
javax.faces.component UIComponent#getChildren() (practically all over JSF UI thus)
