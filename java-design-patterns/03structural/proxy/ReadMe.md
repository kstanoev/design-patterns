
### Proxy

#### When to use

To provide controlled access to a sensitive master object
To provide a local reference to a remote object
To improve performance when an object needs to be accessed frequently

#### Intent

Provide a surrogate or placeholder for another object to control access to it.

#### Implementation

Let us see the example of a Proxy Server. A proxy server, also known as a "proxy" or "application-level gateway", is a computer that acts as a gateway between a local network (e.g., all the computers at one company or in one building) and a larger-scale network such as the Internet. Proxy servers provide increased performance and security. A proxy server can act as an intermediary between the user's computer and the Internet to prevent from attack and unexpected access.
A proxy server will behave just like the real server and implements the same interface.

1. Create an interface. Both the real subject and the proxy subject will implement this interface.

```java
package com.jaypeesoft.dp.proxy;

public interface Server {
  public void authenticate();
  public void get();
  public void post();
  public void put();
  public void delete();
  public void logout();
}
```

2. Implement the real subject and the proxy subject. Note that the RealServer class is protected so that the client cannot access it directly.

```java
package com.jaypeesoft.dp.proxy;

class RealServer implements Server{

  public void authenticate() {
    System.out.println("Logged into the Real Server");
  }

  public void get() {
    System.out.println("GET command executed");
  }

  public void post() {
    System.out.println("POST command executed");
  }

  public void put() {
    System.out.println("PUT command executed");
  }

  public void delete() {
    System.out.println("DELETE command executed");
  }

  public void logout() {
    System.out.println("Logged out from the Real Server");
  }
}
```

```java
package com.jaypeesoft.dp.proxy;

public class ProxyServer implements Server{

  /* Reference to RealServer */
  private RealServer realServer;
  private boolean sessionActive;

  public ProxyServer() {
    this.realServer = new RealServer();
    sessionActive = false;
  }

  public void authenticate() {

    /* Get the user credentials and login */
    realServer.authenticate();

    /* Track the session */
    sessionActive = true;

  }

  public void get() {

    if(sessionActive)
      realServer.get();
    else
      System.out.println("Invalid Session");

  }

  public void post() {

    if(sessionActive)
      realServer.post();
    else
      System.out.println("Invalid Session");

  }

  public void put() {

    if(sessionActive)
      realServer.put();
    else
      System.out.println("Invalid Session");

  }

  public void delete() {

    if(sessionActive)
      realServer.delete();
    else
      System.out.println("Invalid Session");

  }

  public void logout() {

    realServer.logout();

    /* Deactivate the session */
    sessionActive = false;

  }

}
```

3. The client code. The client does not have access to the real subject. Every request is performed via a 'surrogate' object called proxy.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.proxy.ProxyServer;
import com.jaypeesoft.dp.proxy.Server;

public class ProxyClient {
  public static void main(String[] args) {

    /* Client can access only the proxy server*/
    Server server = new ProxyServer();

    /* Client works with the same interface */
    server.authenticate();
    server.get();
    server.post();
    server.put();
    server.delete();
    server.logout();

  }
}
```

Output

```
Logged into the Real Server
GET command executed
POST command executed
PUT command executed
DELETE command executed
Logged out from the Real Server
```

#### Benefits

A proxy can mask the life-cycle and state of a volatile resource from its client.
Easy to manage the access to the real subject.
Proxies can help in creating objects on-demand.

#### Drawbacks

Performance may be impacted due to the extra level of indirection.

#### Real World Examples

Agents with Power of Attorney
Representatives/Brokers

#### Software Examples

SSH client

#### Java SDK Examples

java.lang.reflect.Proxy
java.rmi.\*
javax.ejb.EJB
javax.inject.Inject
javax.persistence.PersistenceContext
