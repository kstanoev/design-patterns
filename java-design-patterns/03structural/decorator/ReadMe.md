
### Decorator

#### When to use

To dynamically change the functionality of an object at runtime without impacting the existing functionality of the objects.
To add functionalities that may be withdrawn later.
To combine multiple functionalities where it is impractical to create a subclass for every possible combination.

#### Intent

Attach additional responsibilities to an object dynamically. Decorators provide a flexible alternative to subclassing for extending functionality.

#### Problem

Assume that you need to prepare a pizza that may have multiple combinations of toppings, cooking type, etc. Though this can be achieved by Inheritance, it is not practical to create subclasses for every possible combination. Alternatively you can use Composition and add the required functionalities. Since all concrete implementations conform to the same interface, we can mix and match any number of classes to create a variety of combinations.

#### Implementation

1. Define Item interface and create a Pizza class which is an implementation of the Item interface.

```java
package com.jaypeesoft.dp.decorator;

public interface Item {
  void prepare();
}
```

```java
package com.jaypeesoft.dp.decorator;

public class Pizza implements Item {
  public void prepare() {
      System.out.print("Pizza");
  }
}
```

2. Create an abstract Decorator class which implements the item interface. This class contains the Item object which will be decorated with new functionalities.

```java
package com.jaypeesoft.dp.decorator;

abstract class PizzaDecorator implements Item {
  private Item pizza;

  public PizzaDecorator(Item item) {
    pizza = item;
  }

  public void prepare() {
    pizza.prepare();
  }
}
```

3. Create Concrete Decorators.

```java
package com.jaypeesoft.dp.decorator;

public class DeepFried extends PizzaDecorator {
  public DeepFried(Item inner) {
      super(inner);
  }

  public void prepare() {
      super.prepare();
      System.out.print(" + Deep Fried");
  }

}
```

```java
package com.jaypeesoft.dp.decorator;

public class DoubleCheese extends PizzaDecorator {
  public DoubleCheese(Item inner) {
    super(inner);
  }

  public void prepare() {
    super.prepare();
    System.out.print(" + Double Cheese");
  }

}
```

```java
package com.jaypeesoft.dp.decorator;

public class Spicy extends PizzaDecorator {
  public Spicy(Item inner) {
      super(inner);
  }

  public void prepare()  {
      super.prepare();
      System.out.print(" + Spicy");
  }

}
```

4. The Client code. The Decorator Pattern allows one to mix and match without needing to create a rigid hierarchy.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.decorator.DeepFried;
import com.jaypeesoft.dp.decorator.DoubleCheese;
import com.jaypeesoft.dp.decorator.Item;
import com.jaypeesoft.dp.decorator.Pizza;
import com.jaypeesoft.dp.decorator.Spicy;

public class DecoratorClient {
  public static void main( String[] args ) {
      Item[] order = {
          new DeepFried(new Pizza()),
          new DeepFried(new DoubleCheese(new Pizza())),
          new DoubleCheese(new Spicy(new DeepFried(new Pizza())))
        };
      for (Item item : order) {
          item.prepare();
          System.out.println("  ");
      }
  }
}
```

Output

```
Pizza + Deep Fried
Pizza + Double Cheese + Deep Fried
Pizza + Deep Fried + Spicy + Double Cheese
```

#### Benefits

Decorator allows us to mix and match features instead of creating concrete implementations for all possible combinations.
Decorator allows us to modify an object in a much more modular and less fundamental way than inheritance would.
New functionalities can be easily supported.

#### Drawbacks

Multiple small objects are created in the process of creating an object.
Complexity is increased.

#### Programming Examples

File Stream implementations

#### Java SDK Examples

All subclasses of java.io.InputStream, OutputStream, Reader and Writer have a constructor taking an instance of same type.
java.util.Collections, the checkedXXX(), synchronizedXXX() and unmodifiableXXX() methods.
javax.servlet.http.HttpServletRequestWrapper and HttpServletResponseWrapper
