
### Prototype

#### When to use

To improve the performance when object creation is costly and time consuming.
To simplify and optimize multiple objects creation that will have mostly the same data

#### Intent

Specify the kinds of objects to create using a prototypical instance, and create new objects by copying this prototype.

#### Components

A class that implements Cloneable interface (public)

#### Implementation

Most of the cloud platforms behave like Prototype pattern to create instances quickly upon requests. This is achieved with the help of pre-installed Machine Images. When a customer wants a Windows or Linux instance, the cloud software just loads the already created machine image on to a server hardware (rather than going through the complete installation process). This saves a lot of time.

1. Create a base machine image by installing the specified OS and antivirus software. Provide a 'clone' method so that client can create an object without using a time consuming 'new' operator every time.

```java
package com.jaypeesoft.dp.prototype;

public class MachineImage implements Cloneable {

  StringBuilder image;

  public MachineImage(String os, String antivirusSW) {
    image = new StringBuilder();
    image.append(os)
         .append(" + " + antivirusSW);
  }

  private MachineImage(String sw) {
    image = new StringBuilder(sw);
  }

  public void install(String sw) {
    image.append(sw);
  }

  public void printSw() {
    System.out.println(image);
  }

  @Override
  public MachineImage clone()
      throws CloneNotSupportedException {
    return new MachineImage(this.image.toString());
  }
}
```

2. The client code. The client creates a base image and clones it to create other images.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.prototype.MachineImage;

public class PrototypeClient {
  public static void main(String [] args)
      throws CloneNotSupportedException {

    /* Create base Machine Images */
    MachineImage linuxVM= new MachineImage("Linux", "Symantec");
    MachineImage windowsVM = new MachineImage("Windows", "MaAfee");

    /* Clone Linux VM and Install Web Server */
    MachineImage webServer= linuxVM.clone();
    webServer.install(" + Web Server S/W");

    /* Create a copy of the Web Server and
     * install Application server on top of it */
    MachineImage webAppServer = webServer.clone();
    webAppServer.install(" + App Server S/W");

    /* Clone Linux VM and Install DB Server */
    MachineImage dbServer= linuxVM.clone();
    dbServer.install(" + Database Server S/W");

    /* Create a test machine from Windows Image */
    MachineImage testMachine= windowsVM.clone();

    System.out.print("Web Server Configuration: ");
    webServer.printSw();
    System.out.print("App Server Configuration: ");
    webAppServer.printSw();
    System.out.print("DB Server Configuration: ");
    dbServer.printSw();
    System.out.print("Test Machine Configuration: ");
    testMachine.printSw();
  }
}
```

Output

```
Web Server Configuration: Linux OS + Antivirus S/W + Web Server S/W
App Server Configuration: Linux OS + Antivirus S/W + Web Server S/W + App Server S/W
DB Server Configuration: Linux OS + Antivirus S/W + Database Server S/W
Test Machine Configuration: Linux OS + Antivirus S/W
```

#### Benefits

Performance: Cloning (using MemberwiseClone) is considerably less expensive than creating a new object afresh (with new operator).
Objects can be cloned very dynamically, without any insistence on up-front instantiation. The first created object can be created at any time in the application execution, and further duplication can take place at any time ahead.

#### Drawbacks

Deep copy has to be handled carefully.

#### Real World Examples

Biological Cell splitting

#### Software Examples

Virtual Machine Images - Have one image per OS which has all the required s/w installed.
DVD duplication - Duplication of the master dvd to create several copies.

#### Java SDK Examples

java.lang.Object clone()
