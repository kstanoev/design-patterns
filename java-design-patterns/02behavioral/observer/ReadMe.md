
### Observer

#### When to use

When there is one to many relationship between objects such as if one object is modified, its dependent objects are to be notified automatically and corresponding changes are done to all dependent objects.

#### Intent

Define a one-to-many dependency between objects so that when one object changes state, all its dependents are notified and updated automatically.

#### Implementation

Consider a simple Point Of Sale (POS) system which has many peripherals attached to it. Each peripheral(observer) registers itself to the interested topics(subjects) so that it can update its status when that topic/event occurs. For e.g., when an item or payment is added to the order, the cashier display and customer display need to be updated with the new details. Similarly when the order is completed, the receipt printer will act on it by printing the receipt.

1. Create an observer interface and its concrete implementations. These observers subscribe to specific topics(subjects) so that it can display the updates.

```java
package com.jaypeesoft.dp.observer;

public abstract class Observer {
  public abstract void update(String str);
}
```

```java
package com.jaypeesoft.dp.observer;

public class CustomerDisplay extends Observer {

  public void update(String str) {
    System.out.println("[CustomerDisplay] " + str);
  }

}
```

```java
package com.jaypeesoft.dp.observer;

public class CashierDisplay extends Observer {

  public void update(String str) {
    System.out.print("[CashierDisplay] " + str);
  }

}
```

2. Create an interface for the subject and its concrete implementations. These subjects contain lists to keep track of subscribed observers that need to be notified.

```java
package com.jaypeesoft.dp.observer;

public interface Topic {
  public void register(Observer obj);
  public void notifyObservers(String line);
}
```

```java
package com.jaypeesoft.dp.observer;

import java.util.ArrayList;
import java.util.List;

public class AddItemTopic implements Topic {

  List addItemObservers = new ArrayList();

  public void notifyObservers(String line) {
    for(Observer o : addItemObservers) {
      o.update(line);
    }
  }

  public void register(Observer o) {
    addItemObservers.add(o);
  }

}
```

```java
package com.jaypeesoft.dp.observer;

import java.util.ArrayList;
import java.util.List;

public class AddPaymentTopic implements Topic {

  List addPaymentObservers = new ArrayList();

  public void notifyObservers(String line) {
    for(Observer o : addPaymentObservers) {
      o.update(line);
    }
  }

  public void register(Observer o) {
    addPaymentObservers.add(o);
  }

}
```

```java
package com.jaypeesoft.dp.observer;

import java.util.ArrayList;
import java.util.List;

public class CompleteOrderTopic implements Topic {

  List orderCompletedObservers = new ArrayList();

  public void notifyObservers(String line) {
    for(Observer o : orderCompletedObservers) {
      o.update(line);
    }
  }

  public void register(Observer o) {
    orderCompletedObservers.add(o);
  }

}
```

3. Create Item, Payment, and Order classes. The observers subscribe to various subjects(topics) in the Order class constructor.

```java
package com.jaypeesoft.dp.observer;

public class Item {
  public final String name;
  public final double price;

  public Item(String name, double price) {
    this.name = name;
    this.price = price;
  }

}
```

```java
package com.jaypeesoft.dp.observer;

public class Payment {

  public final String type;
  public final double amount;

  public Payment(String type, double amount) {
    super();
    this.type = type;
    this.amount = amount;
  }

}
```

```java
package com.jaypeesoft.dp.observer;

import java.util.ArrayList;
import java.util.List;

public class Order {
  List cart = new ArrayList();
  List payments = new ArrayList();

  private Topic addItemTopic;
  private Topic addPaymentTopic;
  private Topic completeOrderTopic;

  public Order() {
    // create observers (devices)
    Observer cashierDisplay = new CashierDisplay();
    Observer customerDisplay = new CustomerDisplay();

    // create subjects (events)
    addItemTopic = new AddItemTopic();
    addPaymentTopic = new AddPaymentTopic();
    completeOrderTopic = new CompleteOrderTopic();

    // Cashier display subscribed to all topics
    addItemTopic.register(cashierDisplay);
    addPaymentTopic.register(cashierDisplay);
    completeOrderTopic.register(cashierDisplay);

    // Customer display subscribed to all topics
    addItemTopic.register(customerDisplay);
    addPaymentTopic.register(customerDisplay);
    completeOrderTopic.register(customerDisplay);
  }

  public void addItem(Item item) {
    cart.add(item);
    String line = item.name + " $" + item.price;
    addItemTopic.notifyObservers(line);
  }

  public void makePayment(Payment payment) {
    payments.add(payment);
    String line = payment.type + " $" + payment.amount;
    addPaymentTopic.notifyObservers(line);

  }

  public void completeOrder() {
    String line = "Order completed";
    completeOrderTopic.notifyObservers(line);
  }

}
```

4. The client code. When an item or payment is added to the order, the corresponding topic notifies its observers.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.observer.Item;
import com.jaypeesoft.dp.observer.Order;
import com.jaypeesoft.dp.observer.Payment;

public class ObserverClient {

  public static void main(String[] args) {

    /* Create an order and add items */
    Order order = new Order();
    order.addItem(new Item("Pizza", 6.99));
    order.addItem(new Item("Wine", 9.99));
    order.addItem(new Item("Beer", 5.99));
    order.addItem(new Item("Apple", 1.49));

    System.out.println("---------------------------------");

    /* Create payments and make payments */
    order.makePayment(new Payment("CASH", 20.00));
    order.makePayment(new Payment("CREDIT", 10.00));
    order.makePayment(new Payment("DEBIT", 10.00));
    System.out.println("---------------------------------");

    /* Complete the order */
    order.completeOrder();
  }

}
```

Output

```
[CashierDisplay] Pizza  $6.99	[CustomerDisplay] Pizza  $6.99
[CashierDisplay] Wine   $9.99	[CustomerDisplay] Wine   $9.99
[CashierDisplay] Beer   $5.99	[CustomerDisplay] Beer   $5.99
[CashierDisplay] Apple  $1.49	[CustomerDisplay] Apple  $1.49
[CashierDisplay] CASH 	$20.0	[CustomerDisplay] CASH   $20.0
[CashierDisplay] CREDIT $10.0	[CustomerDisplay] CREDIT $10.0
[CashierDisplay] DEBIT 	$10.0	[CustomerDisplay] DEBIT  $10.0
[CashierDisplay] Order completed [CustomerDisplay] Order completed
```

#### Benefits

Loose coupling between Subject and Observer allows you vary subjects and observers independently.
Supports broadcast communication.

#### Real World Examples

Auction - The bidders act as observers and raise the paddle to accept the bid. When the bid is accepted, the others are notified by the auctioneer.

#### Java SDK Examples

java.util.Observer, java.util.Observable (rarely used in real world though)
All implementations of java.util.EventListener (practically all over Swing thus)
javax.servlet.http HttpSessionBindingListener
javax.servlet.http HttpSessionAttributeListener
javax.faces.event PhaseListener
