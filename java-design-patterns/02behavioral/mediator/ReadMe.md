
### Mediator

#### When to use

To facilitate interactions between a set of objects where the communications are complex and hard to maintain.
To have a centralized control for the object interactions.

#### Intent

Define an object that encapsulates how a set of objects interact. Mediator promotes loose coupling by keeping objects from referring to each other explicitly, and it lets you vary their interaction independently.

#### Components

Mediator interface – an interface that defines the communication rules between objects
Concrete mediator – a mediator object which will enables communication between participating objects
Colleagues – objects communicating with each other through mediator object

#### Implementation

Mediator enables decoupling of objects by introducing a layer in between so that the interaction between objects happen via the layer. ATC (Air Traffic Controller) is a perfect example for the mediator design pattern. A typical airport is a complex system that involves complex communications between flights, airport vehicles, and other airport systems. Direct communication between all these participants is error prone and practically impossible. Hence we need a system that helps in communication between flights, airport vehicle and coordinates landing, take-off, etc.

1. Create an interface that defines the communication rules between objects

```java
package com.jaypeesoft.dp.mediator;

public interface AtcMediator {

  public void registerRunway(Runway runway);
  public void registerGate(Gate gate);
  public boolean getLandingPermission(Flight flight);
  public boolean getTakeoffPermission(Flight flight);
  public void enterRunway(Runway runway);
  public void exitRunway(Runway runway);
  public void enterGate(Gate gate);
  public void exitGate(Gate gate);

}
```

2. Create a mediator object which will enables communication between the participating objects.

```java
package com.jaypeesoft.dp.mediator;

import java.util.ArrayList;
import java.util.List;

public class AtcMediatorImpl implements AtcMediator {

  private Runway runway;
  private List gates;

  public AtcMediatorImpl() {
    gates = new ArrayList();
  }

  public void registerRunway(Runway runway) {
    this.runway = runway;
  }

  public void registerGate(Gate gate) {
    gates.add(gate);
  }

  public boolean getLandingPermission(Flight flight) {

    /* Check if the runway is free */
    if (runway.isInUse() == false) {

      /* Find an available gate */
      for (Gate gate : gates) {

        if (gate.isInUse() == false) {

          /* reserve the gate and runway */
          flight.allocateRunway(runway);
          flight.allocateGate(gate);
          return true;

        }

      }

      System.out.println("[ATC Mediator] All gates in use");

    }
    else
      System.out.println("[ATC Mediator] Runway in use");

    return false;
  }

  public boolean getTakeoffPermission(Flight flight) {
    return runway.isInUse() == false;
  }

  public void exitRunway(Runway runway) {
    runway.setInUse(false);
  }

  public void exitGate(Gate gate) {
    gate.setInUse(false);
  }

  public void enterRunway(Runway runway) {
    runway.setInUse(true);
  }

  public void enterGate(Gate gate) {
    gate.setInUse(true);
  }

}
```

3. Define colleagues. Colleagues keep a reference to its Mediator object.

```java
package com.jaypeesoft.dp.mediator;

public class Flight {
  private AtcMediator atc;
  private String flightNum;
  private Runway runway;
  private Gate gate;

  public Flight(AtcMediator atc, String flightNum) {
    this.atc = atc;
    this.flightNum = flightNum;
  }

  public String getName() {
    return flightNum;
  }

  public Runway getRunway() {
    return runway;
  }

  public void allocateRunway(Runway runway) {
    this.runway = runway;
  }

  public Gate getGate() {
    return gate;
  }

  public void allocateGate(Gate gate) {
    this.gate = gate;
  }

  public void landAndTakeOff() throws InterruptedException {

    System.out.println(flightNum + " is requesting landing permission");

    while (false == atc.getLandingPermission(this)) {
      Thread.sleep(1000);
    }
    // Landing permission granted, land now
    land();

    while (false == atc.getTakeoffPermission(this)) {
      Thread.sleep(1000);
    }
    // Take off permission granted, take off now
    takeOff();
  }

  public void land() {

    atc.enterRunway(runway);
    System.out.println(this.flightNum + " is landing, gate is "
        + gate.getGateNum());

    /* Exit the runway after 1 second */
    new java.util.Timer().schedule(new java.util.TimerTask() {
      @Override
      public void run() {
        atc.exitRunway(runway);
        atc.enterGate(gate);
      }
    }, 1000);

  }

  public void takeOff() {

    /* Takeoff after 5 seconds */
    new java.util.Timer().schedule(new java.util.TimerTask() {

      @Override
      public void run() {
        System.out.println(flightNum + " is taking off");
        atc.exitGate(gate);
        atc.enterRunway(runway);

        /* Exit the runway after 1 second */
        new java.util.Timer().schedule(new java.util.TimerTask() {
          @Override
          public void run() {
            atc.exitRunway(runway);
          }
        }, 1000);

      }
    }, 5000);

  }

}
```

```java
package com.jaypeesoft.dp.mediator;

public class Runway {

  private String runwayNum;
  private boolean inUse;

  public Runway(String runwayNum, boolean inUse) {
    this.runwayNum = runwayNum;
    this.inUse = inUse;
  }

  public String getRunwayNum() {
    return runwayNum;
  }

  public void setRunwayNum(String runwayNum) {
    this.runwayNum = runwayNum;
  }

  public boolean isInUse() {
    return inUse;
  }

  public void setInUse(boolean inUse) {
    this.inUse = inUse;
  }

}
```

```java
package com.jaypeesoft.dp.mediator;

public class Gate {

  private String gateNum;
  private boolean inUse;

  public Gate(String gateNum, boolean inUse) {
    this.gateNum = gateNum;
    this.inUse = inUse;
  }

  public String getGateNum() {
    return gateNum;
  }

  public boolean isInUse() {
    return inUse;
  }

  public void setInUse(boolean inUse) {
    this.inUse = inUse;
  }

}
```

4. The client code. The client creates mediator and all the colleagues register with the mediator. When the objects want to interact with other objects, it uses the mediator,

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.mediator.AtcMediator;
import com.jaypeesoft.dp.mediator.AtcMediatorImpl;
import com.jaypeesoft.dp.mediator.Flight;
import com.jaypeesoft.dp.mediator.Gate;
import com.jaypeesoft.dp.mediator.Runway;

public class MediatorClient {

  public static void main(String args[]) throws InterruptedException {

    AtcMediator atcMediator = new AtcMediatorImpl();

    /* Create a runway & register with the mediator */
    atcMediator.registerRunway(new Runway("RW-1", false));

    /* Create gates & register gates with the mediator */
    atcMediator.registerGate(new Gate("G-1", false));
    atcMediator.registerGate(new Gate("G-2", false));
    atcMediator.registerGate(new Gate("G-3", false));

    /* Many flights are arriving and they want to land and take off.
     * They just contact the mediator instead of
     * directly communicating with individual objects. */

    for(int i=0; i<10; i++) {
      Flight flight = new Flight(atcMediator, "F-00"+(i+1));
      flight.landAndTakeOff();
    }

  }
}
```

Output

```
F-001 is requesting landing permission
F-001 is landing, gate is G-1
F-002 is requesting landing permission
F-002 is landing, gate is G-2
F-001 is taking off
F-003 is requesting landing permission
F-003 is landing, gate is G-1
F-004 is requesting landing permission
F-004 is landing, gate is G-3
F-002 is taking off
F-003 is taking off
F-005 is requesting landing permission
[ATC Mediator] Runway in use
F-005 is landing, gate is G-1
F-004 is taking off
F-006 is requesting landing permission
F-006 is landing, gate is G-2
F-005 is taking off
F-007 is requesting landing permission
F-007 is landing, gate is G-1
F-006 is taking off
F-008 is requesting landing permission
F-008 is landing, gate is G-2
F-009 is requesting landing permission
F-009 is landing, gate is G-3
F-007 is taking off
F-0010 is requesting landing permission
F-0010 is landing, gate is G-1
F-008 is taking off
F-009 is taking off
F-0010 is taking off
```

#### Benefits

A mediator promotes loose coupling between colleagues.
Promotes one-to-many relationships that are easier to understand, implement and maintain than many-to-many relationships.
A mediator simplifies the communication and provides a centralized control.

#### Drawbacks

A Mediator class may become complex if not designed carefully.
All communications are routed by the Mediator which may impact the performance.

#### Real World Examples

Air Traffic Controller
Head Speaker of the Parliament - members do not communicate with each other

#### Java SDK Examples

java.util.Timer (all scheduleXXX() methods)
java.util.concurrent.Executor execute()
java.util.concurrent.ExecutorService (the invokeXXX() and submit() methods)
java.util.concurrent.ScheduledExecutorService (all scheduleXXX() methods)
java.lang.reflect.Method invoke()
