
### Strategy

#### When to use

To switch out different implementations for different situations.
To support different variants of the algorithm.

#### Intent

Define a family of algorithms, encapsulate each one, and make them interchangeable. Strategy lets the algorithm vary independently from clients that use it.

#### Implementation

Assume that you want to create a shopping cart application. One of the requirements is to allow different types of payments.

1. Define the interface of an interchangeable family of algorithms and move algorithm implementation details in subclasses.

```java
package com.jaypeesoft.dp.strategy;

public interface PaymentStrategy {

  public void pay();

}

public class CardPayment implements PaymentStrategy {

  public String cardType;
  public String issuer;
  public double amount;

  public CardPayment(String cardType, String issuer,
      double amount) {
    super();
    this.cardType = cardType;
    this.issuer = issuer;
    this.amount = amount;
  }

  @Override
  public void pay() {
    System.out.println(issuer + " " + cardType + " " + amount + "$" );
  }

}

public class CashPayment implements PaymentStrategy {

  public double amount;

  public CashPayment(double amount) {
    this.amount = amount;
  }

  @Override
  public void pay() {
    System.out.println("Cash " + amount + "$" );
  }

}
```

2. Interface method is used to invoke the algorithm.

```java
package com.jaypeesoft.dp.strategy;

public class Item {
  private String name;
  private double price;

  public Item(String name, double price) {
    this.name = name;
    this.price = price;
  }

  public String getName() {
    return name;
  }

  public double getPrice() {
    return price;
  }
}
```

```java
package com.jaypeesoft.dp.strategy;

import java.util.ArrayList;
import java.util.List;

public class Order {
  List cart = new ArrayList();
  List payments =
      new ArrayList();

  private final String FORMAT = "%-20s %s";

  public void addItem(Item item) {
    cart.add(item);
    System.out.println(String.format(FORMAT,
        item.getName(), item.getPrice()));
  }

  public void makePayment(PaymentStrategy pm) {
    payments.add(pm);
    pm.pay();
  }

}
```

3. The client code. The client chooses the algorithm/strategy at runtime.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.strategy.CardPayment;
import com.jaypeesoft.dp.strategy.CashPayment;
import com.jaypeesoft.dp.strategy.Item;
import com.jaypeesoft.dp.strategy.Order;

public class StrategyClient {

  public static void main(String[] args) {

    /* Create an order and add items */
    Order order = new Order();
    order.addItem(new Item("Italian Pizza", 6.99));
    order.addItem(new Item("Wine", 9.99));
    order.addItem(new Item("Beer", 5.99));
    order.addItem(new Item("Red Apple", 1.49));
    order.addItem(new Item("Almonds", 11.99));

    System.out.println("---------------------------------");
    /* Create payment strategies and make payment */
    order.makePayment(new CashPayment(20.00));
    order.makePayment(new CardPayment("CREDIT", "VISA", 10.00));
    order.makePayment(new CardPayment("DEBIT", "AMEX", 10.00));
    System.out.println("---------------------------------");
  }
}
```

Output

```
Italian Pizza         6.99
Wine                  9.99
Beer                  5.99
Red Apple             1.49
Almonds              11.99
Cash 20.0$
VISA CREDIT 10.0$
AMEX DEBIT 10.0$
```

#### Benefits

Too many conditional statements can be avoided with different strategy classes.
Allows runtime selection of algorithms from the same algorithm family.
Improves extensibility with 3rd party implementations of the algorithms.
Improves readability by avoiding too many if/else or switch statements.
Enforces Open/Close principle.

#### Drawbacks

May increase the number of classes as each strategy needs to be defined in its own class.

#### Real World Examples

Choose a game strategy (attack/defend) based on the opponent.

#### Software Examples

Sorting (We want to sort these numbers, but we don't know if we are gonna use BrickSort, BubbleSort or some other sorting)
Validation (We need to check items according to "Some rule", but it's not yet clear what that rule will be, and we may think of new ones.)
Games (We want player to either walk or run when he moves, but maybe in the future, he should also be able to swim, fly, teleport, burrow underground, etc.)
Storing information (We want the application to store information to the Database, but later it may need to be able to save a file, or make a webcall)
Outputting (We need to output X as a plain string, but later may be a CSV, XML, Json, etc.)

#### Java SDK Examples

java.util.Comparator compare(), executed by among others Collections sort().
javax.servlet.http.HttpServlet, the service() and all doXXX() methods take HttpServletRequest and HttpServletResponse and the implementor has to process them
javax.servlet.Filter doFilter()
