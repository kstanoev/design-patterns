
### Interpreter

#### When to use

To easily solve 'repeated' problems in a 'well-defined' domain with the help of a 'language'.

#### Intent

Given a language, define a representation for its grammar along with an interpreter that uses the representation to interpret sentences in the language.

#### Implementation

Here we see an example of evaluating arithmetic expressions using the Interpreter pattern. The representation is defined as POSTFIX and the grammer is defined to interprete different type of expressions. Though each expression is different, they are all constructed using the basic rules that make up the grammar for the language of arithmetic expressions.

1. Create an interface for the basic expression.

```java
package com.jaypeesoft.dp.interpreter;

public interface Exp {

  public int evaluate();

}
```

2. Create concrete expressions including the terminal and non-terminal experessions.

```java
package com.jaypeesoft.dp.interpreter;

public class Number implements Exp {

  private final int n;

  public Number(int n) {
    this.n = n;
  }

  public int evaluate() {
    return n;
  }

}
```

```java
package com.jaypeesoft.dp.interpreter;

public class AddExp implements Exp {
  Exp first;
  Exp second;

  public AddExp(Exp first, Exp second) {
    this.first = first;
    this.second = second;
  }

  public int evaluate() {
    return first.evaluate() + second.evaluate();
  }
}
```

```java
package com.jaypeesoft.dp.interpreter;

public class SubtractExp implements Exp{
  Exp first;
  Exp second;

  public SubtractExp(Exp first, Exp second) {
    this.first = first;
    this.second = second;
  }

  public int evaluate() {
    return first.evaluate() - second.evaluate();
  }
}
```

```java
package com.jaypeesoft.dp.interpreter;

public class MultiplyExp implements Exp {
  Exp first;
  Exp second;

  public MultiplyExp(Exp first, Exp second) {
    this.first = first;
    this.second = second;
  }

  public int evaluate() {
    return first.evaluate() * second.evaluate();
  }
}
```

```java
package com.jaypeesoft.dp.interpreter;

public class DivideExp implements Exp {
  Exp first;
  Exp second;

  public DivideExp(Exp first, Exp second) {
    this.first = first;
    this.second = second;
  }

  public int evaluate() {
    return first.evaluate() / second.evaluate();
  }
}
```

3. The client code. The client uses an abstract syntax tree representing a particular sentence in the language that the grammar defines. The abstract syntax tree is assembled from instances of the NonterminalExpression and TerminalExpression classes. The client then invokes the Interpret operation.

```java
package com.jaypeesoft.dp.client;

import java.util.Stack;

import com.jaypeesoft.dp.interpreter.AddExp;
import com.jaypeesoft.dp.interpreter.DivideExp;
import com.jaypeesoft.dp.interpreter.Exp;
import com.jaypeesoft.dp.interpreter.MultiplyExp;
import com.jaypeesoft.dp.interpreter.Number;
import com.jaypeesoft.dp.interpreter.SubtractExp;

public class InterpreterClient {
  public static void main(String args[]) {

    /* POSTFIX Expression to be evaluated */
    String postfix = "543-2+*";

    /* Operations supported */
    final String OPERATORS = "+-*/";

    /* Stack for the operands */
    Stack stack = new Stack();

    for (char c : postfix.toCharArray()) {
      Exp resultExp;
      if (OPERATORS.indexOf(c) == -1) {
        /* number found, push it onto the stack */
        resultExp = new Number(c - 48);
      } else {
        /*
         * operator found, pop out the last two operands from the stack and
         * perform the operation
         */
        Exp right = stack.pop();
        Exp left = stack.pop();

        switch (c) {

        case '+':
          resultExp = new AddExp(left, right);
          break;
        case '-':
          resultExp = new SubtractExp(left, right);
          break;
        case '*':
          resultExp = new MultiplyExp(left, right);
          break;
        case '/':
          resultExp = new DivideExp(left, right);
          break;
        default:
          resultExp = new Number(0);
        }
      }
      /* push the result onto the stack */
      stack.push(new Number(resultExp.evaluate()));
    }
    System.out.println("Result: " + stack.pop().evaluate());
  }
}
```

Output

```
Result: 15
```

#### Benefits

Grammars can be easily modified or extended by inheritance.
Expressions can be interpreted in new ways by adding new operations to the expression.

#### Drawbacks

Grammars containing many rules (i.e many rule classes) can be hard to manage and maintain.

#### Real World Examples

Language Interpreter/Translator

#### Software Examples

Software compiler
SQL evaluation engine
Graphing calculator input parser
XML parser

#### Java SDK Examples

java.util.Pattern
java.text.Normalizer
All subclasses of java.text.Format
All subclasses of javax.el.ELResolver
