
### Command

#### When to use

When the executor of the command does not need to know anything at all about what the command is, what context information it needs on or what it does.
To register a callback when some event is triggered.

#### Intent

Encapsulate a request as an object, thereby letting you parameterize clients with different requests, queue or log requests, and support undoable operations.

#### Implementation

Consider a restaurant scenario. A customer doesn't give the order directly to the cooks. A waiter takes the order to the kitchen where a designated cook will execute the order based on the type of dish the customer ordered. In this case, the waiter is just a medium who does not need to know anything about what the customer has ordered and who is going to prepare.

1. Create the receiver objects for the commands

```java
package com.jaypeesoft.dp.command;

public class MainDish {
  String name;

  public MainDish(String name) {
    this.name = name;
  }

  public void order() {
    System.out.println("Main Dish (" + name + ") is ordered");
  }

  public void cancel() {
    System.out.println("Main Dish (" + name + ") is cancelled");
  }
}

public class Dessert {
  String name;

  public Dessert(String name) {
    this.name = name;
  }

  public void order() {
    System.out.println("Dessert (" + name + ") is ordered");
  }

  public void cancel() {
    System.out.println("Dessert (" + name + ") is cancelled");
  }
}
```

2. Create Command interface and its concrete implementations that represent various commands.

```java
package com.jaypeesoft.dp.command;

public interface Command {
  public abstract void execute();
}
```

```java
package com.jaypeesoft.dp.command;

public class OrderMainDish implements Command {
  private MainDish item;

  public OrderMainDish(MainDish i) {
    item = i;
  }

  public void execute() {
    item.order();
  }
}

public class OrderDessert implements Command {
  private Dessert coupon;

  public OrderDessert(Dessert c) {
    coupon = c;
  }

  public void execute() {
    coupon.order();
  }
}

public class CancelMainDish implements Command {
  private MainDish item;

  public CancelMainDish(MainDish i) {
    item = i;
  }

  public void execute() {
    item.cancel();
  }
}

public class CancelDessert implements Command {
  private Dessert coupon;

  public CancelDessert(Dessert c) {
    coupon = c;
  }

  public void execute() {
    coupon.cancel();
  }
}
```

3 Create the Invoker class

```java
package com.jaypeesoft.dp.command;

public class Waiter {

  /*
   * Waiter does not need to know about
   * the details of the command
   */
  public void execute(Command command) {
    command.execute();
  }

}
```

4. The client code. The client creates the commands and pass it on to the executor(waiter). The waiter does not need to know about the contents of the command, who will execute the command, etc. Everything will be encapsulated in the command.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.command.CancelMainDish;
import com.jaypeesoft.dp.command.Dessert;
import com.jaypeesoft.dp.command.MainDish;
import com.jaypeesoft.dp.command.OrderDessert;
import com.jaypeesoft.dp.command.OrderMainDish;
import com.jaypeesoft.dp.command.Waiter;

public class CommandClient {
  public static void main(String[] args) {

    /* Create a Waiter object */
    Waiter waiter = new Waiter();

    /* Customer wants Pizza */
    MainDish item1 = new MainDish("Pizza");
    /* Order Command is created for Pizza */
    OrderMainDish command1 = new OrderMainDish(item1);
    /* Command is given to the waiter to execute */
    waiter.execute(command1);

    /* Customer wants Burger and an Order is placed as above */
    MainDish item2 = new MainDish("Burger");
    OrderMainDish command2 = new OrderMainDish(item2);
    waiter.execute(command2);

    /* Now customer wants to cancel the burger */
    /* Cancel Command is created for the burger */
    CancelMainDish command3 = new CancelMainDish(item2);
    /* Cancel Command is given to the waiter to execute*/
    waiter.execute(command3);

    /* Customer wants Icecream */
    Dessert item3 = new Dessert("Icecream");
    /* Order Command is created for Icecream */
    OrderDessert command4 = new OrderDessert(item3);
    /* Command is given to the waiter to execute */
    waiter.execute(command4);
  }
}
```

Output

```
Main Dish (Pizza) is ordered
Main Dish (Burger) is ordered
Main Dish (Burger) is cancelled
Dessert (Icecream) is ordered
```

#### Benefits

decouples the object that invokes the operation from the one that know how to perform it
This pattern helps in terms of extensible as we can add a new command without changing the existing code.
It allows you to create a sequence of commands named macro. To run the macro, create a list of Command instances and call the execute method of all commands.
Ability to undo/redo easily

#### Drawbacks

Increase in the number of classes for each individual command

#### Real World Examples

Placing orders to the waiter in the restaurant

#### Java SDK Examples

All implementations of java.lang.Runnable
All implementations of javax.swing.Action
