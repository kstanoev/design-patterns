
### Visitor

#### When to use

To perform similar operations on objects of different types grouped in a structure (a collection or a more complex structure).
To perform distinct and unrelated operations on objects without polluting their classes with these operations.
To run different methods based on concrete type without instanceof or typeof operators.
To perform double dispatching.

#### Intent

Represent an operation to be performed on the elements of an object structure. Visitor lets you define a new operation without changing the classes of the elements on which it operates.

#### Components

A Visitable Interface
Concrete classes that implement Visitable interface
A Visitor Interface
Concrete classes that implement Visitor interface

#### Implementation

Consider a scenario where we need to perform a certain kind of operations without modifying the actual classes. For example, while creating an order, we want to perform a set of operations on all items (for e.g. applying discount, calculate tax etc). We can add getTax() and getDiscount() methods to the actual classes, but this pollutes the class and requires a change in the class signature every time an operation needs to added or removed. We can avoid these issues by using the double delegation feature of Visitor pattern.

1. Create a Visitable interface and the concrete classes that implement this.

```java
package com.jaypeesoft.dp.visitor;

public interface Visitable {

  public void apply(Visitor visitor);

}
```

```java
package com.jaypeesoft.dp.visitor;

public class FoodItem implements Visitable {

  public int id;
  public String name;
  public double price;

  public FoodItem(int id, String name, double price) {
    this.id = id;
    this.name = name;
    this.price = price;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  @Override
  public void apply(Visitor visitor) {
    visitor.visit(this);
  }

}
```

```java
package com.jaypeesoft.dp.visitor;

public class LiquorItem implements Visitable {

  public int id;
  public String name;
  public double price;

  public LiquorItem(int id, String name, double price) {
    this.id = id;
    this.name = name;
    this.price = price;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  @Override
  public void apply(Visitor visitor) {
    visitor.visit(this);
  }

}
```

2. Create a Visitor interface and the concrete classes that implement this.

```java
package com.jaypeesoft.dp.visitor;

public interface Visitor {

  void visit(FoodItem item);
  void visit(LiquorItem item);

}
```

```java
package com.jaypeesoft.dp.visitor;

public class DiscountVisitor implements Visitor {

  private double totalDiscount;

  public void visit(FoodItem item) {
    /* apply 30% off for food items */
    double discount = item.getPrice() * 0.3;
    totalDiscount += discount;
    item.setPrice(item.getPrice()-discount);
  }

  public void visit(LiquorItem item) {
    /* apply 10% off for liquor items */
    double discount = item.getPrice() * 0.1;
    totalDiscount += discount;
    item.setPrice(item.getPrice()-discount);
  }

  public double getTotalDiscount() {
    return totalDiscount;
  }

}

package com.jaypeesoft.dp.visitor;

public class TaxVisitor implements Visitor {

  private double totalTax;

  public void visit(FoodItem item) {
    /* apply 2% tax on food items */
    double tax = item.getPrice() * 0.02;
    totalTax += tax;
    item.setPrice(item.getPrice()+tax);
  }

  public void visit(LiquorItem item) {
    /* apply 30% tax on liquor items */
    double tax = item.getPrice() * 0.20;
    totalTax += tax;
    item.setPrice(item.getPrice()+tax);
  }

  public double getTotalTax() {
    return totalTax;
  }

}
```

3. The client code. With this approach, new operations can be performed easily by adding only the Visitor class without changing the signature of the Visitable concrete classes.

```java
package com.jaypeesoft.dp.client;

import java.util.ArrayList;
import java.util.List;

import com.jaypeesoft.dp.visitor.DiscountVisitor;
import com.jaypeesoft.dp.visitor.FoodItem;
import com.jaypeesoft.dp.visitor.LiquorItem;
import com.jaypeesoft.dp.visitor.TaxVisitor;
import com.jaypeesoft.dp.visitor.Visitable;

public class VisitorClient {

  public static void main(String[] args) {

    /* Create an order and add items */
    List order = new ArrayList();
    order.add(new FoodItem(1, "Italian Pizza", 6.99));
    order.add(new LiquorItem(1, "Wine", 9.99));
    order.add(new LiquorItem(1, "Beer", 5.99));
    order.add(new FoodItem(1, "Red Apple", 1.49));
    order.add(new FoodItem(1, "Almonds", 11.99));

    /* Create visitors to be applied */
    DiscountVisitor discountVisitor = new DiscountVisitor();
    TaxVisitor taxVisitor = new TaxVisitor();

    /* Apply visitors on items */
    for(Visitable item : order) {
      item.apply(discountVisitor);
      item.apply(taxVisitor);
    }

    System.out.println("Total Discount = " +
        discountVisitor.getTotalDiscount());
    System.out.println("Total Tax = " +
        taxVisitor.getTotalTax());
  }

}
```

Output

```
Total Discount = 7.73
Total Tax = 3.16
```

#### Benefits

Separate data structures from the operations on them.
New operations can be added easily by creating a new visitor.

#### Drawbacks

Adding a new type to the type hierarchy requires changes to all visitors.
Encapsulation principle is broken as we need to provide setter methods which allows access to the object's internal state.

#### Real World Examples

Tax Consultants visiting a campus to assist employees in tax filing

#### Java SDK Examples

javax.lang.model.element AnnotationValue and AnnotationValueVisitor
javax.lang.model.element Element and ElementVisitor
javax.lang.model.type TypeMirror and TypeVisitor
java.nio.file FileVisitor and SimpleFileVisitor
javax.faces.component.visit VisitContext and VisitCallback
