
### Chain of Responsibility

#### When to use

When a request needs to be processed by multiple processors
To achieve loose coupling between sender and receivers

#### Intent

Avoid coupling the sender of a request to its receiver by giving more than one object a chance to handle the request. Chain the receiving objects and pass the request along the chain until an object handles it.

#### Implementation

Assume that you want to implement an ATM Cash Dispenser which dispenses US Dollar notes in all denominations (100$, 50$, 20$, 10$, 5$, 2$, 1\$). This can be achieved by creating dispensers for each of the denominations and linking them to form a chain of dispensers. When the amount to be withdrawn is entered, the requested amount is passed to the first dispenser in the dispenser chain. Once the processor completes the action, the balance amount is passed to the next dispenser in the chain. This process is repeated until the balance becomes 0.

1. Create a CashDispenser class that will take the denomination as the constructor argument. This class has a reference to the next CashDispenser (so that a chain of dispensers can be formed).

```java
package com.jaypeesoft.dp.chainofresponsibility;

public class CashDispenser {

  private int denominator;
  private CashDispenser next = null;

  public CashDispenser(int val) {
    this.denominator = val;
  }

  /* Method to Chain the dispensers */
  public void setNextDispenser(CashDispenser d) {
    if(next == null)
      next = d;
    else
      next.setNextDispenser(d);
  }

  /* Process the request and pass it
     to the next processor if required */
  public void dispense(int amount) {
    if (amount >= denominator) {
      int num = amount / denominator;
      int balance = amount % denominator;
      System.out.println(num + " * "
          + denominator + "$");
      if (balance != 0)
        next.dispense(balance);
    } else {
      next.dispense(amount);
    }
  }

}
```

2. The client code. The client creates dispensers for various denominations and chain them to form a linked list. The client just launches the dispense activity only once and the request is passed through the chain automatically (until it gets processed).

```java
package com.jaypeesoft.dp.client;

import java.util.Scanner;

import com.jaypeesoft.dp.chainofresponsibility.CashDispenser;

public class ChainClient {

    public static void main(String[] args) {

      /* Create a chain of dispensers */
      CashDispenser dispenser = new CashDispenser(100);
      dispenser.setNextDispenser(new CashDispenser(50));
      dispenser.setNextDispenser(new CashDispenser(20));
      dispenser.setNextDispenser(new CashDispenser(10));
      dispenser.setNextDispenser(new CashDispenser(5));
      dispenser.setNextDispenser(new CashDispenser(2));
      dispenser.setNextDispenser(new CashDispenser(1));

      /* Get the amount */
      int amount = 0;
      Scanner in = new Scanner(System.in);
      System.out.print("Enter the amount to withdraw: ");
      amount = in.nextInt();

      /* dispense the amount */
      dispenser.dispense(amount);

      in.close();

    }
}
```

Output

```
[input 1]
Enter the amount to withdraw: 324
[output 1]
3 * 100$
1 * 20$
2 * 2$
```

```
[input 2]
Enter the amount to withdraw: 635
[output 2]
6 * 100$
1 * 20$
1 * 10$
1 * 5$
```

#### Benefits

Client does not need to know about all the processors. It sends the request to the first processor in the chain (launch and leave).
Unlike the Decorator pattern, the chain can be broken at any point to prevent other processors from handling the request.

#### Drawbacks

Since there is no explicit handler/receiver for the request, there is a possibility that the request remains unprocessed.
Incorrectly configured chain may cause some requests to be skipped.

#### Real World Examples

Escalation Matrix
Reimbursement Approval Hierarchy

#### Software Examples

Windows Event Handlers - Events are propagated until it gets processed
Exception Handling - Exceptions are re-thrown if the handler is incapable of handling it.

#### Java SDK Examples

java.util.logging.Logger log()
javax.servlet.Filter doFilter()
