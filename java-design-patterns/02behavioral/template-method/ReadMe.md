
### Template Method

#### When to use

To define a skeleton of an algorithm or an operation. Allow the sub-classes to re-define part of the logic.

#### Intent

Define the skeleton of an algorithm in an operation, deferring some steps to subclasses. Template Method lets subclasses redefine certain steps of an algorithm without changing the algorithm's structure.

#### Components

An abstract class that defines the template method (public final)
Concrete implementations that override the steps defined in the above template method

#### Implementation

Assume that you want to have a standard procedure to cook pizza and you do not want the subclasses to change this procedure. This can be implemented with the help of a template method which defines the skeleton of the algorithm. You can create subclasses that redefine certain steps in the algorithm.

1. Create an abstract base class that defines the template method. Note that the method is declared as 'final' to avoid the subclasses from overriding and changing the logic. Provide default implementations and also the abstract methods for the steps to be overridden.

```java
package com.jaypeesoft.dp.templatemethod;

public abstract class Pizza {

  /* Template method exposed to client */
  public final void preparePizza() {
    selectCrust();
    addIngredients();
    addToppings();
    cook();
  }

  /* Define abstract steps to be overridden */

  protected abstract void addToppings();
  protected abstract void addIngredients();

  /* Define default implementations */

  protected void selectCrust() {
    System.out.println("Selected default Crust");
  }

  protected void cook() {
    System.out.println("Cooked for 5 minutes");
  }

}
```

2. Create subclasses which redefine certain steps in the algorithm.

```java
package com.jaypeesoft.dp.templatemethod;

public class MeatPizza extends Pizza {

  protected void addIngredients() {
    System.out.println("Added Meat Pizza ingredients");
  }

  protected void addToppings() {
    System.out.println("Added Meat Pizza toppings");
  }

  protected void cook() {
    System.out.println("Cooked for 15 minutes");
  }

}
```

```java
package com.jaypeesoft.dp.templatemethod;

public class CheesePizza extends Pizza {

  protected void addIngredients() {
    System.out.println("Added Cheese Pizza ingredients");
  }

  protected void addToppings() {
    System.out.println("Added Cheese Pizza toppings");
  }

  protected void cook() {
    System.out.println("Cooked for 10 minutes");
  }

}
```

3. The client code. The client instantiates appropriate subclass object and invokes the template method.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.templatemethod.CheesePizza;
import com.jaypeesoft.dp.templatemethod.MeatPizza;
import com.jaypeesoft.dp.templatemethod.Pizza;

public class TemplateMethodClient {

  public static void main(String[] args) {
    System.out.println("Preparing a Cheese Pizza");
    Pizza pizza1 = new CheesePizza();
    pizza1.preparePizza();

    System.out.println("Preparing a Meat Pizza");
    Pizza pizza2 = new MeatPizza();
    pizza2.preparePizza();
  }

}
```

Output

```
Preparing a Cheese Pizza
Selected default Crust
Added Cheese Pizza ingredients
Added Cheese Pizza toppings
Cooked for 10 minutes

Preparing a Meat Pizza
Selected default Crust
Added Meat Pizza ingredients
Added Meat Pizza toppings
Cooked for 15 minutes
```

#### Benefits

Avoids code duplication.
Subclasses can decide how to implement steps in an algorithm.
Steps of the algorithm or the operation can be changed without changes in the subclasses.

#### Drawbacks

Inadequate documentation may confuse developers about the flow in the template method.

#### Software Examples

Most of the software frameworks define template methods. Framework make callbacks into methods implemented in child classes.

#### Java SDK Examples

All non-abstract methods of java.io.InputStream, java.io.OutputStream, java.io.Reader and java.io.Writer.
All non-abstract methods of java.util.AbstractList, java.util.AbstractSet and java.util.AbstractMap.
javax.servlet.http.HttpServlet, all the doXXX() methods by default sends a HTTP 405 "Method Not Allowed" error to the response. You're free to implement none or any of them.
