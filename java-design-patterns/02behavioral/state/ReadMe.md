
### State

#### When to use

To encapsulate varying behavior for the same object based on its internal state.
To have the ability to change the behavior at run time.

#### Intent

Allow an object to alter its behavior when its internal state changes. The object will appear to change its class.

#### Implementation

Consider the ATM scenario. The ATM can be in various states/modes like working, no cash, error, etc. When the customer interacts with the ATM, it should respond based on the current state. For example, when the ATM is in "No Cash" or "Error" state, the withdrawal should not be allowed. Typically, conditional statements are used to implement this kind of behavior. Also the same conditional statements may have to be repeated in different functions to support the possible interactions. This makes the code fragile and supporting a new state/operation may become difficult.
State pattern can be used here to simplify the design. For e.g, when a new ATM state needs to be added, it can be done easily by adding a new state. The other classes will remain unchanged.

1. Define a State abstract base class and represent the different "states" of the state machine as derived classes of the State base class. Define state-specific behavior in the appropriate State derived classes.

```java
package com.jaypeesoft.dp.state;

public interface AtmState {
  public void withdraw(int amount);
  public void refill(int amount);
}
```

```java
package com.jaypeesoft.dp.state;

public class Working implements AtmState {

  Atm atm;

  Working(Atm atm) {
    this.atm = atm;
  }

  public void withdraw(int amount) {

    int cashStock = atm.getCashStock();
    if(amount > cashStock) {
      /* Insufficient fund.
       * Dispense the available cash */
      amount = cashStock;
      System.out.print("Partial amount ");
    }
    System.out.println(amount + "$ is dispensed");
    int newCashStock = cashStock - amount;
    atm.setCashStock(newCashStock);
    if(newCashStock == 0) {
      atm.setState(new NoCash(atm));
    }

  }

  public void refill(int amount) {
    System.out.println(amount + "$ is loaded");
    atm.setCashStock(atm.getCashStock()+amount);
  }

}

public class NoCash implements AtmState {

  Atm atm;

  NoCash(Atm atm) {
    this.atm = atm;
  }

  public void withdraw(int amount) {
    System.out.println("Out of cash");
  }

  public void refill(int amount) {
    System.out.println(amount + "$ is loaded");
    atm.setState(new Working(atm));
    atm.setCashStock(atm.getCashStock()+amount);
  }

}
```

2. Define a "context" class to present a single interface to the outside world. Maintain a pointer to the current "state" in the "context" class. To change the state of the state machine, change the current "state" pointer.

```java
package com.jaypeesoft.dp.state;

public class Atm implements AtmState {

  int cashStock;
  AtmState currentState;

  public Atm() {
    currentState = new NoCash(this);
  }

  public int getCashStock() {
    return cashStock;
  }

  public void setCashStock(int CashStock) {
    this.cashStock = CashStock;
  }

  public void setState(AtmState state) {
    currentState = state;
  }

  public AtmState getState() {
    return currentState;
  }

  public void withdraw(int amount) {
    currentState.withdraw(amount);
  }

  public void refill(int amount) {
    currentState.refill(amount);
  }

}
```

3. The client code. The client interacts with the context.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.state.Atm;

public class StateClient {

  public static void main(String [] args) {
    Atm atm = new Atm();
    atm.refill(100);
    atm.withdraw(50);
    atm.withdraw(30);
    atm.withdraw(30); // overdraft
    atm.withdraw(20); // overdraft
    atm.refill(50);
    atm.withdraw(50);
  }
}
```

Output

```
100$ is loaded
50$ is dispensed
30$ is dispensed
Partial amount 20$ is dispensed
Out of cash
50$ is loaded
50$ is dispensed
```

#### Benefits

New states can be added easily as the state specific behavior is encapsulated in that state class.
Avoids too many conditional statements.
State class aggregates the state specific behavior which results in increased cohesion.

#### Drawbacks

State classes must know about each other so that the state can be changed.
May result in duplicate objects if state classes are not defined as singletons.

#### Real World Examples

Electro-machanical machines (ATM machine, Gear box, Microwave oven) which can have many internal states.

#### Software Examples

FSM - Finite State Machine

#### Java SDK Examples

javax.faces.lifecycle.LifeCycle execute() (controlled by FacesServlet, the behaviour is dependent on current phase (state) of JSF lifecycle)
