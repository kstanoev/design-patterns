
### Iterator

#### When to use

To provide a standard way to traverse through collections of similar objects.
To expose a simple interface to the client by hiding the complexities of the traversal.
To provide a uniform interface for traversing different aggregate structures.

#### Intent

Provide a way to access the elements of an aggregate object sequentially without exposing its underlying representation.

#### Components

An interface for the collection
Concrete implementations of the collection interface
An "iterator" class that can encapsulate traversal of the "collection" class.

#### Implementation

The key idea is to take the responsibility for access and traversal out of the aggregate object and put it into an Iterator object that defines a standard traversal protocol. Lets create an iterator for a collection of integers. The collection can be in the form of an array or a linked list.

1. Create an interface for the collection and its concrete implementations. The implementations will have a getIterator() method that returns the iterator for the underlying collection of objects.

```java
package com.jaypeesoft.dp.iterator;

public interface Collection {

  public Iterator getIterator();
  public void insert(int val);
}
```

```java
package com.jaypeesoft.dp.iterator;

public class Array implements Collection {

  private int [] arr;
  private int len;

  public Array(int size) {
    arr = new int[size];
    len = 0;
  }

  public void insert(int val) {
    arr[len++] = val;
  }

  public Iterator getIterator() {
    return new ArrayIterator(arr, len);
  }

}
```

```java
package com.jaypeesoft.dp.iterator;

public class LinkedList implements Collection {

  private Node head;
  private Node current;

  public void insert(int val) {
    Node node = new Node(val);
    if (current == null) {
      head = node;
      current = node;
    } else {
      current.next = node;
      current = node;
    }
  }

  public Iterator getIterator() {
    return new ListIterator(head);
  }

  /* Inner class for Node */
  class Node {
    private int data;
    private Node next;

    public Node(int data) {
      this.data = data;
    }

    public int data() {
      return data;
    }

    public Node next() {
      return this.next;
    }

  }

}
```

2. Design an "iterator" interface and its concrete implementations that can encapsulate traversal of the "collection" class.

```java
package com.jaypeesoft.dp.iterator;

public interface Iterator {

  public int next();
  public boolean hasNext();

}
```

```java
package com.jaypeesoft.dp.iterator;

public class ArrayIterator implements Iterator{
  private int [] arr;
  private int pos;
  private int len;

  public ArrayIterator(int [] arr, int len) {
    this.arr = arr;
    this.len = len;
    pos = -1;
  }

  public int next() {
    return arr[++pos];
  }

  public boolean hasNext() {
    return (pos+1) < len;
  }

}
```

```java
package com.jaypeesoft.dp.iterator;

import com.jaypeesoft.dp.iterator.LinkedList.Node;

public class ListIterator implements Iterator {
  private Node current;

  public ListIterator(Node head) {
    this.current = head;
  }

  public int next() {
    int val = current.data();
    current = current.next();
    return val;
  }

  public boolean hasNext() {
    return current != null;
  }

}
```

3. The client code. The client uses iterator methods to access the elements of the collection class.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.iterator.Array;
import com.jaypeesoft.dp.iterator.Collection;
import com.jaypeesoft.dp.iterator.Iterator;
import com.jaypeesoft.dp.iterator.LinkedList;

public class IteratorClient {

  public static void main(String[] args) {

    Collection arr = new Array(4);
    arr.insert(1);
    arr.insert(2);
    arr.insert(3);
    arr.insert(4);
    iterate(arr.getIterator());

    Collection list = new LinkedList();
    list.insert(11);
    list.insert(22);
    list.insert(33);
    iterate(list.getIterator());

  }

  public static void iterate(Iterator it) {
    while (it.hasNext())
      System.out.print(it.next() + " ");
    System.out.println();
  }
}
```

Output

```
1 2 3 4
11 22 33
```

#### Benefits

The iterator class can be extended to add new functionalities without having to alter the actual object it iterates over.
The original class need not be cluttered with interfaces that are specific to traversal.
Iterator can support multiple traversals because it keeps track of its own traversal state.

#### Real World Examples

Television Remote Control. User can access the required channel by pressing the 'next' or 'previous' button. User does not need to know about the internal details of the channel like frequency, band etc. Remote Control provides a simplified interface to iterate through the channels.

#### Software Examples

Iterators provided for various types of collections in SDKs

#### Java SDK Examples

All implementations of java.util.Iterator
All implementations of java.util.Enumeration
