
### Interpreter

#### When to use

To easily solve 'repeated' problems in a 'well-defined' domain with the help of a 'language'.

#### Intent

Given a language, define a representation for its grammar along with an interpreter that uses the representation to interpret sentences in the language.

#### Implementation

Here we see an example of evaluating arithmetic expressions using the Interpreter pattern. The representation is defined as POSTFIX and the grammer is defined to interprete different type of expressions. Though each expression is different, they are all constructed using the basic rules that make up the grammar for the language of arithmetic expressions.

1. Create an interface for the basic expression.

```csharp
interface Exp
```

2. Create concrete expressions including the terminal and non-terminal experessions.

```csharp
class Number
```

```csharp
class AddExp
```

```csharp
class SubtractExp
```

```csharp
class MultiplyExp
```

```csharp
class DivideExp
```

3. The client code. The client uses an abstract syntax tree representing a particular sentence in the language that the grammar defines. The abstract syntax tree is assembled from instances of the NonterminalExpression and TerminalExpression classes. The client then invokes the Interpret operation.

```csharp
class InterpreterClient
```

Output

```
Result: 15
```

#### Benefits

Grammars can be easily modified or extended by inheritance.
Expressions can be interpreted in new ways by adding new operations to the expression.

#### Drawbacks

Grammars containing many rules (i.e many rule classes) can be hard to manage and maintain.

#### Real World Examples

Language Interpreter/Translator

#### Software Examples

Software compiler
SQL evaluation engine
Graphing calculator input parser
XML parser

#### .NET Examples

java.util.Pattern
java.text.Normalizer
All subclasses of java.text.Format
All subclasses of javax.el.ELResolver
