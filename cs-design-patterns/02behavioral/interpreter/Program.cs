﻿using System;
using System.Collections.Generic;
namespace behavioral_interpreter
{
    class Program
    {
        static void Main(string[] args)
        {
            /* POSTFIX Expression to be evaluated */
            String postfix = "543-2+*";
            /* Operations supported */
            String OPERATORS = "+-*/";
            /* Stack for the operands */
            Stack<Expression> stack = new Stack<Expression>();
            foreach (char c in postfix.ToCharArray())
            {
                Expression resultExp;
                if (OPERATORS.IndexOf(c) == -1)
                {
                    /* number found, push it onto the stack */
                    resultExp = new Number(c - 48);
                }
                else
                {
                    /*
                     * operator found, pop out the last two operands from the stack and
                     * perform the operation
                     */
                    Expression right = stack.Pop();
                    Expression left = stack.Pop();
                    switch (c)
                    {
                        case '+':
                            resultExp = new Addition(left, right);
                            break;
                        case '-':
                            resultExp = new Subtraction(left, right);
                            break;
                        case '*':
                            resultExp = new Multiplication(left, right);
                            break;
                        case '/':
                            resultExp = new Division(left, right);
                            break;
                        default:
                            resultExp = new Number(0);
                            break;
                    }
                }
                /* push the result onto the stack */
                stack.Push(new Number(resultExp.Evaluate()));
            }
            Console.WriteLine("Result: " + stack.Pop().Evaluate());
        }
    }
    interface Expression
    {
        int Evaluate();
    }
    class Number : Expression
    {
        private int n;
        public Number(int n)
        {
            this.n = n;
        }
        public int Evaluate()
        {
            return n;
        }
    }
    class Addition : Expression
    {
        Expression first;
        Expression second;
        public Addition(Expression first, Expression second)
        {
            this.first = first;
            this.second = second;
        }
        public int Evaluate()
        {
            return first.Evaluate() + second.Evaluate();
        }
    }
    class Subtraction : Expression
    {
        Expression first;
        Expression second;
        public Subtraction(Expression first, Expression second)
        {
            this.first = first;
            this.second = second;
        }
        public int Evaluate()
        {
            return first.Evaluate() - second.Evaluate();
        }
    }
    class Multiplication : Expression
    {
        Expression first;
        Expression second;
        public Multiplication(Expression first, Expression second)
        {
            this.first = first;
            this.second = second;
        }
        public int Evaluate()
        {
            return first.Evaluate() * second.Evaluate();
        }
    }
    class Division : Expression
    {
        Expression first;
        Expression second;
        public Division(Expression first, Expression second)
        {
            this.first = first;
            this.second = second;
        }
        public int Evaluate()
        {
            return first.Evaluate() / second.Evaluate();
        }
    }
}