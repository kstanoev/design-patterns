﻿using System;
using System.Collections.Generic;
namespace behavioral_observer
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Create an order and add items */
            Order order = new Order();
            order.addItem(new Item("Pizza", 6.99));
            order.addItem(new Item("Wine", 9.99));
            order.addItem(new Item("Beer", 5.99));
            order.addItem(new Item("Apple", 1.49));
            Console.WriteLine("---------------------------------");
            /* Create payments and make payments */
            order.makePayment(new Payment("CASH", 20.00));
            order.makePayment(new Payment("CREDIT", 10.00));
            order.makePayment(new Payment("DEBIT", 10.00));
            Console.WriteLine("---------------------------------");
            /* Complete the order */
            order.completeOrder();
        }
    }
    public abstract class Observer
    {
        public abstract void update(String str);
    }
    class CustomerDisplay : Observer
    {
        public override void update(String str)
        {
            Console.WriteLine("[CustomerDisplay] " + str);
        }
    }
    class CashierDisplay : Observer
    {
        public override void update(String str)
        {
            Console.Write("[CashierDisplay] " + str);
        }
    }
    interface Topic
    {
        void register(Observer obj);
        void notifyObservers(String line);
    }
    class AddItemTopic : Topic
    {
        List<Observer> addItemObservers = new List<Observer>();
        public void notifyObservers(String line)
        {
            foreach (Observer o in addItemObservers)
            {
                o.update(line);
            }
        }
        public void register(Observer o)
        {
            addItemObservers.Add(o);
        }
    }
    class AddPaymentTopic : Topic
    {
        List<Observer> addPaymentObservers = new List<Observer>();
        public void notifyObservers(String line)
        {
            foreach (Observer o in addPaymentObservers)
            {
                o.update(line);
            }
        }
        public void register(Observer o)
        {
            addPaymentObservers.Add(o);
        }
    }
    class CompleteOrderTopic : Topic
    {
        List<Observer> orderCompletedObservers = new List<Observer>();
        public void notifyObservers(String line)
        {
            foreach (Observer o in orderCompletedObservers)
            {
                o.update(line);
            }
        }
        public void register(Observer o)
        {
            orderCompletedObservers.Add(o);
        }
    }
    class Item
    {
        public readonly String name;
        public readonly double price;
        public Item(String name, double price)
        {
            this.name = name;
            this.price = price;
        }
    }
    class Payment
    {
        public readonly String type;
        public readonly double amount;
        public Payment(String type, double amount)
        {
            this.type = type;
            this.amount = amount;
        }
    }
    class Order
    {
        List<Item> cart = new List<Item>();
        List<Payment> payments = new List<Payment>();
        private Topic addItemTopic;
        private Topic addPaymentTopic;
        private Topic completeOrderTopic;
        public Order()
        {
            // create observers (devices)
            Observer cashierDisplay = new CashierDisplay();
            Observer customerDisplay = new CustomerDisplay();
            // create subjects (events)
            addItemTopic = new AddItemTopic();
            addPaymentTopic = new AddPaymentTopic();
            completeOrderTopic = new CompleteOrderTopic();
            // Cashier display subscribed to all topics
            addItemTopic.register(cashierDisplay);
            addPaymentTopic.register(cashierDisplay);
            completeOrderTopic.register(cashierDisplay);
            // Customer display subscribed to all topics
            addItemTopic.register(customerDisplay);
            addPaymentTopic.register(customerDisplay);
            completeOrderTopic.register(customerDisplay);
        }
        public void addItem(Item item)
        {
            cart.Add(item);
            String line = item.name + " $" + item.price;
            addItemTopic.notifyObservers(line);
        }
        public void makePayment(Payment payment)
        {
            payments.Add(payment);
            String line = payment.type + " $" + payment.amount;
            addPaymentTopic.notifyObservers(line);
        }
        public void completeOrder()
        {
            String line = "Order completed";
            completeOrderTopic.notifyObservers(line);
        }
    }
}