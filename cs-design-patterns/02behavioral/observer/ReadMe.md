
### Observer

#### When to use

When there is one to many relationship between objects such as if one object is modified, its dependent objects are to be notified automatically and corresponding changes are done to all dependent objects.

#### Intent

Define a one-to-many dependency between objects so that when one object changes state, all its dependents are notified and updated automatically.

#### Implementation

Consider a simple Point Of Sale (POS) system which has many peripherals attached to it. Each peripheral(observer) registers itself to the interested topics(subjects) so that it can update its status when that topic/event occurs. For e.g., when an item or payment is added to the order, the cashier display and customer display need to be updated with the new details. Similarly when the order is completed, the receipt printer will act on it by printing the receipt.

1. Create an observer interface and its concrete implementations. These observers subscribe to specific topics(subjects) so that it can display the updates.

```csharp
public abstract class Observer
```

```csharp
class CustomerDisplay
```

```csharp
class CashierDisplay
```

1. Create an interface for the subject and its concrete implementations. These subjects contain lists to keep track of subscribed observers that need to be notified.

```csharp
interface Topic
```

```csharp
class AddItemTopic
```

```csharp
class AddPaymentTopic
```

```csharp
class CompleteOrderTopic
```

1. Create Item, Payment, and Order classes. The observers subscribe to various subjects(topics) in the Order class constructor.

```csharp
class Item
```

```csharp
class Payment
```

```csharp
class Order
```

1. The client code. When an item or payment is added to the order, the corresponding topic notifies its observers.

```csharp
class ObserverClient
```

Output

```
[CashierDisplay] Pizza  $6.99	[CustomerDisplay] Pizza  $6.99
[CashierDisplay] Wine   $9.99	[CustomerDisplay] Wine   $9.99
[CashierDisplay] Beer   $5.99	[CustomerDisplay] Beer   $5.99
[CashierDisplay] Apple  $1.49	[CustomerDisplay] Apple  $1.49
[CashierDisplay] CASH 	$20.0	[CustomerDisplay] CASH   $20.0
[CashierDisplay] CREDIT $10.0	[CustomerDisplay] CREDIT $10.0
[CashierDisplay] DEBIT 	$10.0	[CustomerDisplay] DEBIT  $10.0
[CashierDisplay] Order completed [CustomerDisplay] Order completed
```

#### Benefits

Loose coupling between Subject and Observer allows you vary subjects and observers independently.
Supports broadcast communication.

#### Real World Examples

Auction - The bidders act as observers and raise the paddle to accept the bid. When the bid is accepted, the others are notified by the auctioneer.

#### .NET Examples

Events & Delegates
