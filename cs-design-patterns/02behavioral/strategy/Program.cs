﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace behavioral_strategy
{
	class Program
	{
		static void Main(string[] args)
		{
			var numbers = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
			var strategist = new Traversal.Strategist();
			strategist.Set(new EvenIndex());
			strategist.Run(numbers);
		}
	}
	class EvenIndex : Traversal.ITraversalStrategy
	{
		public EvenIndex()
		{
			this.Name = "Even Index";
		}
		public string Name { get; }
		public IEnumerable<int> Traverse(IList<int> numbers)
		{
			var result = new List<int>();
			for (int i = 0; i < numbers.Count; i++)
			{
				if (i % 2 == 0)
				{
					result.Add(numbers[i]);
				}
			}
			return result;
		}
	}
}
namespace behavioral_strategy.Traversal
{
	public interface ITraversalStrategy
	{
		string Name { get; }
		IEnumerable<int> Traverse(IList<int> numbers);
	}

	public class Strategist
	{
		private ITraversalStrategy strategy = null;
		public Strategist()
		{
			this.strategy = new DefaultStrategy();
		}

		public void Set(ITraversalStrategy newStrategy)
		{
			this.strategy = newStrategy;
		}
		public void Run(IList<int> numbers)
		{
			var result = strategy.Traverse(numbers);
			Console.WriteLine($"{strategy.Name}: {string.Join(",", result)}");
		}
	}
	class DefaultStrategy : ITraversalStrategy
	{
		public DefaultStrategy()
		{
			this.Name = "Simple";
		}
		public string Name { get; }
		public IEnumerable<int> Traverse(IList<int> numbers)
		{
			return numbers;
		}
	}
}