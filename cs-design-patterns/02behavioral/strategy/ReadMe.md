
### Strategy

#### When to use

To switch out different implementations for different situations.

To support different variants of the algorithm.

#### Intent

Define a family of algorithms, encapsulate each one, and make them interchangeable. Strategy lets the algorithm vary independently from clients that use it.

#### Implementation

Assume that you want to create a shopping cart application. One of the requirements is to allow different types of payments:

- Card
- Cash

1. Define the interface of an interchangeable family of algorithms and move algorithm implementation details in subclasses.

```csharp
interface PaymentStrategy
```
```csharp
class CardPayment
```
```csharp
class CashPayment
```

2. Interface method is used to invoke the algorithm.

```csharp
class Item
```

```csharp
class Order
```

3. The client code. The client chooses the algorithm/strategy at runtime.

```csharp
class StrategyClient
```

Output

```
Italian Pizza         6.99
Wine                  9.99
Beer                  5.99
Red Apple             1.49
Almonds              11.99
Cash 20.0$
VISA CREDIT 10.0$
AMEX DEBIT 10.0$
```

#### Benefits

Too many conditional statements can be avoided with different strategy classes.
Allows runtime selection of algorithms from the same algorithm family.
Improves extensibility with 3rd party implementations of the algorithms.
Improves readability by avoiding too many if/else or switch statements.
Enforces Open/Close principle.

#### Drawbacks

May increase the number of classes as each strategy needs to be defined in its own class.

#### Real World Examples

Choose a game strategy (attack/defend) based on the opponent.

#### Software Examples

Sorting (We want to sort these numbers, but we don't know if we are gonna use BrickSort, BubbleSort or some other sorting)
Validation (We need to check items according to "Some rule", but it's not yet clear what that rule will be, and we may think of new ones.)
Games (We want player to either walk or run when he moves, but maybe in the future, he should also be able to swim, fly, teleport, burrow underground, etc.)
Storing information (We want the application to store information to the Database, but later it may need to be able to save a file, or make a webcall)
Outputting (We need to output X as a plain string, but later may be a CSV, XML, Json, etc.)

#### .NET Examples

Linq through Lambda experssion queries .Where(...)

Both Array and ArrayList provide the capability to sort the objects contained in the collection via the Sort method. In fact, ArrayList.Sort just calls Sort on the underlying array. These methods use the QuickSort algorithm. By default, the Sort method will use the IComparable implementation for each element to handle the comparisons necessary for sorting. Sometimes, though, it is useful to sort the same list in different ways. For example, arrays of strings might be sorted with or without case sensitivity. To accomplish this, an overload of Sort exists that takes an IComparer as a parameter; IComparer.Compare is then used for the comparisons. This overload allows users of the class to use any of the built-in IComparers or any of their own making, without having to change or even know the implementation details of Array, ArrayList, or the QuickSort algorithm.