﻿using System;
using System.Collections.Generic;
namespace behavioral_strategy.payment
{
	class Program
	{
		void Main(string[] args)
		{
			/* Create an order and add items */
			Order order = new Order();
			order.addItem(new Item("Italian Pizza", 6.99));
			order.addItem(new Item("Wine", 9.99));
			order.addItem(new Item("Beer", 5.99));
			order.addItem(new Item("Red Apple", 1.49));
			order.addItem(new Item("Almonds", 11.99));
			Console.WriteLine("---------------------------------");
			/* Create payment strategies and make payment */
			order.MakePayment(new CashPayment(20.00));
			order.MakePayment(new CardPayment("CREDIT", "VISA", 10.00));
			order.MakePayment(new CardPayment("DEBIT", "AMEX", 10.00));
			Console.WriteLine("---------------------------------");
		}
	}
	interface PaymentStrategy
	{
		void Pay();
	}
	class CardPayment : PaymentStrategy
	{
		private string accountType;
		private string cardIssuer;
		private double amount;
		public CardPayment(string accountType, string cardIssuer, double amount)
		{
			this.accountType = accountType;
			this.cardIssuer = cardIssuer;
			this.amount = amount;
		}
		public void Pay()
		{
			Console.WriteLine($"{this.cardIssuer} {this.accountType} ${this.amount}");
		}
	}
	class CashPayment : PaymentStrategy
	{
		private double amount;
		public CashPayment(double amount)
		{
			this.amount = amount;
		}
		public void Pay()
		{
			Console.WriteLine($"Cash ${this.amount}");
		}
	}
	class Item
	{
		public Item(string name, double price)
		{
			this.Name = name;
			this.Price = price;
		}
		public string Name { get; }
		public double Price { get; }
	}
	class Order
	{
		List<Item> cart = new List<Item>();
		List<PaymentStrategy> paymentStrategies = new List<PaymentStrategy>();
		public void addItem(Item item)
		{
			this.cart.Add(item);
			Console.WriteLine($"{item.Name}\t{item.Price}");
		}
		public void MakePayment(PaymentStrategy strategy)
		{
			this.paymentStrategies.Add(strategy);
			strategy.Pay();
		}
	}
}