﻿using System;
namespace behavioral_template_method
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Preparing a Cheese Pizza");
            Pizza pizza1 = new CheesePizza();
            pizza1.Prepare();
            Console.WriteLine("Preparing a Meat Pizza");
            Pizza pizza2 = new MeatPizza();
            pizza2.Prepare();
        }
    }
    public abstract class Pizza
    {
        /* Template method exposed to client */
        public void Prepare()
        {
            this.SelectCrust();
            this.AddIngredients();
            this.AddToppings();
            this.Cook();
        }
        /* Define abstract steps to be overridden */
        protected abstract void AddToppings();
        protected abstract void AddIngredients();
        /* Define default implementations */
        protected void SelectCrust()
        {
            Console.WriteLine("Selected default Crust");
        }
        protected virtual void Cook()
        {
            Console.WriteLine("Cooked for 5 minutes");
        }
    }
    public class MeatPizza : Pizza
    {
        protected override void AddIngredients()
        {
            Console.WriteLine("Added Meat Pizza ingredients");
        }
        protected override void AddToppings()
        {
            Console.WriteLine("Added Meat Pizza toppings");
        }
        protected override void Cook()
        {
            Console.WriteLine("Cooked for 15 minutes");
        }
    }
    public class CheesePizza : Pizza
    {
        protected override void AddIngredients()
        {
            Console.WriteLine("Added Cheese Pizza ingredients");
        }
        protected override void AddToppings()
        {
            Console.WriteLine("Added Cheese Pizza toppings");
        }
        protected override void Cook()
        {
            Console.WriteLine("Cooked for 10 minutes");
        }
    }
}