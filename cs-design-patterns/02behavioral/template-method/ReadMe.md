
### Template Method

#### When to use

To define a skeleton of an algorithm or an operation **and** allow the sub-classes to re-define part of the logic.

#### Intent

Define the skeleton of an algorithm in an operation, deferring some steps to subclasses. Template Method lets subclasses redefine certain steps of an algorithm without changing the algorithm's structure.

#### Components

An abstract class that defines the template method (public final)
Concrete implementations that override the steps defined in the above template method

#### Implementation

Assume that you want to have a standard procedure to cook pizza and you do not want the subclasses to change this procedure. This can be implemented with the help of a template method which defines the skeleton of the algorithm. You can create subclasses that redefine certain steps in the algorithm.

1. Create an abstract base class that defines the template method. Note that the method is declared as 'final' to avoid the subclasses from overriding and changing the logic. Provide default implementations and also the abstract methods for the steps to be overridden.

```csharp
public abstract class Pizza
```

2. Create subclasses which redefine certain steps in the algorithm.

```csharp
class MeatPizza
```

```csharp
class CheesePizza
```

3. The client code. The client instantiates appropriate subclass object and invokes the template method.

```csharp
class TemplateMethodClient
```

Output

```
Preparing a Cheese Pizza
Selected default Crust
Added Cheese Pizza ingredients
Added Cheese Pizza toppings
Cooked for 10 minutes

Preparing a Meat Pizza
Selected default Crust
Added Meat Pizza ingredients
Added Meat Pizza toppings
Cooked for 15 minutes
```

#### Benefits

Avoids code duplication.

Subclasses can decide how to implement steps in an algorithm.

Steps of the algorithm or the operation can be changed without changes in the subclasses.

#### Drawbacks

Inadequate documentation may confuse developers about the flow in the template method.

#### Software Examples

Most of the software frameworks define template methods. Framework make callbacks into methods implemented in child classes.

#### .NET Examples

All non-abstract methods of java.io.InputStream, java.io.OutputStream, java.io.Reader and java.io.Writer.
All non-abstract methods of java.util.AbstractList, java.util.AbstractSet and java.util.AbstractMap.
javax.servlet.http.HttpServlet, all the doXXX() methods by default sends a HTTP 405 "Method Not Allowed" error to the response. You're free to implement none or any of them.
