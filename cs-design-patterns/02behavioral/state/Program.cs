﻿using System;
namespace behavioral_state
{
    class Program
    {
        static void Main(string[] args)
        {
            Atm atm = new Atm();
            atm.refill(100);
            atm.withdraw(50);
            atm.withdraw(30);
            atm.withdraw(30); // overdraft
            atm.withdraw(20); // overdraft
            atm.refill(50);
            atm.withdraw(50);
        }
    }
    interface AtmState
    {
        void withdraw(int amount);
        void refill(int amount);
    }
    class Working : AtmState
    {
        Atm atm;
        public Working(Atm atm)
        {
            this.atm = atm;
        }
        public void withdraw(int amount)
        {
            int cashStock = atm.getCashStock();
            if (amount > cashStock)
            {
                /* Insufficient fund.
                 * Dispense the available cash */
                amount = cashStock;
                Console.Write("Partial amount ");
            }
            else
            {
                Console.WriteLine(amount + "$ is dispensed");
                int newCashStock = cashStock - amount;
                atm.setCashStock(newCashStock);
                if (newCashStock == 0)
                {
                    atm.setState(new NoCash(atm));
                }
            }
        }
        public void refill(int amount)
        {
            Console.WriteLine(amount + "$ is loaded");
            atm.setCashStock(atm.getCashStock() + amount);
        }
    }
    class NoCash : AtmState
    {
        Atm atm;
        public NoCash(Atm atm)
        {
            this.atm = atm;
        }
        public void withdraw(int amount)
        {
            Console.WriteLine("Out of cash");
        }
        public void refill(int amount)
        {
            Console.WriteLine(amount + "$ is loaded");
            atm.setState(new Working(atm));
            atm.setCashStock(atm.getCashStock() + amount);
        }
    }
    class Atm : AtmState
    {
        int cashStock;
        AtmState currentState;
        public Atm()
        {
            currentState = new NoCash(this);
        }
        public int getCashStock()
        {
            return cashStock;
        }
        public void setCashStock(int CashStock)
        {
            this.cashStock = CashStock;
        }
        public void setState(AtmState state)
        {
            currentState = state;
        }
        public AtmState getState()
        {
            return currentState;
        }
        public void withdraw(int amount)
        {
            currentState.withdraw(amount);
        }
        public void refill(int amount)
        {
            currentState.refill(amount);
        }
    }
}