
### State

#### When to use

To encapsulate varying behavior for the same object based on its internal state.
To have the ability to change the behavior at run time.

#### Intent

Allow an object to alter its behavior when its internal state changes. The object will appear to change its class.

#### Implementation

Consider the ATM scenario. The ATM can be in various states/modes like working, no cash, error, etc. When the customer interacts with the ATM, it should respond based on the current state. For example, when the ATM is in "No Cash" or "Error" state, the withdrawal should not be allowed. Typically, conditional statements are used to implement this kind of behavior. Also the same conditional statements may have to be repeated in different functions to support the possible interactions. This makes the code fragile and supporting a new state/operation may become difficult.
State pattern can be used here to simplify the design. For e.g, when a new ATM state needs to be added, it can be done easily by adding a new state. The other classes will remain unchanged.

1. Define a State abstract base class and represent the different "states" of the state machine as derived classes of the State base class. Define state-specific behavior in the appropriate State derived classes.

```csharp
interface AtmState
```

```csharp
class Working
```
```csharp
class NoCash
```

2. Define a "context" class to present a single interface to the outside world. Maintain a pointer to the current "state" in the "context" class. To change the state of the state machine, change the current "state" pointer.

```csharp
class Atm
```

3. The client code. The client interacts with the context.

```csharp
class StateClient
```

Output

```
100$ is loaded
50$ is dispensed
30$ is dispensed
Partial amount 20$ is dispensed
Out of cash
50$ is loaded
50$ is dispensed
```

#### Benefits

New states can be added easily as the state specific behavior is encapsulated in that state class.
Avoids too many conditional statements.
State class aggregates the state specific behavior which results in increased cohesion.

#### Drawbacks

State classes must know about each other so that the state can be changed.
May result in duplicate objects if state classes are not defined as singletons.

#### Real World Examples

Electro-machanical machines (ATM machine, Gear box, Microwave oven) which can have many internal states.

#### Software Examples

FSM - Finite State Machine

#### .NET Examples

javax.faces.lifecycle.LifeCycle execute() (controlled by FacesServlet, the behaviour is dependent on current phase (state) of JSF lifecycle)
