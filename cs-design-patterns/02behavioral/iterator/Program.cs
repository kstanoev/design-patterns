﻿using System;
namespace behavioral_iterator
{
    class Program
    {
        static void Main(string[] args) { }
        public static void iterate(Iterator it)
        {
            while (it.hasNext())
                Console.Write(it.next() + " ");
            Console.WriteLine();
        }
    }
    class ListIterator : Iterator
    {
        private Node current;
        public ListIterator(Node head)
        {
            this.current = head;
        }
        public int next()
        {
            int val = current.data();
            current = current.Next;
            return val;
        }
        public bool hasNext()
        {
            return current != null;
        }
    }
    class ArrayIterator : Iterator
    {
        private int[] arr;
        private int pos;
        private int len;
        public ArrayIterator(int[] arr, int len)
        {
            this.arr = arr;
            this.len = len;
            pos = -1;
        }
        public int next()
        {
            return arr[++pos];
        }
        public bool hasNext()
        {
            return (pos + 1) < len;
        }
    }
    interface Iterator
    {
        int next();
        bool hasNext();
    }
    class Node
    {
        private int d;
        private Node n;
        public Node(int data)
        {
            this.d = data;
        }
        public int data()
        {
            return d;
        }
        public Node Next { get; set; }
        // public Node next()
        // {
        //     return this.n;
        // }
    }
    class LinkedList : Collection
    {
        private Node head;
        private Node current;
        public void insert(int val)
        {
            Node node = new Node(val);
            if (current == null)
            {
                head = node;
                current = node;
            }
            else
            {
                current.Next = node;
                current = node;
            }
        }
        public Iterator getIterator()
        {
            return new ListIterator(head);
        }
    }
    class Array : Collection
    {
        private int[] arr;
        private int len;
        public Array(int size)
        {
            arr = new int[size];
            len = 0;
        }
        public void insert(int val)
        {
            arr[len++] = val;
        }
        public Iterator getIterator()
        {
            return new ArrayIterator(arr, len);
        }
    }
    interface Collection
    {
        Iterator getIterator();
        void insert(int val);
    }
}