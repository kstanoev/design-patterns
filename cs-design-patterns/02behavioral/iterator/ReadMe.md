
### Iterator

#### When to use

To provide a standard way to traverse through collections of similar objects.

To expose a simple interface to the client by hiding the complexities of the traversal.

To provide a uniform interface for traversing different aggregate structures.

#### Intent

Provide a way to access the elements of an aggregate object sequentially without exposing its underlying representation.

#### Components

An interface for the collection
Concrete implementations of the collection interface
An "iterator" class that can encapsulate traversal of the "collection" class.

#### Implementation

The key idea is to take the responsibility for access and traversal out of the aggregate object and put it into an Iterator object that defines a standard traversal protocol. Lets create an iterator for a collection of integers. The collection can be in the form of an array or a linked list.

1. Create an interface for the collection and its concrete implementations. The implementations will have a getIterator() method that returns the iterator for the underlying collection of objects.

```csharp
interface Collection
```

```csharp
class Array
```

```csharp
class LinkedList
```

2. Design an "iterator" interface and its concrete implementations that can encapsulate traversal of the "collection" class.

```csharp
interface Iterator
```

```csharp
class ArrayIterator
```

```csharp
class ListIterator
```

3. The client code. The client uses iterator methods to access the elements of the collection class.

```csharp
class IteratorClient
```

Output

```
1 2 3 4
11 22 33
```

#### Benefits

The iterator class can be extended to add new functionalities without having to alter the actual object it iterates over.
The original class need not be cluttered with interfaces that are specific to traversal.
Iterator can support multiple traversals because it keeps track of its own traversal state.

#### Real World Examples

Television Remote Control. User can access the required channel by pressing the 'next' or 'previous' button. User does not need to know about the internal details of the channel like frequency, band etc. Remote Control provides a simplified interface to iterate through the channels.

#### Software Examples

Iterators provided for various types of collections in SDKs

#### .NET Examples

Any collection that implements IEnumerable
