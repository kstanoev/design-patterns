
### Command

#### When to use

When the executor of the command does not need to know anything at all about what the command is, what context information it needs on or what it does.
To register a callback when some event is triggered.

#### Intent

Encapsulate a request as an object, thereby letting you parameterize clients with different requests, queue or log requests, and support undoable operations.

#### Implementation

Consider a restaurant scenario. A customer doesn't give the order directly to the cooks. A waiter takes the order to the kitchen where a designated cook will execute the order based on the type of dish the customer ordered. In this case, the waiter is just a medium who does not need to know anything about what the customer has ordered and who is going to prepare.

1. Create the receiver objects for the commands

```csharp
class MainDish
```
```csharp
class Dessert
```

2. Create Command interface and its concrete implementations that represent various commands.

```csharp
interface Command
```

```csharp
class OrderMainDish
```
```csharp
class OrderDessert
```
```csharp
class CancelMainDish
```
```csharp
class CancelDessert
```

3 Create the Invoker class

```csharp
class Waiter
```

4. The client code. The client creates the commands and pass it on to the executor(waiter). The waiter does not need to know about the contents of the command, who will execute the command, etc. Everything will be encapsulated in the command.

```csharp
class CommandClient
```

Output

```
Main Dish (Pizza) is ordered
Main Dish (Burger) is ordered
Main Dish (Burger) is cancelled
Dessert (Icecream) is ordered
```

#### Benefits

decouples the object that invokes the operation from the one that know how to perform it
This pattern helps in terms of extensible as we can add a new command without changing the existing code.
It allows you to create a sequence of commands named macro. To run the macro, create a list of Command instances and call the execute method of all commands.
Ability to undo/redo easily

#### Drawbacks

Increase in the number of classes for each individual command

#### Real World Examples

Placing orders to the waiter in the restaurant

#### .NET Examples

All implementations of java.lang.Runnable
All implementations of javax.swing.Action
