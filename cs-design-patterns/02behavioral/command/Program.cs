﻿using System;
namespace behavioral_command
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Create a Waiter object */
            Waiter waiter = new Waiter();
            /* Customer wants Pizza */
            MainDish item1 = new MainDish("Pizza");
            /* Order Command is created for Pizza */
            OrderMainDish command1 = new OrderMainDish(item1);
            /* Command is given to the waiter to execute */
            waiter.Execute(command1);
            /* Customer wants Burger and an Order is placed as above */
            MainDish item2 = new MainDish("Burger");
            OrderMainDish command2 = new OrderMainDish(item2);
            waiter.Execute(command2);
            /* Now customer wants to cancel the burger */
            /* Cancel Command is created for the burger */
            CancelMainDish command3 = new CancelMainDish(item2);
            /* Cancel Command is given to the waiter to execute*/
            waiter.Execute(command3);
            /* Customer wants Icecream */
            Dessert item3 = new Dessert("Icecream");
            /* Order Command is created for Icecream */
            OrderDessert command4 = new OrderDessert(item3);
            /* Command is given to the waiter to execute */
            waiter.Execute(command4);
        }
    }
    class MainDish
    {
        private readonly string name;
        public MainDish(string name)
        {
            this.name = name;
        }
        public void Order()
        {
            Console.WriteLine("Main Dish (" + this.name + ") is ordered");
        }
        public void Cancel()
        {
            Console.WriteLine("Main Dish (" + this.name + ") is cancelled");
        }
    }
    class Dessert
    {
        private string name;
        public Dessert(string name)
        {
            this.name = name;
        }
        public void Order()
        {
            Console.WriteLine("Dessert (" + this.name + ") is ordered");
        }
        public void Cancel()
        {
            Console.WriteLine("Dessert (" + this.name + ") is cancelled");
        }
    }
    interface Command
    {
        void Execute();
    }
    class OrderMainDish : Command
    {
        private readonly MainDish mainDish;
        public OrderMainDish(MainDish mainDish)
        {
            this.mainDish = mainDish;
        }
        public void Execute()
        {
            this.mainDish.Order();
        }
    }
    class OrderDessert : Command
    {
        private readonly Dessert dessert;
        public OrderDessert(Dessert dessert)
        {
            this.dessert = dessert;
        }
        public void Execute()
        {
            this.dessert.Order();
        }
    }
    class CancelMainDish : Command
    {
        private readonly MainDish mainDish;
        public CancelMainDish(MainDish mainDish)
        {
            this.mainDish = mainDish;
        }
        public void Execute()
        {
            this.mainDish.Cancel();
        }
    }
    class CancelDessert : Command
    {
        private readonly Dessert dessert;
        public CancelDessert(Dessert dessert)
        {
            this.dessert = dessert;
        }
        public void Execute()
        {
            this.dessert.Cancel();
        }
    }
    class Waiter
    {
        /*
         * Waiter does not need to know about
         * the details of the command
         */
        public void Execute(Command order)
        {
            order.Execute();
        }
    }
}