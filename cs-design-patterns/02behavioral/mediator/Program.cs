﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace behavioral_mediator
{
	// TODO: Port to C#
    class Program
    {
        static void Main(string[] args)
        {
            // AtcMediator atcMediator = new AtcMediatorImpl();
            // /* Create a runway & register with the mediator */
            // atcMediator.registerRunway(new Runway("RW-1", false));
            // /* Create gates & register gates with the mediator */
            // atcMediator.registerGate(new Gate("G-1", false));
            // atcMediator.registerGate(new Gate("G-2", false));
            // atcMediator.registerGate(new Gate("G-3", false));
            // /* Many flights are arriving and they want to land and take off.
            //  * They just contact the mediator instead of
            //  * directly communicating with individual objects. */
            // for (int i = 0; i < 10; i++)
            // {
            //     Flight flight = new Flight(atcMediator, "F-00" + (i + 1));
            //     flight.landAndTakeOff();
            // }
        }
    }
    class Gate
    {
        private String gateNum;
        private bool inUse;
        public Gate(String gateNum, bool inUse)
        {
            this.gateNum = gateNum;
            this.inUse = inUse;
        }
        public String getGateNum()
        {
            return gateNum;
        }
        public bool isInUse()
        {
            return inUse;
        }
        public void setInUse(bool inUse)
        {
            this.inUse = inUse;
        }
    }
    class Runway
    {
        private String runwayNum;
        private bool inUse;
        public Runway(String runwayNum, bool inUse)
        {
            this.runwayNum = runwayNum;
            this.inUse = inUse;
        }
        public String getRunwayNum()
        {
            return runwayNum;
        }
        public void setRunwayNum(String runwayNum)
        {
            this.runwayNum = runwayNum;
        }
        public bool isInUse()
        {
            return inUse;
        }
        public void setInUse(bool inUse)
        {
            this.inUse = inUse;
        }
    }
    class Flight
    {
        private AtcMediator atc;
        private String flightNum;
        private Runway runway;
        private Gate gate;
        public Flight(AtcMediator atc, String flightNum)
        {
            this.atc = atc;
            this.flightNum = flightNum;
        }
        public String getName()
        {
            return flightNum;
        }
        public Runway getRunway()
        {
            return runway;
        }
        public void allocateRunway(Runway runway)
        {
            this.runway = runway;
        }
        public Gate getGate()
        {
            return gate;
        }
        public void allocateGate(Gate gate)
        {
            this.gate = gate;
        }
        public void landAndTakeOff()
        {
            // Console.WriteLine(flightNum + " is requesting landing permission");
            // while (false == atc.getLandingPermission(this))
            // {
            //     Thread.sleep(1000);
            // }
            // // Landing permission granted, land now
            // land();
            // while (false == atc.getTakeoffPermission(this))
            // {
            //     Thread.sleep(1000);
            // }
            // // Take off permission granted, take off now
            // takeOff();
        }
        public void land()
        {
            // atc.enterRunway(runway);
            // Console.WriteLine(this.flightNum + " is landing, gate is " +
            //     gate.getGateNum());
            // /* Exit the runway after 1 second */
            // new java.util.Timer().schedule(new java.util.TimerTask()
            // {
            //     public void run()
            //     {
            //         atc.exitRunway(runway);
            //         atc.enterGate(gate);
            //     }
            // }, 1000);
        }
        public void takeOff()
        {
            // /* Takeoff after 5 seconds */
            // new java.util.Timer().schedule(new java.util.TimerTask()
            // {
            //     public void run()
            //     {
            //         Console.WriteLine(flightNum + " is taking off");
            //         atc.exitGate(gate);
            //         atc.enterRunway(runway);
            //         /* Exit the runway after 1 second */
            //         new java.util.Timer().schedule(new java.util.TimerTask()
            //         {
            //             public void run()
            //             {
            //                 atc.exitRunway(runway);
            //             }
            //         }, 1000);
            //     }
            // }, 5000);
        }
    }
    class AtcMediatorImpl : AtcMediator
    {
        private Runway runway;
        private List<Gate> gates;
        public AtcMediatorImpl()
        {
            gates = new List<Gate>();
        }
        public void registerRunway(Runway runway)
        {
            this.runway = runway;
        }
        public void registerGate(Gate gate)
        {
            gates.Add(gate);
        }
        public bool getLandingPermission(Flight flight)
        {
            /* Check if the runway is free */
            if (runway.isInUse() == false)
            {
                /* Find an available gate */
                foreach (Gate gate in gates)
                {
                    if (gate.isInUse() == false)
                    {
                        /* reserve the gate and runway */
                        flight.allocateRunway(runway);
                        flight.allocateGate(gate);
                        return true;
                    }
                }
                Console.WriteLine("[ATC Mediator] All gates in use");
            }
            else
                Console.WriteLine("[ATC Mediator] Runway in use");
            return false;
        }
        public bool getTakeoffPermission(Flight flight)
        {
            return runway.isInUse() == false;
        }
        public void exitRunway(Runway runway)
        {
            runway.setInUse(false);
        }
        public void exitGate(Gate gate)
        {
            gate.setInUse(false);
        }
        public void enterRunway(Runway runway)
        {
            runway.setInUse(true);
        }
        public void enterGate(Gate gate)
        {
            gate.setInUse(true);
        }
    }
    interface AtcMediator
    {
        void registerRunway(Runway runway);
        void registerGate(Gate gate);
        bool getLandingPermission(Flight flight);
        bool getTakeoffPermission(Flight flight);
        void enterRunway(Runway runway);
        void exitRunway(Runway runway);
        void enterGate(Gate gate);
        void exitGate(Gate gate);
    }
	class Dispatcher
	{

	}
}