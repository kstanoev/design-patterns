
### Mediator

#### When to use

To facilitate interactions between a set of objects where the communications are complex and hard to maintain.
To have a centralized control for the object interactions.

#### Intent

Define an object that encapsulates how a set of objects interact. Mediator promotes loose coupling by keeping objects from referring to each other explicitly, and it lets you vary their interaction independently.

#### Components

Mediator interface – an interface that defines the communication rules between objects

Concrete mediator – a mediator object which will enables communication between participating objects

Colleagues – objects communicating with each other through mediator object

#### Implementation

Mediator enables decoupling of objects by introducing a layer in between so that the interaction between objects happen via the layer. ATC (Air Traffic Controller) is a perfect example for the mediator design pattern. A typical airport is a complex system that involves complex communications between flights, airport vehicles, and other airport systems. Direct communication between all these participants is error prone and practically impossible. Hence we need a system that helps in communication between flights, airport vehicle and coordinates landing, take-off, etc.

1. Create an interface that defines the communication rules between objects

```csharp
interface AtcMediator
```

2. Create a mediator object which will enables communication between the participating objects.

```csharp
class AtcMediatorImpl
```

3. Define colleagues. Colleagues keep a reference to its Mediator object.

```csharp
class Flight
```

```csharp
class Runway
```

```csharp
class Gate
```

4. The client code. The client creates mediator and all the colleagues register with the mediator. When the objects want to interact with other objects, it uses the mediator,

```csharp
class MediatorClient
```

Output

```
F-001 is requesting landing permission
F-001 is landing, gate is G-1
F-002 is requesting landing permission
F-002 is landing, gate is G-2
F-001 is taking off
F-003 is requesting landing permission
F-003 is landing, gate is G-1
F-004 is requesting landing permission
F-004 is landing, gate is G-3
F-002 is taking off
F-003 is taking off
F-005 is requesting landing permission
[ATC Mediator] Runway in use
F-005 is landing, gate is G-1
F-004 is taking off
F-006 is requesting landing permission
F-006 is landing, gate is G-2
F-005 is taking off
F-007 is requesting landing permission
F-007 is landing, gate is G-1
F-006 is taking off
F-008 is requesting landing permission
F-008 is landing, gate is G-2
F-009 is requesting landing permission
F-009 is landing, gate is G-3
F-007 is taking off
F-0010 is requesting landing permission
F-0010 is landing, gate is G-1
F-008 is taking off
F-009 is taking off
F-0010 is taking off
```

#### Benefits

A mediator promotes loose coupling between colleagues.
Promotes one-to-many relationships that are easier to understand, implement and maintain than many-to-many relationships.
A mediator simplifies the communication and provides a centralized control.

#### Drawbacks

A Mediator class may become complex if not designed carefully.
All communications are routed by the Mediator which may impact the performance.

#### Real World Examples

Air Traffic Controller
Head Speaker of the Parliament - members do not communicate with each other

#### .NET Examples

java.util.Timer (all scheduleXXX() methods)
java.util.concurrent.Executor execute()
java.util.concurrent.ExecutorService (the invokeXXX() and submit() methods)
java.util.concurrent.ScheduledExecutorService (all scheduleXXX() methods)
java.lang.reflect.Method invoke()
