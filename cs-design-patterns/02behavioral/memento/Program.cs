﻿using System;
using System.Collections.Generic;
using System.Text;
namespace behavioral_memento
{
	class Program
	{
		static void Main(string[] args)
		{
			Bug bug = new Bug("Application does not work as expected.", Severity.Low);
			bug.Save();
			// ...
			bug.Update(Severity.Medium);
			bug.Update(Severity.High);
			bug.Save();
			Console.WriteLine(bug.ToString());
			bug.Update(Severity.Critical);
			Console.WriteLine(bug.ToString());
			Console.WriteLine();
			Console.WriteLine("==== UNDO ====");
			while (bug.Undo())
			{
				Console.WriteLine(bug.ToString());
			}
		}
	}

	interface IUndoableItem
	{
		void Save();
		bool Undo();
	}
	enum Severity
	{
		Low,
		Medium,
		High,
		Critical
	}
	interface IBug
	{
		string Title { get; }
		Severity Severity { get; }
	}
	class Bug : IBug, IUndoableItem
	{
		private Stack<Severity> severityHistory = new Stack<Severity>();

		public Bug(string title, Severity severity)
		{
			this.Title = title;
			this.Severity = severity;
		}
		public string Title
		{
			get;
			private set;
		}
		public Severity Severity
		{
			get;
			private set;
		}
		public void Update(Severity newSeverity)
		{
			this.Severity = newSeverity;
		}

		public void Save()
		{
			this.severityHistory.Push(this.Severity);
		}

		public bool Undo()
		{
			if (this.severityHistory.Count > 0)
			{
				this.Severity = this.severityHistory.Pop();
				return true;
			}
			else
			{
				return false;
			}
		}

		public override string ToString()
		{
			return $"{this.Title} ::: {this.Severity}";
		}
	}
}