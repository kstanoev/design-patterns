
### Memento

#### When to use

To take snapshots and restore an object back to its previous state (e.g. "undo" or "rollback" operations).

#### Intent

Without violating encapsulation, capture and externalize an object's internal state so that the object can be restored to this state later.

#### Components

Originator - the object that knows how to save itself.

Caretaker - the object that knows why and when the Originator needs to save and restore itself.

Memento - the lock box that is written and read by the Originator, and shepherded by the Caretaker.

#### Implementation

Memento design pattern is used when the object state needs to be saved so that it can be restored later. Memento pattern : this in a way that the saved state data of the object is not accessible outside of the object, thus protecting the integrity of saved state data.
We will see an OS Recovery tool that saves the good configuration of the OS so that it can be restored later if there is any data corruption.

1. Create an Originator class. In our example, the operating system s/w is the originator which creates/restores a memento.

```csharp
class OS
```

2. Create a Memento class. In this case, the RecoveryImage class is the memento which represents the object that can be saved and restored.

```csharp
class RecoveryImage
```

3. Create a Caretaker class which manages the mementos. The caretaker class cannot modify the contents of memento.

```csharp
class RecoveryTool
```

4. The client code. The client uses the Caretaker (RecoveryTool) to "rollback" to the previous good state.

```csharp
public class MementoClient {

	public static void main(String[] args) 
	{
		// TODO
	}
```

Output

```
Windows 10 + Antivirus
--Saved OS Image--
Windows 10 + Antivirus + Tomcat Server
--Saved OS Image--
Windows 10 + Antivirus + Tomcat Server + MySql
--Restored OS Image--
Windows 10 + Antivirus + Tomcat Server
--Restored OS Image--
Windows 10 + Antivirus
```

#### Benefits

Internal state of the memento object cannot be changed
Easy to implement recoverable states

#### Drawbacks

Too many objects may be created by the Originator which makes the maintenance expensive.

#### Software Examples

Undo functionality in text editors
Snapshots of softwares

#### .NET Examples

java.util.Date (the setter methods do that, Date is internally represented by a long value)
All implementations of java.io.Serializable
All implementations of javax.faces.component.StateHolder
