﻿using System;
namespace behavioral_chain_of_responsibility
{
	class Program
	{
		static void Main(string[] args)
		{
			/* Create a chain of dispensers */
			CashDispenser dispenser = new CashDispenser(100) // $100
				.SetNextDispenser(new CashDispenser(50)) // $50
				.SetNextDispenser(new CashDispenser(20)) // $20
				.SetNextDispenser(new CashDispenser(10)) // $10
				.SetNextDispenser(new CashDispenser(5)) // $5
				.SetNextDispenser(new CashDispenser(2)) // $2
				.SetNextDispenser(new CashDispenser(1)); // $1

			Console.Write("Enter the amount to withdraw: ");
			var amount = int.Parse(Console.ReadLine());
			dispenser.Dispense(amount);
		}
	}
}
class CashDispenser
{
	private int denomination;
	private CashDispenser next = null;
	public CashDispenser(int denomination)
	{
		this.denomination = denomination;
	}
	// Method to Chain the dispensers
	public CashDispenser SetNextDispenser(CashDispenser next)
	{
		if (this.next == null)
		{
			this.next = next;
		}
		else
		{
			this.next.SetNextDispenser(next);
		}
		return this;
	}
	// Process the request and pass it to the next processor if required
	public void Dispense(int amount)
	{
		if (amount >= denomination)
		{
			int banknotes = amount / denomination;
			Console.WriteLine($"{banknotes} of ${denomination}");
			int reminder = amount % denomination;
			if (reminder != 0)
			{
				next.Dispense(reminder);
			}
		}
		else
		{
			next.Dispense(amount);
		}
	}
}