﻿using System;
using System.Collections.Generic;
namespace behavioral_visitor
{
	class Program
	{
		static void Main(string[] args)
		{
			/* Create an order and add items */
			List<Visitable> order = new List<Visitable>();
			order.Add(new FoodItem(1, "Italian Pizza", 6.99));
			order.Add(new LiquorItem(1, "Wine", 9.99));
			order.Add(new LiquorItem(1, "Beer", 5.99));
			order.Add(new FoodItem(1, "Red Apple", 1.49));
			order.Add(new FoodItem(1, "Almonds", 11.99));
			/* Create visitors to be applied */
			DiscountVisitor discountVisitor = new DiscountVisitor();
			TaxVisitor taxVisitor = new TaxVisitor();
			/* Apply visitors on items */
			foreach (Visitable item in order)
			{
				item.Apply(discountVisitor);
				item.Apply(taxVisitor);
			}
			Console.WriteLine("Total Discount = " +
				discountVisitor.getTotalDiscount());
			Console.WriteLine("Total Tax = " +
				taxVisitor.getTotalTax());
		}
	}
	interface Visitable
	{
		void Apply(Visitor visitor);
	}
	class FoodItem : Visitable
	{
		public int id;
		public String name;
		public double price;
		public FoodItem(int id, String name, double price)
		{
			this.id = id;
			this.name = name;
			this.price = price;
		}
		public double getPrice()
		{
			return price;
		}
		public void setPrice(double price)
		{
			this.price = price;
		}
		public void Apply(Visitor visitor)
		{
			visitor.visit(this);
		}
	}
	class LiquorItem : Visitable
	{
		public int id;
		public String name;
		public double price;
		public LiquorItem(int id, String name, double price)
		{
			this.id = id;
			this.name = name;
			this.price = price;
		}
		public double getPrice()
		{
			return price;
		}
		public void setPrice(double price)
		{
			this.price = price;
		}
		public void Apply(Visitor visitor)
		{
			visitor.visit(this);
		}
	}
	interface Visitor
	{
		void visit(FoodItem item);
		void visit(LiquorItem item);
	}
	class DiscountVisitor : Visitor
	{
		private double totalDiscount;
		public void visit(FoodItem item)
		{
			/* apply 30% off for food items */
			double discount = item.getPrice() * 0.3;
			totalDiscount += discount;
			item.setPrice(item.getPrice() - discount);
		}
		public void visit(LiquorItem item)
		{
			/* apply 10% off for liquor items */
			double discount = item.getPrice() * 0.1;
			totalDiscount += discount;
			item.setPrice(item.getPrice() - discount);
		}
		public double getTotalDiscount()
		{
			return totalDiscount;
		}
	}
	class TaxVisitor : Visitor
	{
		private double totalTax;
		public void visit(FoodItem item)
		{
			/* apply 2% tax on food items */
			double tax = item.getPrice() * 0.02;
			totalTax += tax;
			item.setPrice(item.getPrice() + tax);
		}
		public void visit(LiquorItem item)
		{
			/* apply 30% tax on liquor items */
			double tax = item.getPrice() * 0.20;
			totalTax += tax;
			item.setPrice(item.getPrice() + tax);
		}
		public double getTotalTax()
		{
			return totalTax;
		}
	}
}