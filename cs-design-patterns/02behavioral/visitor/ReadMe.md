### Visitor

#### When to use

To perform similar operations on objects of different types grouped in a structure (a collection or a more complex structure).
To perform distinct and unrelated operations on objects without polluting their classes with these operations.
To run different methods based on concrete type without instanceof or typeof operators.
To perform double dispatching.

#### Intent

Represent an operation to be performed on the elements of an object structure. Visitor lets you define a new operation without changing the classes of the elements on which it operates.

#### Components

A Visitable Interface
Concrete classes that implement Visitable interface
A Visitor Interface
Concrete classes that implement Visitor interface

#### Implementation

Consider a scenario where we need to perform a certain kind of operations without modifying the actual classes. For example, while creating an order, we want to perform a set of operations on all items (for e.g. applying discount, calculate tax etc). We can add getTax() and getDiscount() methods to the actual classes, but this pollutes the class and requires a change in the class signature every time an operation needs to added or removed. We can avoid these issues by using the double delegation feature of Visitor pattern.

1. Create a Visitable interface and the concrete classes that implement this.

```csharp
interface Visitable
```

```csharp
class FoodItem
```

```csharp
class LiquorItem
```

2. Create a Visitor interface and the concrete classes that implement this.

```csharp

interface Visitor
```

```csharp
class DiscountVisitor
```

``` csharp
class TaxVisitor
```

3. The client code. With this approach, new operations can be performed easily by adding only the Visitor class without changing the signature of the Visitable concrete classes.

```csharp
class VisitorClient
```

Output

```
Total Discount = 7.73
Total Tax = 3.16
```

#### Benefits

Separate data structures from the operations on them.
New operations can be added easily by creating a new visitor.

#### Drawbacks

Adding a new type to the type hierarchy requires changes to all visitors.
Encapsulation principle is broken as we need to provide setter methods which allows access to the object's internal state.

#### Real World Examples

Tax Consultants visiting a campus to assist employees in tax filing

#### .NET Examples

javax.lang.model.element AnnotationValue and AnnotationValueVisitor
javax.lang.model.element Element and ElementVisitor
javax.lang.model.type TypeMirror and TypeVisitor
java.nio.file FileVisitor and SimpleFileVisitor
javax.faces.component.visit VisitContext and VisitCallback
