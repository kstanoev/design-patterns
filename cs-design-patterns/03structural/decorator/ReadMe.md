
### Decorator

#### When to use

To dynamically change the functionality of an object at runtime without impacting the existing functionality of the objects.
To add functionalities that may be withdrawn later.
To combine multiple functionalities where it is impractical to create a subclass for every possible combination.

#### Intent

Attach additional responsibilities to an object dynamically. Decorators provide a flexible alternative to subclassing for extending functionality.

#### Problem

Assume that you need to prepare a pizza that may have multiple combinations of toppings, cooking type, etc. Though this can be achieved by Inheritance, it is not practical to create subclasses for every possible combination. Alternatively you can use Composition and add the required functionalities. Since all concrete implementations conform to the same interface, we can mix and match any number of classes to create a variety of combinations.

#### Implementation

1. Define IWorkItem interface and create a WorkItem class which is an implementation of the IWorkItem interface.

```csharp
interface IWorkItem
```

```csharp
abstract class WorkItem : IWorkItem
```

2. Create Concrete Decorators.

```csharp
abstract class AssignableWorkItem : IWorkItem
```
```csharp
abstract class EditableWorkItem : IWorkItem
```

3. The Client code. The Decorator Pattern allows one to mix and match without needing to create a rigid hierarchy.

```csharp
class Program
{
	static void Main(string[] args)
	{
		// TODO
	}
}
```

Output

```
// TODO
```

#### Benefits

Decorator allows us to mix and match features instead of creating concrete implementations for all possible combinations.

Decorator allows us to modify an object in a much more modular and less fundamental way than inheritance would.

New functionalities can be easily supported.

#### Drawbacks

Multiple small objects are created in the process of creating an object.

Complexity is increased.

#### Programming Examples

File Stream implementations

#### .NET Examples

Any useful executable program involves either reading input, writing output, or both. Regardless of the source of the data being read or written, it can be treated abstractly as a sequence of bytes. 

.NET uses the System.IO.Stream class to represent this abstraction. Whether the data involves characters in a text file, TCP/IP network traffic, or something else entirely, chances are you will have access to it via a Stream.