﻿using System;
using System.Collections.Generic;

namespace structural_decorator
{
	class Program
	{
		static void Main(string[] args)
		{
			var board = new List<IWorkItem>();
			board.Add(new EditableWorkItem("Item 1"));
			board.Add(new EditableWorkItem("Item 2"));
			board.Add(new ReadOnlyWorkItem("Item 3"));

			// List all items
			Console.WriteLine("=== List ===");
			foreach (var item in board)
			{
				Console.WriteLine(item.Title);
			}

			// Update titles
			Console.WriteLine("=== Update ===");
			foreach (var item in board)
			{
				item.UpdateTitle($"Modified: {item.Title}");
				Console.WriteLine(item.Title);
			}
		}
	}
}

namespace WIM.Library
{
	interface IWorkItem
	{
		string Title { get; }
		void UpdateTitle(string title);
	}

	public abstract class WorkItem : IWorkItem
	{
		public string Title { get; protected set; }

		public virtual void UpdateTitle(string title)
		{
			this.Title = title;
		}
	}

	public class 

	class EditableWorkItem : WorkItem
	{
		public EditableWorkItem(string title)
		{
			this.Title = title;
		}
	}
	class ReadOnlyWorkItem : WorkItem
	{
		public ReadOnlyWorkItem(string title)
		{
			this.Title = title;
		}
		public override void UpdateTitle(string title)
		{
			Console.WriteLine($"{this.Title} is a read-only item.");
		}
	}
}