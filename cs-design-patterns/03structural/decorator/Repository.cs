﻿using System;
using System.Collections.Generic;

namespace structural_decorator.repository
{
	class Program
	{
		static void Main(string[] args)
		{
			var client = new GitLab.Client();
			client.Sync(new GitLab.Repository("https://gitlab.com/kstanoev/design-patterns.git"));

			var client2 = new GitLab.Client();
			client2.Sync(new RepositoryLogDecorator(new GitLab.Repository("https://gitlab.com/kstanoev/design-patterns.git")));
		}
	}

	public class RepositoryLogDecorator : GitLab.IRepository
	{
		public GitLab.IRepository innerRepo;

		public RepositoryLogDecorator(GitLab.IRepository inner)
		{
			this.innerRepo = inner;
		}

		public string Url { get; }

		public void Add()
		{
			try
			{
				Console.WriteLine("Stage files for commit");
				this.innerRepo.Add();
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Add operation failed: {ex.Message}");
			}
		}

		public void Commit()
		{
			try
			{
				Console.WriteLine("Files committed to local repository");
				this.innerRepo.Commit();
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Commit operation failed: {ex.Message}");
			}
		}

		public void LogAndSync()
		{
			try
			{
				Console.WriteLine($"Start: {DateTime.Now.ToShortTimeString()}");
				innerRepo.Pull();
				innerRepo.Add();
				innerRepo.Commit();
				innerRepo.Push();
				Console.WriteLine($"End: {DateTime.Now.ToShortTimeString()}");
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Exception: {ex.Message}");
			}
			Console.WriteLine("Success");
		}

		public void Pull()
		{
			try
			{
				Console.WriteLine("Pull from server");
				this.innerRepo.Pull();
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Pull operation failed: {ex.Message}");
			}
		}

		public void Push()
		{
			try
			{
				Console.WriteLine("Push to server");
				this.innerRepo.Push();
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Push operation failed: {ex.Message}");
			}
		}
	}
}

namespace GitLab
{
	public interface IRepository
	{
		string Url { get; }
		void Pull();
		void Add();
		void Commit();
		void Push();
	}

	public class Repository : IRepository
	{
		public Repository(string url)
		{
			this.Url = url;
		}

		public string Url { get; }

		public void Add()
		{
			// Some private GitLab code executing
		}

		public void Commit()
		{
			// Some private GitLab code executing
		}

		public void Pull()
		{
			// Some private GitLab code executing
		}

		public void Push()
		{
			// Some private GitLab code executing
		}
	}

	public class Client
	{
		public void Sync(IRepository repo)
		{
			repo.Pull();
			repo.Add();
			repo.Commit();
			repo.Push();
		}
	}
}