
### Proxy

#### When to use

To provide controlled access to a sensitive master object

To provide a local reference to a remote object

To improve performance when an object needs to be accessed frequently

#### Intent

Provide a surrogate or placeholder for another object to control access to it.

#### Implementation

Let us see the example of a Proxy Server. A proxy server, also known as a "proxy" or "application-level gateway", is a computer that acts as a gateway between a local network (e.g., all the computers at one company or in one building) and a larger-scale network such as the Internet. Proxy servers provide increased performance and security. A proxy server can act as an intermediary between the user's computer and the Internet to prevent from attack and unexpected access.
A proxy server will behave just like the real server and : the same interface.

1. Create an interface. Both the real subject and the proxy subject will implement this interface.

```csharp
interface Server
```

2. Implement the real subject and the proxy subject. Note that the RealServer class is protected so that the client cannot access it directly.

```csharp
class RealServer
```

```csharp
class ProxyServer
```

3. The client code. The client does not have access to the real subject. Every request is performed via a 'surrogate' object called proxy.

```csharp
class ProxyClient
```

Output

```
Logged into the Real Server
GET command executed
POST command executed
PUT command executed
DELETE command executed
Logged out from the Real Server
```

#### Benefits

A proxy can mask the life-cycle and state of a volatile resource from its client.
Easy to manage the access to the real subject.
Proxies can help in creating objects on-demand.

#### Drawbacks

Performance may be impacted due to the extra level of indirection.

#### Real World Examples

Agents with Power of Attorney
Representatives/Brokers

#### Software Examples

SSH client

#### .NET Examples

java.lang.reflect.Proxy
java.rmi.\*
javax.ejb.EJB
javax.inject.Inject
javax.persistence.PersistenceContext
