﻿using System;
namespace structural_proxy
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Client can access only the proxy server*/
            Server server = new ProxyServer();
            /* Client works with the same interface */
            server.authenticate();
            server.get();
            server.post();
            server.put();
            server.delete();
            server.logout();
        }
    }
    interface Server
    {
        void authenticate();
        void get();
        void post();
        void put();
        void delete();
        void logout();
    }
    class RealServer : Server
    {
        public void authenticate()
        {
            Console.WriteLine("Logged into the Real Server");
        }
        public void get()
        {
            Console.WriteLine("GET command executed");
        }
        public void post()
        {
            Console.WriteLine("POST command executed");
        }
        public void put()
        {
            Console.WriteLine("PUT command executed");
        }
        public void delete()
        {
            Console.WriteLine("DELETE command executed");
        }
        public void logout()
        {
            Console.WriteLine("Logged out from the Real Server");
        }
    }
    class ProxyServer : Server
    {
        /* Reference to RealServer */
        private RealServer realServer;
        private bool sessionActive;
        public ProxyServer()
        {
            this.realServer = new RealServer();
            sessionActive = false;
        }
        public void authenticate()
        {
            /* Get the user credentials and login */
            realServer.authenticate();
            /* Track the session */
            sessionActive = true;
        }
        public void get()
        {
            if (sessionActive)
                realServer.get();
            else
                Console.WriteLine("Invalid Session");
        }
        public void post()
        {
            if (sessionActive)
                realServer.post();
            else
                Console.WriteLine("Invalid Session");
        }
        public void put()
        {
            if (sessionActive)
                realServer.put();
            else
                Console.WriteLine("Invalid Session");
        }
        public void delete()
        {
            if (sessionActive)
                realServer.delete();
            else
                Console.WriteLine("Invalid Session");
        }
        public void logout()
        {
            realServer.logout();
            /* Deactivate the session */
            sessionActive = false;
        }
    }
}