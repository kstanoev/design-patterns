﻿using System;
using System.Collections.Generic;
namespace structural_adapter
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Create an order and add items */
            LegacyOMS oms = new LegacyOMS();
            oms.addItem(new Item("Italian Pizza", 6.99));
            oms.addItem(new Item("Wine", 9.99));
            oms.addItem(new Item("Beer", 5.99));
            oms.addItem(new Item("Red Apple", 1.49));
            oms.addItem(new Item("Almonds", 11.99));
            Console.WriteLine("---------------------------------");
            /* Create payment and make payment */
            oms.makePayment(new Payment("CASH", 20.00));
            oms.makePayment(new Payment("CREDIT", 10.00));
            oms.makePayment(new Payment("DEBIT", 10.00));
            Console.WriteLine("---------------------------------");
        }
    }
    class Item
    {
        private String name;
        private double price;
        public Item(String name, double price)
        {
            this.name = name;
            this.price = price;
        }
        public String getName()
        {
            return name;
        }
        public double getPrice()
        {
            return price;
        }
    }
    class Payment
    {
        public String type;
        public double amount;
        public Payment(String type, double amount)
        {
            this.type = type;
            this.amount = amount;
        }
        public void pay()
        {
            Console.WriteLine(type + " " + amount + "$");
        }
    }
    class LegacyOMS
    {
        /* The Legacy OMS accepts input in XML format */
        List<Item> cart = new List<Item>();
        List<Payment> payments = new List<Payment>();
        public void addItem(Item itemXml)
        {
            cart.Add(itemXml);
            Console.WriteLine(itemXml.getName() + " " + itemXml.getPrice());
        }
        public void makePayment(Payment paymentXml)
        {
            payments.Add(paymentXml);
            paymentXml.pay();
        }
    }
}