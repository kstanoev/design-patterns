
### Adapter

#### When to use

To wrap an existing class with a new interface.
To perform impedance matching

#### Intent

Convert the interface of a class into another interface clients expect. Adapter lets classes work together that couldn't otherwise because of incompatible interfaces.

#### Components

Target - defines the domain-specific interface that Client uses.
Adapter - adapts the interface Adaptee to the Target interface.
Adaptee - defines an existing interface that needs adapting.
Client - collaborates with objects conforming to the Target interface.

#### Implementation

Assume that you have an e-commerce application which is serving your customers for a long time. This e-commerce application is using a Legacy Order Management System (OMS). Due to the high maintenance cost and degraded performance of the legacy OMS software, you have decided to use a cheap and efficient OMS software which is readily available in the market. However, you realize that the interfaces are different in the new software and it requires a lot of code change in the existing e-commerce application.
Adapter design pattern can be very useful in these situations. Instead of modifying your e-commerce application to use the new interfaces, you can write a 'wrapper' class that acts as a bridge between your e-commerce application and the new OMS software. With this approach, the e-commerce application can still use the old interface.
Adapter design pattern can be implemented in two ways. One using the inheritance method (Class Adapter) and second using the composition (Object Adapter). The following example depicts the implementation of Object adapter.

1. Below is the code that uses the LegacyOMS.

```csharp
class Item
```

```csharp
class Payment
```

```csharp
class LegacyOMS
```

2. The client code.

```csharp
class AdapterClient
```

3. When the OMS needs to be swapped, you can simply create an Adapter class with same interface that the client uses. This adapter/wrapper class "maps" the client interface to the adaptee (New OMS) interface.

```csharp
class NewOMS
```

```csharp
class OMSAdapter
```

4. The new client code. The client interacts in the same way as before.

```csharp
class AdapterClient
```

Output

```
Italian Pizza   6.99
Wine            9.99
Beer            5.99
Red Apple       1.49
Almonds        11.99
CASH    20.0$
CREDIT  10.0$
DEBIT   10.0$
```

#### Benefits

Class adapter can override adaptee's behavior.
Objects adapter allows a single adapter to work with many adaptees.
Helps achieve reusability and flexibility.
Client class is not complicated by having to use a different interface and can use polymorphism to swap between different implementations of adapters.

#### Drawbacks

Object adapter involves an extra level of indirection.

#### Real World Examples

Power adapters
Memory card adapters

#### Software Examples

Wrappers used to adopt 3rd parties libraries and frameworks.

#### .NET Examples
ToList, ToArray???

One of the strengths of the .NET Framework is backward compatibility. From .NET-based code you can easily call legacy COM objects and vice versa. In order to use a COM component in your project, all you have to do is add a reference to it via the Add Reference dialog in Visual Studio .NET. Behind the scenes, Visual Studio® .NET invokes the tlbimp.exe tool to create a Runtime Callable Wrapper (RCW) class, contained in an interop assembly. 

