﻿using System;
using System.Collections.Generic;
namespace structural_composite
{
	class Program
	{
		static void Main(string[] args)
		{
			/*
			root
				file1.txt
				sub-root
					index.html
					test.txt
				cat.jpg
			*/
			var root = new Directory("root");
			root.Add(new File("file1.txt"));
			var subDir = new Directory("sub-dir");
			subDir.Add(new File("index.html"));
			subDir.Add(new File("test.txt"));
			root.Add(subDir);
			root.Add(new File("cat.jpg"));
			root.Print(0);
		}
	}
	public interface IFileSystemItem
	{
		string Name { get; }
		void Print(int level);
	}
	public class File : IFileSystemItem
	{
		public File(string name)
		{
			this.Name = name;
		}
		public string Name { get; }
		public void Print(int level)
		{
			string indent = new string('\t', level);
			Console.WriteLine($"{indent}{this.Name}");
		}
	}
	public class Directory : IFileSystemItem
	{
		private List<IFileSystemItem> children = new List<IFileSystemItem>();
		public Directory(string name)
		{
			this.Name = name;
		}

		public string Name { get; private set; }

		public void Add(IFileSystemItem child)
		{
			this.children.Add(child);
		}
		public void Print(int level)
		{
			string indent = new string('\t', level);
			Console.WriteLine($"{indent}{this.Name}");
			foreach (var item in this.children)
			{
				item.Print(level + 1);
			}
		}
	}
}