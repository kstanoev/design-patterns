
### Composite

#### When to use

To have a hierarchical collection of primitive and composite entities.

To create a structure in a way that the objects in the structure can be treated the same way.

#### Intent

Compose objects into tree structures to represent part-whole hierarchies. Composite lets clients treat individual objects and compositions of objects uniformly.

#### Components

An interface for all objects in the composition

A leaf element which is the building block of the composition

A composite element which can contain leaf elements and/or composites

#### Implementation

Typical example is the File system which contains directories and files. A directory can contain files or sub directories but both have to be handled in the same way.
In the following example, boxes and products are implemented using the Composite pattern. A box can contain many products and boxes. We need a common way to interact with both entities.

1. Create a common interface.

```csharp
interface ITask
{
    string Title { get; }
}
```

2. Create the leaf object that implements the interface.

```csharp
class Task : ITask
{
    private string title;
    public Task(string title)
    {
        this.title = title;
    }
    public string Title
    {
        get
        {
            return this.title;
        }
    }
}
```

3. Create composite objects that implement the interface.

```csharp
class Bug : ITask
{
    private int severity;
    private string title;
    public Bug(string title, int severity)
    {
        this.title = title;
        this.severity = severity;
    }
    public string Title
    {
        get
        {
            return this.title;
        }
    }
}
```
```csharp
class Story : ITask
{
    private string title;
    private IList<ITask> tasks;
    public Story(string title)
    {
        this.title = title;
        this.tasks = new List<ITask>();
    }
    public string Title
    {
        get
        {
            return this.title;
        }
    }
    public void AddTask(ITask item)
    {
        this.tasks.Add(item);
    }
    public void RemoveTask(ITask item)
    {
        this.tasks.Remove(item);
    }
    public IEnumerable<ITask> GetAllTasks()
    {
        return this.tasks;
    }
}
```

4. The client code. The client creates leaf elements and composite elements and interact with them using the common interface.

```csharp
// TODO
class CompositeClient
```

Output

```
// TODO
```

#### Benefits

Simplifies the representation of part-whole hierarchies.

Clients can treat all objects in the composite structure uniformly.

#### Drawbacks

Strict restrictions need to be enforced otherwise the tree structure may become overly generalized.

#### Real World Examples

Organization structure with manager and reportees. The reportees could be managers who may have their own reportees.

#### Software Examples

File system (directories and files)

#### .NET Examples

java.awt Container#add(Component) (practically all over Swing thus)
javax.faces.component UIComponent#getChildren() (practically all over JSF UI thus)
