﻿using System;
using System.Collections.Generic;
namespace structural_composite.demo
{
	class Program
	{
		void Main(string[] args)
		{
			var root = new Directory("root");
			root.Add(new File("file1.txt"));
			var subRoot = new Directory("sub-root");
			subRoot.Add(new File("index.html"));
			subRoot.Add(new File("foo.txt"));
			root.Add(subRoot);
			root.Add(new File("admin.cs"));
			root.Print(0);
		}
	}
	interface IFileSystemItem
	{
		string Name { get; }
	}
	class File : IFileSystemItem
	{
		public File(string name)
		{
			this.Name = name;
		}
		public string Name { get; }
	}
	class Directory : IFileSystemItem
	{
		private List<IFileSystemItem> children 
			= new List<IFileSystemItem>();
		
		public Directory(string name)
		{
			this.Name = name;
		}
		public string Name { get; }

		public void Add(IFileSystemItem child)
		{
			this.children.Add(child);
		}

		public void Print(int level)
		{
			string indentation = new string('\t', level);
			Console.WriteLine($"{indentation}{this.Name}");
			foreach (var item in this.children)
			{
				if (item is Directory)
				{
					(item as Directory).Print(level + 1);
				}
				else
				{
					indentation = new string('\t', level + 1);
					Console.WriteLine($"{indentation}{item.Name}");
				}
			}
		}
	}
}