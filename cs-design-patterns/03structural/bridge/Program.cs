﻿using System;
namespace structural_bridge
{
    class Program
    {
        static void Main(string[] args)
        {
            Shape[] shapes = {
                new Circle(new Red()),
                new Square(new Red()),
                new Rectangle(new Green())
            };
            foreach (Shape shape in shapes)
            {
                shape.draw();
            }
        }
    }
    interface Color
    {
        void applyColor();
    }
    class Red : Color
    {
        public void applyColor()
        {
            Console.WriteLine("Red");
        }
    }
    class Green : Color
    {
        public void applyColor()
        {
            Console.WriteLine("Green");
        }
    }
    abstract class Shape
    {
        public Shape(Color c)
        {
            Color = c;
        }
        public Color Color { get; }
        public abstract void draw();
    }
    class Circle : Shape
    {
        public Circle(Color color) : base(color) { }
        public override void draw()
        {
            Console.Write("Draw Circle in ");
            base.Color.applyColor();
        }
    }
    class Rectangle : Shape
    {
        public Rectangle(Color color) : base(color) { }
        public override void draw()
        {
            Console.Write("Draw Rectangle in ");
            base.Color.applyColor();
        }
    }
    class Square : Shape
    {
        public Square(Color color) : base(color) { }
        public override void draw()
        {
            Console.Write("Draw Square in ");
            base.Color.applyColor();
        }
    }
}