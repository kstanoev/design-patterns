
### Facade

#### When to use

To provide a simplified interface to the overall functionality of a complex subsystem.
To promote subsystem independence and portability.

#### Intent

Provide a unified interface to a set of interfaces in a subsystem. Facade defines a higher-level interface that makes the subsystem easier to use.

#### Implementation

Assume that we have set of interfaces for a system that includes many subsystems. The client application can use these interfaces to perform the required operation. But when the complexity increases, client application will find it difficult to manage it. By using the Facade pattern, we can hide the complexities of the system and provide an interface to the client using which the client can access the system.

1. A typical example for the Facade pattern is the wedding planner who hides from you the complexity of a large subsystem. The wedding planner orders flowers, makes reservations, organizes everything for you.

```csharp
class Hall
```
```csharp
class Restaurant
```
```csharp
class Photographer
```
```csharp
class Vehicle {

  public void reserve() {
    Console.WriteLine("Reserve vehicle");
  }

}
```
```csharp
class WeddingPlanner
```

2. The client code. The client contacts the facade class to perform the required operation.

```csharp
class FacadeClient
```

Output

```
Book Marriage Hall
Order food
Book photographer
Reserve vehicle
```

#### Benefits

Number of objects the client interact with is minimal which reduces the compilation complexity.
Promotes loose coupling.
Facade still allows the client to use the subsystem interfaces.

#### Drawbacks

One more layer is introduced in the system which may impact the performance.

#### Real World Examples

Customer Support Desk which hides all complexities of the system that involves various departments.
Event Planner who does everything including like making reservations, organizing activities, etc.

#### .NET Examples

javax.faces.context.FacesContext, it internally uses among others the abstract/interface types LifeCycle, ViewHandler, NavigationHandler and many more without that the enduser has
javax.faces.context.ExternalContext, which internally uses ServletContext, HttpSession, HttpServletRequest, HttpServletResponse, etc.
