﻿using System;
namespace structural_facade
{
    class Program
    {
        static void Main(string[] args)
        {
            WeddingPlanner planner = new WeddingPlanner();
            planner.Organize();
        }
    }
    class Hall
    {
        public void Book()
        {
            Console.WriteLine("Book Marriage Hall");
        }
    }
    class Restaurant
    {
        public void Order()
        {
            Console.WriteLine("Order food and beverages");
        }
    }
    class Photographer
    {
        public void Hire()
        {
            Console.WriteLine("Hire a wedding photographer");
        }
    }
    class Vehicle
    {
        public void Rent()
        {
            Console.WriteLine("Rent a wedding vehicle");
        }
    }
    class WeddingPlanner
    {
        /* Facade class to hide the complexity */
        private Hall hall;
        private Restaurant restaurant;
        private Photographer photographer;
        private Vehicle limousine;
        public WeddingPlanner()
        {
            this.hall = new Hall();
            this.photographer = new Photographer();
            this.restaurant = new Restaurant();
            this.limousine = new Vehicle();
        }
        /* simplified interface exposed to the client */
        public void Organize()
        {
            this.hall.Book();
            this.restaurant.Order();
            this.photographer.Hire();
            this.limousine.Rent();
        }
    }
}