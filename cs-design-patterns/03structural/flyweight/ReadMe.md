
### Flyweight

#### When to use

To improve the performance when large number of objects need to be created.
When most of the object attributes can be made external and shared.

#### Intent

Use sharing to support large numbers of fine-grained objects efficiently.

#### Implementation

Assume that you want to create a car racing game where there will be one player, few opponents, and a bunch of traffic vehicles. These traffic vehicles need to appear throughout the game and there can be hundreds of such vehicles appearing during the course of the race. All these traffic vehicles have a common mission (that is creating traffic on the road) but the type, color, size of the vehicles can vary. Creating each and every traffic vehicle will increase the load on the memory. Flyweight pattern can be applied here to improve the performance and reduce the memory usage. It is achieved by segregating object properties into two types: intrinsic and extrinsic.
In a race, a traffic vehicle needs to appear only for a shorter time. So, it is not required to create all the traffic vehicles at once. Instead, a bunch of traffic vehicles can be created. These vehicles will have intrinsic state like the task and vehicle id. Other properties like the vehicle type, color, size, etc. can be made extrinsic so that different types of vehicles can be created based on the input. When a traffic vehicle needs to appear, it is retrieved from the vehicle pool and the extrinsic properties are applied to change the appearance of the vehicle. When the traffic vehicle goes out of the scene, it can be returned to the pool so that it can be reused.

1. Create the vehicle class. Intrinsic and extrinsic characteristics are carefully chosen.

```csharp
class Vehicle
```

2. Create the vehicle factory class. A bunch of vehicles are created and added to the vehicle pool. When a traffic vehicle is requested, it is retrieved from the pool and the required properties are set.

```csharp
class VehicleFactory
```

3. The client code. The client requests a vehicle by passing the required extrinsic properties.

```csharp
class FlyweightClient
```

Output

```
vehicle1->v1-truck-red-50mph-2seconds
vehicle2->v2-bus-blue-30mph-2seconds
vehicle3->v3-bus-blue-80mph-4seconds
vehicle4->v4-truck-red-30mph-2seconds
vehicle5->v5-car-red-80mph-3seconds
v4->out
v1->out
v2->out
vehicle6->v1-truck-red-50mph-5seconds
vehicle7->v2-car-red-50mph-5seconds
vehicle8->v4-bus-red-30mph-4seconds
v5->out
vehicle9->v5-bus-blue-50mph-1seconds
v3->out
vehicle10->v3-car-green-30mph-4seconds
v5->out
vehicle11->v5-car-blue-80mph-3seconds
v4->out
vehicle12->v4-truck-blue-80mph-2seconds
v1->out
v2->out
v5->out
vehicle13->v1-car-red-50mph-2seconds
vehicle14->v2-truck-blue-50mph-2seconds
vehicle15->v5-car-green-30mph-5seconds
v3->out
v4->out
vehicle16->v3-car-red-30mph-2seconds
vehicle17->v4-truck-red-80mph-4seconds
v1->out
v2->out
vehicle18->v1-truck-red-30mph-2seconds
vehicle19->v2-car-blue-50mph-5seconds
v3->out
vehicle20->v3-bus-blue-50mph-4seconds
v1->out
v5->out
v4->out
v2->out
v3->out
```

#### Benefits

The total number of instances can be reduced
Objects sharing reduces the total memory used

#### Drawbacks

May introduce run-time costs associated with transferring, finding, and/or computing extrinsic state, especially if it was formerly stored as intrinsic state.

#### .NET Examples

java.lang.Integer valueOf(int) (also on Boolean, Byte, Character, Short, Long and BigDecimal)
