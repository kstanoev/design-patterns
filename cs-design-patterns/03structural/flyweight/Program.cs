﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
namespace structural_flyweight
{
    class Program
    {
        static Random r = new Random();
        private static String[] types = { "bus", "truck", "car" };
        private static String[] colors = { "red", "green", "blue" };
        private static int[] speeds = { 50, 30, 80 };
        static void Main(string[] args)
        {
            VehicleFactory factory = new VehicleFactory();
            /* Create traffic vehicles */
            for (int i = 0; i < 20; i++)
            {
                Vehicle v = factory.getVehicle(getRandType(),
                    getRandColor(), getRandSpeed(), (r.Next(5) + 1));
                if (v != null)
                {
                    /* free vehicle object found, add to the traffic */
                    Console.Write("vehicle" + (i + 1));
                    v.addToTraffic();
                }
                else
                {
                    i--;
                    /* all objects in use, wait and try again */
                    Thread.Sleep(1000);
                }
            }
        }
        public static String getRandType()
        {
            return types[r.Next(types.Length)];
        }
        public static String getRandColor()
        {
            return colors[r.Next(colors.Length)];
        }
        public static int getRandSpeed()
        {
            return speeds[r.Next(speeds.Length)];
        }
    }
    class Vehicle
    {
        private String name;
        private readonly String task;
        private String type;
        private String color;
        private int speed;
        private bool active;
        private int duration;
        public Vehicle(String name)
        {
            /* Intrinsic state of the object */
            task = "Obstruct the racers";
            this.name = name;
        }
        public void setProperties(String type, String color, int speed, int duration)
        {
            /* Extrinsic state of the object */
            this.type = type;
            this.color = color;
            this.speed = speed;
            this.duration = duration;
        }
        public bool isActive()
        {
            return active;
        }
        public void addToTraffic()
        {
            /* add the vehicle to the traffic */
            Console.WriteLine("->" + name + "-" + type + "-" + color + "-" + speed +
                "mph-" + duration + "seconds");
            /* Create a timer task to take the vehicle out
             * from the traffic after the duration */
            var aTimer = new System.Timers.Timer(duration * 1000);
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += (o, e) =>
            {
                active = false;
                Console.WriteLine(name + "->out");
            };
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
            active = true;
        }
    }
    class VehicleFactory
    {
        private List<Vehicle> pool = new List<Vehicle>();
        public VehicleFactory()
        {
            for (int i = 0; i < 5; i++)
            {
                /* Create traffic vehicles and add it to the vehicle pool */
                pool.Add(new Vehicle("v" + (i + 1)));
            }
        }
        /* Retrieve a vehicle from the pool and set the properties */
        public Vehicle getVehicle(String type, String color, int speed, int duration)
        {
            foreach (Vehicle v in pool)
            {
                if (!v.isActive())
                {
                    /* set the extrinsic properties */
                    v.setProperties(type, color, speed, duration);
                    return v;
                }
            }
            return null;
        }
    }
}