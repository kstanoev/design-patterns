﻿using System;
using System.Text;
namespace creational_prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Create base Machine Images */
            MachineImage linuxVM = new MachineImage("Linux", "Symantec");
            MachineImage windowsVM = new MachineImage("Windows", "Norton");
            /* Clone Linux VM and Install Web Server */
            MachineImage webServer = linuxVM.Clone();
            webServer.InstallSoftware(" + Web Server S/W");
            /* Create a copy of the Web Server and
             * install Application server on top of it */
            MachineImage webAppServer = webServer.Clone();
            webAppServer.InstallSoftware(" + App Server S/W");
            /* Clone Linux VM and Install DB Server */
            MachineImage dbServer = linuxVM.Clone();
            dbServer.InstallSoftware(" + Database Server S/W");
            /* Create a test machine from Windows Image */
            MachineImage testMachine = windowsVM.Clone();
            Console.Write("Web Server Configuration: ");
            webServer.PrintInstalledSoftware();
            Console.Write("App Server Configuration: ");
            webAppServer.PrintInstalledSoftware();
            Console.Write("DB Server Configuration: ");
            dbServer.PrintInstalledSoftware();
            Console.Write("Test Machine Configuration: ");
            testMachine.PrintInstalledSoftware();
        }
    }
    class MachineImage
    {
        StringBuilder image;
        public MachineImage(string os, string antivirusSW)
        {
            image = new StringBuilder();
            image.Append(os)
                .Append(" + " + antivirusSW);
        }
        private MachineImage(string sw)
        {
            image = new StringBuilder(sw);
        }
        public void InstallSoftware(string sw)
        {
            image.Append(sw);
        }
        public void PrintInstalledSoftware()
        {
            Console.WriteLine(image);
        }
        public MachineImage Clone()
        {
            return new MachineImage(this.image.ToString());
        }
    }
}