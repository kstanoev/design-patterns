
### Prototype

#### When to use

To improve the performance when object creation is costly and time consuming.
To simplify and optimize multiple objects creation that will have mostly the same data

#### Intent

Specify the kinds of objects to create using a prototypical instance, and create new objects by copying this prototype.

#### Components

A class that : Cloneable interface (public)

#### Implementation

Most of the cloud platforms behave like Prototype pattern to create instances quickly upon requests. This is achieved with the help of pre-installed Machine Images. When a customer wants a Windows or Linux instance, the cloud software just loads the already created machine image on to a server hardware (rather than going through the complete installation process). This saves a lot of time.

1. Create a base machine image by installing the specified OS and antivirus software. Provide a 'clone' method so that client can create an object without using a time consuming 'new' operator every time.

```csharp
class MachineImage
```

2. The client code. The client creates a base image and clones it to create other images.

```csharp
class PrototypeClient
```

Output

```
Web Server Configuration: Linux OS + Antivirus S/W + Web Server S/W
App Server Configuration: Linux OS + Antivirus S/W + Web Server S/W + App Server S/W
DB Server Configuration: Linux OS + Antivirus S/W + Database Server S/W
Test Machine Configuration: Linux OS + Antivirus S/W
```

#### Benefits

Performance: Cloning (using MemberwiseClone) is considerably less expensive than creating a new object afresh (with new operator).
Objects can be cloned very dynamically, without any insistence on up-front instantiation. The first created object can be created at any time in the application execution, and further duplication can take place at any time ahead.

#### Drawbacks

Deep copy has to be handled carefully.

#### Real World Examples

Biological Cell splitting

#### Software Examples

Virtual Machine Images - Have one image per OS which has all the required s/w installed.
DVD duplication - Duplication of the master dvd to create several copies.

#### .NET Examples

java.lang.Object clone()
