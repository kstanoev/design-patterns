﻿using System;
using System.Collections.Generic;

namespace creational_singleton.backup
{
	class Program
	{
		/*static*/ void Main(string[] args)
		{
			Logger.Instance.Log("Hello World");
		}
	}
	class Logger
	{
		private List<string> messages = new List<string>();
		private static Logger instance = null;

		private Logger() { }

		public static Logger Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new Logger();
				}
				return instance;
			}
		}

		public void Log(string message)
		{
			this.messages.Add(message);
			Console.WriteLine("Message successfully added.");
		}
	}
}