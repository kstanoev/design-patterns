
### Singleton

#### When to use

Application needs “only one instance” of a class.

To have complete control over the instance creation.

#### Intent

Ensure a class has only one instance, and provide a global point of access to it.

#### Components

Singleton Class

#### Implementation

Create a Singleton class with a static instance and a private constructor. Provide a static method to allow access to this 'only' instance.

1. Create a Singleton Class

```csharp
class Singleton
```

2. Access the singleton instance

```csharp
class Demo
```

Output

```
Unique Id of the obj: 1916222108
Unique Id of the obj: 1916222108
```

#### Benefits

Controlled instantiation
Supports both EAGER and LAZY initializations
Singletons can be converted to Multitons (to support limited number of instances identified by keys)

#### Drawbacks

Singleton is often seen as 'not-so-good' design as it resembles global variables

Special handling is required if the Singleton object needs to be deleted

Singletons that maintain global state may cause issues

#### Real World Examples

The Office of the President (there can be only one President at any given time)

#### Software Examples

Logger classes

Window Manager

Printer Spooler

#### .NET Examples

java.lang.Runtime.getRuntime( )
java.awt.Toolkit.getDefaultToolkit( )
java.awt.Desktop.getDesktop( )
java.util.logging.LogManager.getLogManager( )
java.lang.System#getSecurityManager( )
