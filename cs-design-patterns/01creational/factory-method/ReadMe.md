
### Factory Method

#### When to use

To enforce coding for interface rather than implementation

To transfer the responsibility of instantiation from the client class to the factory method

To decouple the implementation from the client program

#### Intent

Define an interface for creating an object, but let subclasses decide which class to instantiate. Factory Method lets a class defer instantiation to subclasses.

#### Components

An Interface (or) Abstract class (public)
Set of implementation subclasses (private)
A Factory Method (public)

#### Implementation

1. Create an interface. Clients can code for this interface without worrying about the internal implementation.

```csharp
interface Vehicle
```

2. Create a set of implementation subclasses. Constructors are protected to prohibit instantiations in clients modules using the 'new' operator.

```csharp
class Car
```

```csharp
class Truck
```

```csharp
class Motorcycle 
```

3. Create a class with method 'getVehicle()'. Clients can use this method to create an object instead of using 'new' operator.

```csharp
class VehicleFactory
```

4. The client code. Client knows only the factory method and the interface. Client code does not use 'new' hence decoupled from implementation

```csharp
class FactoryMethodClient
```

Output

```
[input1]
    MotorCycle
[output1]
    Designing Motorcycle
    Manufacturing Motorcycle
```

```
[input2]
    Car
[output2]
    Designing Car
    Manufacturing Car
```

```
[input3]
    Bus
[output3]
    Invalid vehicle type entered!
```

#### Benefits

Loose coupling allows changing the internals without impacting the customer code
Factory method provides a single point of control for multiple products
Number of instances and their reusability can be controlled with Singleton or Multiton

#### Drawbacks

An extra level of abstraction makes the code more difficult to read

#### Real World Examples

Renting Vehicles. Customer needs to specify only the type of vehicle (car, truck, etc.) that is needed. Customer need not know about the internal details of the vehicle.

#### Software Examples

Memcache
Filecache
Code for SQL standard without worrying about the underlying DB

#### .NET Examples

System.Net.WebRequest
