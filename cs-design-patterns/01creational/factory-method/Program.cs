﻿using System;
namespace creational_factory_method
{
    class Program
    {
        static void Main(string[] args)
        {
            string vehicleType = Console.ReadLine().ToLower();
            /* Create a factory instance */
            VehicleFactory factory = new VehicleFactory();
            try
            {
                // Create an appropriate vehicle based on the input
                Vehicle vehicle = factory.GetVehicle(vehicleType);
                // Design and manufacture the vehicle
                vehicle.Design();
                vehicle.Manifacture();
            }
            catch (Exception)
            {
                Console.WriteLine("Invalid vehicle type entered!");
            }
        }
    }
    interface Vehicle
    {
        void Design();
        void Manifacture();
    }
    class Car : Vehicle
    {
        public void Design()
        {
            Console.WriteLine("Designing Car");
        }
        public void Manifacture()
        {
            Console.WriteLine("Manufacturing Car");
        }
    }
    class Truck : Vehicle
    {
        public void Design()
        {
            Console.WriteLine("Designing Truck");
        }
        public void Manifacture()
        {
            Console.WriteLine("Manufacturing Truck");
        }
    }
    class Motorcycle : Vehicle
    {
        public Motorcycle()
        {
            /* constructor is protected.
               clients need to use the factory method */
        }
        public void Design()
        {
            Console.WriteLine("Designing Motorcycle");
        }
        public void Manifacture()
        {
            Console.WriteLine("Manufacturing Motorcycle");
        }
    }
    class VehicleFactory
    {
        /* This is the factory method exposed to the client.
           Client requests for an object by passing the type.
           Client does not need to know about which & how object
           is created internally.
           */
        public Vehicle GetVehicle(string vehicleType)
        {
            if (vehicleType == null)
            {
                return null;
            }
            Vehicle vehicle = null;
            switch (vehicleType)
            {
                case "car":
                    vehicle = new Car();
                    break;
                case "truck":
                    vehicle = new Truck();
                    break;
                case "motorcycle":
                    vehicle = new Motorcycle();
                    break;
                default:
                    throw new Exception("Vehicle type not found");
            }
            return vehicle;
        }
    }
}