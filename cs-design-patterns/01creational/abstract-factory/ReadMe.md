### Abstract Factory

#### When to use

To support families of related or dependent objects.

To encapsulate platform dependencies to make an application portable.

To prevent client code from using the 'new' operator.

To easily swap the underlying platform with minimal changes.

#### Intent

Provide an interface for creating families of related or dependent objects without specifying their concrete classes.

#### Components

An Abstract Factory class (public)

Factory Implementations for various familes (protected)

Interfaces for various products (public)

Set of product implementations for various families (protected)

#### Implementation

1. Define interfaces for different types products/objects. Each family will have all these parts.

```csharp
interface Engine
{
    void Design();
    void Manufacture();
    void Test();
}
```

```csharp
interface Tyre
{
    void Design();
    void Manufacture();
}
```

2. Create sets of implementation subclasses for the above interfaces. Classes are access protected to prohibit instantiations in client modules using the 'new' operator.

```csharp
class CarEngine : Engine
{
    public void Design()
    {
        Console.WriteLine("Designing Car Engine");
    }
    public void Manufacture()
    {
        Console.WriteLine("Manufacturing Car Engine");
    }
    public void Test()
    {
        Console.WriteLine("Testing Car Engine");
    }
}
```
```csharp
class TruckEngine : Engine
{
    public void Design()
    {
        Console.WriteLine("Designing Truck Engine");
    }
    public void Manufacture()
    {
        Console.WriteLine("Manufacturing Truck Engine");
    }
    public void Test()
    {
        Console.WriteLine("Testing Truck Engine");
    }
}
```
```csharp
class CarTyre : Tyre
{
    public void Design()
    {
        Console.WriteLine("Designing Car Tyre");
    }
    public void Manufacture()
    {
        Console.WriteLine("Manufacturing Car Tyre");
    }
}
```
```csharp
class TruckTyre : Tyre
{
    public void Design()
    {
        Console.WriteLine("Designing Truck Tyre");
    }
    public void Manufacture()
    {
        Console.WriteLine("Manufacturing Truck Tyre");
    }
}
```

3. Create a Abstract Factory class with factory method 'getFactory()'. Clients can use this method to get an object the required factory. This example uses both Singleton and Factory Method patterns for better design.

```csharp
abstract class Factory
{
    /* Singleton Factory objects */
    private static Factory carFactory = null;
    private static Factory truckFactory = null;
    public abstract Engine GetEngine();
    public abstract Tyre GetTyre();
    /*
    * This is the factory method exposed to the client.
    * Client requests for a factory instance by passing the type.
    * Client does not need to know about which & how
    * object is created internally.
    */
    public static Factory GetFactory(String vehicleType)
    {
        if (vehicleType == null)
        {
            return null;
        }
        Factory factory = null;
        switch (vehicleType)
        {
            case "car":
                if (carFactory == null)
                    carFactory = new CarFactory();
                factory = carFactory;
                break;
            case "truck":
                if (truckFactory == null)
                    truckFactory = new TruckFactory();
                factory = truckFactory;
                break;
            default:
                throw new Exception("Unknown vehicle");
        }
        return factory;
    }
}
```

4. Create Factory implementations. Classes are protected to prohibit direct access in client modules.

```csharp

class CarFactory : Factory
{
    public override Engine GetEngine()
    {
        return new CarEngine();
    }
    public override Tyre GetTyre()
    {
        return new CarTyre();
    }
}
class TruckFactory : Factory
{
    public override Engine GetEngine()
    {
        return new TruckEngine();
    }
    public override Tyre GetTyre()
    {
        return new TruckTyre();
    }
}
```

5. The client code. Client is exposed to only the Abstract Factory class and the interfaces.

```csharp
class Program
{
    static void Main(string[] args)
    {
        Console.Write("Please enter a command: ");
        string command = "";
        while (command != "end")
        {
            command = Console.ReadLine();
            string vehicleType = command.ToLower();
            /* Get the factory instance */
            Factory factory;
            try
            {
                factory = Factory.GetFactory(vehicleType);
                /* Get the Engine from the factory */
                Engine engine = factory.GetEngine();
                engine.Design();
                engine.Manufacture();
                engine.Test();
                /* Get the Tyre from the factory */
                Tyre tyre = factory.GetTyre();
                tyre.Design();
                tyre.Manufacture();
            }
            catch (Exception)
            {
                Console.WriteLine("Invalid vehicle type entered!");
            }
        }
    }
}
```

Output

```
[input1]
    Car
[output1]
    Designing Car Engine
    Manufacturing Car Engine
    Testing Car Engine
    Designing Car Tyre
    Manufacturing Car Tyre
```

```
[input2]
    Bus
[output2]
    Invalid vehicle type entered!
```

#### Benefits

Loosely coupled code.

Abstract Factory provides a single point of access for all products in a family.

New product family can be easily supported.

#### Drawbacks

More layers of abstraction increases complexity.

If there are any changes to any underlying detail of one factory, the interface might need to be modified for all the factories.

#### Real World Examples

Providing data access to two different data sources (e.g. a SQL Database and a XML file). You have two different data access classes (a gateway to the datastore). Both inherit from a base class that defines the common methods to be implemented (e.g. Load, Save, Delete). Which data source shall be used shouldn't change the way client code retrieves it's data access class. Your Abstract Factory knows which data source shall be used and returns an appropriate instance on request. The factory returns this instance as the base class type.

#### Software Examples

Dependency Injection

The System.Convert class contains a host of static methods that work like this. To convert an integer to a Boolean, for example, you can call Convert.ToBoolean and pass in the integer. The return value of this method call is a new Boolean set to "true" if the integer was non-zero and "false" otherwise. The Convert class creates the Boolean for you with the correct value. Other type conversion methods work similarly. The Parse methods on Int32 and Double return new instances of those objects set to the appropriate value given only a string.