﻿using System;
namespace creational_abstract_factory
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Please enter a vehicle: ");
			string vehicleType = Console.ReadLine().ToLower();
			try
			{
				/* Get the factory instance */
				Factory factory = Factory.GetFactory(vehicleType);
				/* Get the Engine from the factory */
				Engine engine = factory.GetEngine();
				engine.Design();
				engine.Assemble();
				engine.StressTest();
				/* Get the Tyre from the factory */
				Tires tires = factory.GetTires();
				tires.PrepareRubberMixture();
				tires.TracktionTest();
			}
			catch (Exception)
			{
				Console.WriteLine("Invalid vehicle type entered!");
			}
		}
	}

	interface Tires
	{
		void PrepareRubberMixture();
		void TracktionTest();
	}
	interface Engine
	{
		void Design();
		void Assemble();
		void StressTest();
	}
	class CarEngine : Engine
	{
		public void Design()
		{
			Console.WriteLine("Designing Car Engine");
		}
		public void Assemble()
		{
			Console.WriteLine("Assembling Car Engine");
		}
		public void StressTest()
		{
			Console.WriteLine("Testing Car Engine");
		}
	}
	class TruckEngine : Engine
	{
		public void Design()
		{
			Console.WriteLine("Designing Truck Engine");
		}
		public void Assemble()
		{
			Console.WriteLine("Manufacturing Truck Engine");
		}
		public void StressTest()
		{
			Console.WriteLine("Testing Truck Engine");
		}
	}
	class CarTires : Tires
	{
		public void PrepareRubberMixture()
		{
			Console.WriteLine("Preparing rubber mixture for Car Tires");
		}
		public void TracktionTest()
		{
			Console.WriteLine("Testing Car Tires on slippery surface.mo");
		}
	}
	class TruckTires : Tires
	{
		public void PrepareRubberMixture()
		{
			Console.WriteLine("Preparing rubber mixture for Truck Tires.");
		}
		public void TracktionTest()
		{
			Console.WriteLine("Testing Truck Tires on rough terrain.");
		}
	}
	abstract class Factory
	{
		/* Singleton Factory objects */
		private static Factory carFactory = null;
		private static Factory truckFactory = null;
		public abstract Engine GetEngine();
		public abstract Tires GetTires();
		/*
		 * This is the factory method exposed to the client.
		 * Client requests for a factory instance by passing the type.
		 * Client does not need to know about which & how
		 * object is created internally.
		 */
		public static Factory GetFactory(string vehicleType)
		{
			switch (vehicleType)
			{
				case "car":
					if (carFactory == null)
						carFactory = new CarFactory();
					return carFactory;
				case "truck":
					if (truckFactory == null)
						truckFactory = new TruckFactory();
					return truckFactory;
				default:
					throw new Exception("Unknown vehicle");
			}
		}
	}
	class CarFactory : Factory
	{
		public override Engine GetEngine()
		{
			return new CarEngine();
		}
		public override Tires GetTires()
		{
			return new CarTires();
		}
	}

	class TruckFactory : Factory
	{
		public override Engine GetEngine()
		{
			return new TruckEngine();
		}
		public override Tires GetTires()
		{
			return new TruckTires();
		}
	}
}