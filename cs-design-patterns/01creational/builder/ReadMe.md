
### Builder

#### When to use

To avoid dealing with inconsistent object when the object needs to be created over several steps.

To avoid too many constructor arguments.

To construct an object that should be immutable.

To encapsulate the complete creation logic.

#### Intent

Separate the construction of a complex object from its representation so that the same construction process can create different representations.

#### Components

The Builder class specifies an abstract interface for creating parts of a Product object.

The ConcreteBuilder constructs and puts together parts of the product by implementing the Builder interface. It defines and keeps track of the representation it creates and provides an interface for saving the product.

The Director class constructs the complex object using the Builder interface.

The Product represents the complex object that is being built.

#### Implementation

1. Define the Product (WorkItem) that gets assembled in the builder pattern.

```csharp
class WorkItem
{
	private string title;
	private Dictionary<string, string> attributes = new Dictionary<string, string>();
	public void SetTitle(string title)
	{
		this.title = title;
	}
	public bool AddAttribute(string attribute, string value)
	{
		return this.attributes.TryAdd(attribute, value);
	}
	public override string ToString()
	{
		StringBuilder builder = new StringBuilder($"WorkItem:\n{this.title}");
		foreach (var attribute in this.attributes)
		{
			builder.Append($"\n{attribute.Key} : {attribute.Value}");
		}
		return builder.ToString();
	}
}
```

2. Define the Builder interface (or abstract class) along with Concrete Builders. The Builder interface contains methods for the step by step construction of the product. It also has a build method for retrieving the product object.

```csharp
interface IWorkItemBuilder
{
	IWorkItemBuilder SetTitle(string title);
	IWorkItemBuilder SetDefaultAttributes();
	WorkItem Build();
}
```

3. Concrete Builders implement the IWorkItemBuilder interface. A Concrete Builder is responsible for creating and assembling a WorkItem object. Different Concrete Builders create and assemble WorkItem objects differently.

```csharp
class BugBuilder : IWorkItemBuilder
{
	private WorkItem workItem;
	public BugBuilder()
	{
		workItem = new WorkItem();
	}
	public IWorkItemBuilder SetTitle(string title)
	{
		workItem.SetTitle($"BUG: {title}");
		return this;
	}
	public IWorkItemBuilder SetDefaultAttributes()
	{
		workItem.AddAttribute("severity", "CRITICAL");
		return this;
	}
	public WorkItem Build()
	{
		return workItem;
	}
}
```

```csharp
class StoryBuilder : IWorkItemBuilder
{
	private WorkItem house;
	public StoryBuilder()
	{
		house = new WorkItem();
	}
	public IWorkItemBuilder SetTitle(string title)
	{
		house.SetTitle($"STORY: {title}");
		return this;
	}
	public IWorkItemBuilder SetDefaultAttributes()
	{
		house.AddAttribute("priority", "HIGH");
		return this;
	}
	public WorkItem Build()
	{
		return house;
	}
}
```

4. A Director object is responsible for constructing a Product. It does this via the Builder interface to a Concrete Builder. It constructs a Product via the various Builder methods. The director class ensures that all the required operations are performed before the object is returned to the client in a 'consistent' state.

```csharp
class WorkItemBuildDirector
{
	private IWorkItemBuilder builder;
	public WorkItemBuildDirector()
	{
	}
	public void SetBuilder(IWorkItemBuilder builder)
	{
		this.builder = builder;
	}
	public WorkItem Construct(string title)
	{
		/* call the necessary methods and return the consistent object*/
		return builder.SetTitle(title).SetDefaultAttributes().Build();
	}
}
```

5. The client code. The Client uses different builder objects to create different types of products. However, the construction process is same.

```csharp
class Program
{
	static void Main(string[] args)
	{
		IWorkItemBuilder builder = null;
		WorkItemBuildDirector buildDirector = new WorkItemBuildDirector();

		// Construct a bug
		builder = new BugBuilder();
		buildDirector.SetBuilder(builder);
		Console.WriteLine(buildDirector.Construct("Application crashes"));

		// Construct a story
		builder = new StoryBuilder();
		buildDirector.SetBuilder(builder);
		Console.WriteLine(buildDirector.Construct("Enable listing all teams"));
	}
}
```

Output

```
WorkItem:
BUG: Application crashes
severity : CRITICAL

WorkItem:
STORY: Enable listing all teams
priority : HIGH
```

#### Benefits

Construction process can be controlled by the director.

Useful when many operations have to be done to build an object.

Avoids Telescoping Constructor Pattern.

#### Drawbacks

Not suitable if a mutable object is required.

#### Real World Examples

Building a house - We need to tell the architect what all we want as part of the building. The Architect then designs and constructs the building. It will be handed over only when everything is implemented. We do not get a 'partially' built house (which is unsafe).

#### .NET Examples

```csharp
System.Text.StringBuilder
```
