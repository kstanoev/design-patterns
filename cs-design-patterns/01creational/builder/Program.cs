﻿using System;
using System.Collections.Generic;
using System.Text;

namespace creational_builder
{
	class Program
	{
		static void Main(string[] args)
		{
			IWorkItemBuilder builder = null;
			WorkItemBuildDirector buildDirector = new WorkItemBuildDirector();

			// Construct a bug
			builder = new BugBuilder();
			buildDirector.SetBuilder(builder);
			Console.WriteLine(buildDirector.Construct("Application crashes"));

			// Construct a story
			builder = new StoryBuilder();
			buildDirector.SetBuilder(builder);
			Console.WriteLine(buildDirector.Construct("Enable listing all teams"));
		}
	}
	class WorkItem
	{
		private string title;
		private Dictionary<string, string> attributes = new Dictionary<string, string>();
		public void SetTitle(string title)
		{
			this.title = title;
		}
		public bool AddAttribute(string attribute, string value)
		{
			return this.attributes.TryAdd(attribute, value);
		}
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder($"WorkItem:\n{this.title}");
			foreach (var attribute in this.attributes)
			{
				builder.Append($"\n{attribute.Key} : {attribute.Value}");
			}
			return builder.ToString();
		}
	}
	interface IWorkItemBuilder
	{
		IWorkItemBuilder SetTitle(string title);
		IWorkItemBuilder SetDefaultAttributes();
		WorkItem Build();
	}
	class BugBuilder : IWorkItemBuilder
	{
		private WorkItem workItem;
		public BugBuilder()
		{
			workItem = new WorkItem();
		}
		public IWorkItemBuilder SetTitle(string title)
		{
			workItem.SetTitle($"BUG: {title}");
			return this;
		}
		public IWorkItemBuilder SetDefaultAttributes()
		{
			workItem.AddAttribute("severity", "CRITICAL");
			return this;
		}
		public WorkItem Build()
		{
			return workItem;
		}
	}
	class StoryBuilder : IWorkItemBuilder
	{
		private WorkItem house;
		public StoryBuilder()
		{
			house = new WorkItem();
		}
		public IWorkItemBuilder SetTitle(string title)
		{
			house.SetTitle($"STORY: {title}");
			return this;
		}
		public IWorkItemBuilder SetDefaultAttributes()
		{
			house.AddAttribute("priority", "HIGH");
			return this;
		}
		public WorkItem Build()
		{
			return house;
		}
	}
	class WorkItemBuildDirector
	{
		private IWorkItemBuilder builder;
		public WorkItemBuildDirector()
		{
		}
		public void SetBuilder(IWorkItemBuilder builder)
		{
			this.builder = builder;
		}
		public WorkItem Construct(string title)
		{
			/* call the necessary methods and return the consistent object*/
			return builder.SetTitle(title).SetDefaultAttributes().Build();
		}
	}
}