# Design Patterns

## Behavioral Patterns

### Chain of Responsibility

#### When to use

When a request needs to be processed by multiple processors
To achieve loose coupling between sender and receivers

#### Intent

Avoid coupling the sender of a request to its receiver by giving more than one object a chance to handle the request. Chain the receiving objects and pass the request along the chain until an object handles it.

#### Implementation

Assume that you want to implement an ATM Cash Dispenser which dispenses US Dollar notes in all denominations (100$, 50$, 20$, 10$, 5$, 2$, 1\$). This can be achieved by creating dispensers for each of the denominations and linking them to form a chain of dispensers. When the amount to be withdrawn is entered, the requested amount is passed to the first dispenser in the dispenser chain. Once the processor completes the action, the balance amount is passed to the next dispenser in the chain. This process is repeated until the balance becomes 0.

1. Create a CashDispenser class that will take the denomination as the constructor argument. This class has a reference to the next CashDispenser (so that a chain of dispensers can be formed).

```java
package com.jaypeesoft.dp.chainofresponsibility;

public class CashDispenser {

  private int denominator;
  private CashDispenser next = null;

  public CashDispenser(int val) {
    this.denominator = val;
  }

  /* Method to Chain the dispensers */
  public void setNextDispenser(CashDispenser d) {
    if(next == null)
      next = d;
    else
      next.setNextDispenser(d);
  }

  /* Process the request and pass it
     to the next processor if required */
  public void dispense(int amount) {
    if (amount >= denominator) {
      int num = amount / denominator;
      int balance = amount % denominator;
      System.out.println(num + " * "
          + denominator + "$");
      if (balance != 0)
        next.dispense(balance);
    } else {
      next.dispense(amount);
    }
  }

}
```

2. The client code. The client creates dispensers for various denominations and chain them to form a linked list. The client just launches the dispense activity only once and the request is passed through the chain automatically (until it gets processed).

```java
package com.jaypeesoft.dp.client;

import java.util.Scanner;

import com.jaypeesoft.dp.chainofresponsibility.CashDispenser;

public class ChainClient {

    public static void main(String[] args) {

      /* Create a chain of dispensers */
      CashDispenser dispenser = new CashDispenser(100);
      dispenser.setNextDispenser(new CashDispenser(50));
      dispenser.setNextDispenser(new CashDispenser(20));
      dispenser.setNextDispenser(new CashDispenser(10));
      dispenser.setNextDispenser(new CashDispenser(5));
      dispenser.setNextDispenser(new CashDispenser(2));
      dispenser.setNextDispenser(new CashDispenser(1));

      /* Get the amount */
      int amount = 0;
      Scanner in = new Scanner(System.in);
      System.out.print("Enter the amount to withdraw: ");
      amount = in.nextInt();

      /* dispense the amount */
      dispenser.dispense(amount);

      in.close();

    }
}
```

Output

```
[input 1]
Enter the amount to withdraw: 324
[output 1]
3 * 100$
1 * 20$
2 * 2$
```

```
[input 2]
Enter the amount to withdraw: 635
[output 2]
6 * 100$
1 * 20$
1 * 10$
1 * 5$
```

#### Benefits

Client does not need to know about all the processors. It sends the request to the first processor in the chain (launch and leave).
Unlike the Decorator pattern, the chain can be broken at any point to prevent other processors from handling the request.

#### Drawbacks

Since there is no explicit handler/receiver for the request, there is a possibility that the request remains unprocessed.
Incorrectly configured chain may cause some requests to be skipped.

#### Real World Examples

Escalation Matrix
Reimbursement Approval Hierarchy

#### Software Examples

Windows Event Handlers - Events are propagated until it gets processed
Exception Handling - Exceptions are re-thrown if the handler is incapable of handling it.

#### Java SDK Examples

java.util.logging.Logger log()
javax.servlet.Filter doFilter()

### Command

#### When to use

When the executor of the command does not need to know anything at all about what the command is, what context information it needs on or what it does.
To register a callback when some event is triggered.

#### Intent

Encapsulate a request as an object, thereby letting you parameterize clients with different requests, queue or log requests, and support undoable operations.

#### Implementation

Consider a restaurant scenario. A customer doesn't give the order directly to the cooks. A waiter takes the order to the kitchen where a designated cook will execute the order based on the type of dish the customer ordered. In this case, the waiter is just a medium who does not need to know anything about what the customer has ordered and who is going to prepare.

1. Create the receiver objects for the commands

```java
package com.jaypeesoft.dp.command;

public class MainDish {
  String name;

  public MainDish(String name) {
    this.name = name;
  }

  public void order() {
    System.out.println("Main Dish (" + name + ") is ordered");
  }

  public void cancel() {
    System.out.println("Main Dish (" + name + ") is cancelled");
  }
}

public class Dessert {
  String name;

  public Dessert(String name) {
    this.name = name;
  }

  public void order() {
    System.out.println("Dessert (" + name + ") is ordered");
  }

  public void cancel() {
    System.out.println("Dessert (" + name + ") is cancelled");
  }
}
```

2. Create Command interface and its concrete implementations that represent various commands.

```java
package com.jaypeesoft.dp.command;

public interface Command {
  public abstract void execute();
}
```

```java
package com.jaypeesoft.dp.command;

public class OrderMainDish implements Command {
  private MainDish item;

  public OrderMainDish(MainDish i) {
    item = i;
  }

  public void execute() {
    item.order();
  }
}

public class OrderDessert implements Command {
  private Dessert coupon;

  public OrderDessert(Dessert c) {
    coupon = c;
  }

  public void execute() {
    coupon.order();
  }
}

public class CancelMainDish implements Command {
  private MainDish item;

  public CancelMainDish(MainDish i) {
    item = i;
  }

  public void execute() {
    item.cancel();
  }
}

public class CancelDessert implements Command {
  private Dessert coupon;

  public CancelDessert(Dessert c) {
    coupon = c;
  }

  public void execute() {
    coupon.cancel();
  }
}
```

3 Create the Invoker class

```java
package com.jaypeesoft.dp.command;

public class Waiter {

  /*
   * Waiter does not need to know about
   * the details of the command
   */
  public void execute(Command command) {
    command.execute();
  }

}
```

4. The client code. The client creates the commands and pass it on to the executor(waiter). The waiter does not need to know about the contents of the command, who will execute the command, etc. Everything will be encapsulated in the command.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.command.CancelMainDish;
import com.jaypeesoft.dp.command.Dessert;
import com.jaypeesoft.dp.command.MainDish;
import com.jaypeesoft.dp.command.OrderDessert;
import com.jaypeesoft.dp.command.OrderMainDish;
import com.jaypeesoft.dp.command.Waiter;

public class CommandClient {
  public static void main(String[] args) {

    /* Create a Waiter object */
    Waiter waiter = new Waiter();

    /* Customer wants Pizza */
    MainDish item1 = new MainDish("Pizza");
    /* Order Command is created for Pizza */
    OrderMainDish command1 = new OrderMainDish(item1);
    /* Command is given to the waiter to execute */
    waiter.execute(command1);

    /* Customer wants Burger and an Order is placed as above */
    MainDish item2 = new MainDish("Burger");
    OrderMainDish command2 = new OrderMainDish(item2);
    waiter.execute(command2);

    /* Now customer wants to cancel the burger */
    /* Cancel Command is created for the burger */
    CancelMainDish command3 = new CancelMainDish(item2);
    /* Cancel Command is given to the waiter to execute*/
    waiter.execute(command3);

    /* Customer wants Icecream */
    Dessert item3 = new Dessert("Icecream");
    /* Order Command is created for Icecream */
    OrderDessert command4 = new OrderDessert(item3);
    /* Command is given to the waiter to execute */
    waiter.execute(command4);
  }
}
```

Output

```
Main Dish (Pizza) is ordered
Main Dish (Burger) is ordered
Main Dish (Burger) is cancelled
Dessert (Icecream) is ordered
```

#### Benefits

decouples the object that invokes the operation from the one that know how to perform it
This pattern helps in terms of extensible as we can add a new command without changing the existing code.
It allows you to create a sequence of commands named macro. To run the macro, create a list of Command instances and call the execute method of all commands.
Ability to undo/redo easily

#### Drawbacks

Increase in the number of classes for each individual command

#### Real World Examples

Placing orders to the waiter in the restaurant

#### Java SDK Examples

All implementations of java.lang.Runnable
All implementations of javax.swing.Action

### Interpreter

#### When to use

To easily solve 'repeated' problems in a 'well-defined' domain with the help of a 'language'.

#### Intent

Given a language, define a representation for its grammar along with an interpreter that uses the representation to interpret sentences in the language.

#### Implementation

Here we see an example of evaluating arithmetic expressions using the Interpreter pattern. The representation is defined as POSTFIX and the grammer is defined to interprete different type of expressions. Though each expression is different, they are all constructed using the basic rules that make up the grammar for the language of arithmetic expressions.

1. Create an interface for the basic expression.

```java
package com.jaypeesoft.dp.interpreter;

public interface Exp {

  public int evaluate();

}
```

2. Create concrete expressions including the terminal and non-terminal experessions.

```java
package com.jaypeesoft.dp.interpreter;

public class Number implements Exp {

  private final int n;

  public Number(int n) {
    this.n = n;
  }

  public int evaluate() {
    return n;
  }

}
```

```java
package com.jaypeesoft.dp.interpreter;

public class AddExp implements Exp {
  Exp first;
  Exp second;

  public AddExp(Exp first, Exp second) {
    this.first = first;
    this.second = second;
  }

  public int evaluate() {
    return first.evaluate() + second.evaluate();
  }
}
```

```java
package com.jaypeesoft.dp.interpreter;

public class SubtractExp implements Exp{
  Exp first;
  Exp second;

  public SubtractExp(Exp first, Exp second) {
    this.first = first;
    this.second = second;
  }

  public int evaluate() {
    return first.evaluate() - second.evaluate();
  }
}
```

```java
package com.jaypeesoft.dp.interpreter;

public class MultiplyExp implements Exp {
  Exp first;
  Exp second;

  public MultiplyExp(Exp first, Exp second) {
    this.first = first;
    this.second = second;
  }

  public int evaluate() {
    return first.evaluate() * second.evaluate();
  }
}
```

```java
package com.jaypeesoft.dp.interpreter;

public class DivideExp implements Exp {
  Exp first;
  Exp second;

  public DivideExp(Exp first, Exp second) {
    this.first = first;
    this.second = second;
  }

  public int evaluate() {
    return first.evaluate() / second.evaluate();
  }
}
```

3. The client code. The client uses an abstract syntax tree representing a particular sentence in the language that the grammar defines. The abstract syntax tree is assembled from instances of the NonterminalExpression and TerminalExpression classes. The client then invokes the Interpret operation.

```java
package com.jaypeesoft.dp.client;

import java.util.Stack;

import com.jaypeesoft.dp.interpreter.AddExp;
import com.jaypeesoft.dp.interpreter.DivideExp;
import com.jaypeesoft.dp.interpreter.Exp;
import com.jaypeesoft.dp.interpreter.MultiplyExp;
import com.jaypeesoft.dp.interpreter.Number;
import com.jaypeesoft.dp.interpreter.SubtractExp;

public class InterpreterClient {
  public static void main(String args[]) {

    /* POSTFIX Expression to be evaluated */
    String postfix = "543-2+*";

    /* Operations supported */
    final String OPERATORS = "+-*/";

    /* Stack for the operands */
    Stack stack = new Stack();

    for (char c : postfix.toCharArray()) {
      Exp resultExp;
      if (OPERATORS.indexOf(c) == -1) {
        /* number found, push it onto the stack */
        resultExp = new Number(c - 48);
      } else {
        /*
         * operator found, pop out the last two operands from the stack and
         * perform the operation
         */
        Exp right = stack.pop();
        Exp left = stack.pop();

        switch (c) {

        case '+':
          resultExp = new AddExp(left, right);
          break;
        case '-':
          resultExp = new SubtractExp(left, right);
          break;
        case '*':
          resultExp = new MultiplyExp(left, right);
          break;
        case '/':
          resultExp = new DivideExp(left, right);
          break;
        default:
          resultExp = new Number(0);
        }
      }
      /* push the result onto the stack */
      stack.push(new Number(resultExp.evaluate()));
    }
    System.out.println("Result: " + stack.pop().evaluate());
  }
}
```

Output

```
Result: 15
```

#### Benefits

Grammars can be easily modified or extended by inheritance.
Expressions can be interpreted in new ways by adding new operations to the expression.

#### Drawbacks

Grammars containing many rules (i.e many rule classes) can be hard to manage and maintain.

#### Real World Examples

Language Interpreter/Translator

#### Software Examples

Software compiler
SQL evaluation engine
Graphing calculator input parser
XML parser

#### Java SDK Examples

java.util.Pattern
java.text.Normalizer
All subclasses of java.text.Format
All subclasses of javax.el.ELResolver

### Iterator

#### When to use

To provide a standard way to traverse through collections of similar objects.
To expose a simple interface to the client by hiding the complexities of the traversal.
To provide a uniform interface for traversing different aggregate structures.

#### Intent

Provide a way to access the elements of an aggregate object sequentially without exposing its underlying representation.

#### Components

An interface for the collection
Concrete implementations of the collection interface
An "iterator" class that can encapsulate traversal of the "collection" class.

#### Implementation

The key idea is to take the responsibility for access and traversal out of the aggregate object and put it into an Iterator object that defines a standard traversal protocol. Lets create an iterator for a collection of integers. The collection can be in the form of an array or a linked list.

1. Create an interface for the collection and its concrete implementations. The implementations will have a getIterator() method that returns the iterator for the underlying collection of objects.

```java
package com.jaypeesoft.dp.iterator;

public interface Collection {

  public Iterator getIterator();
  public void insert(int val);
}
```

```java
package com.jaypeesoft.dp.iterator;

public class Array implements Collection {

  private int [] arr;
  private int len;

  public Array(int size) {
    arr = new int[size];
    len = 0;
  }

  public void insert(int val) {
    arr[len++] = val;
  }

  public Iterator getIterator() {
    return new ArrayIterator(arr, len);
  }

}
```

```java
package com.jaypeesoft.dp.iterator;

public class LinkedList implements Collection {

  private Node head;
  private Node current;

  public void insert(int val) {
    Node node = new Node(val);
    if (current == null) {
      head = node;
      current = node;
    } else {
      current.next = node;
      current = node;
    }
  }

  public Iterator getIterator() {
    return new ListIterator(head);
  }

  /* Inner class for Node */
  class Node {
    private int data;
    private Node next;

    public Node(int data) {
      this.data = data;
    }

    public int data() {
      return data;
    }

    public Node next() {
      return this.next;
    }

  }

}
```

2. Design an "iterator" interface and its concrete implementations that can encapsulate traversal of the "collection" class.

```java
package com.jaypeesoft.dp.iterator;

public interface Iterator {

  public int next();
  public boolean hasNext();

}
```

```java
package com.jaypeesoft.dp.iterator;

public class ArrayIterator implements Iterator{
  private int [] arr;
  private int pos;
  private int len;

  public ArrayIterator(int [] arr, int len) {
    this.arr = arr;
    this.len = len;
    pos = -1;
  }

  public int next() {
    return arr[++pos];
  }

  public boolean hasNext() {
    return (pos+1) < len;
  }

}
```

```java
package com.jaypeesoft.dp.iterator;

import com.jaypeesoft.dp.iterator.LinkedList.Node;

public class ListIterator implements Iterator {
  private Node current;

  public ListIterator(Node head) {
    this.current = head;
  }

  public int next() {
    int val = current.data();
    current = current.next();
    return val;
  }

  public boolean hasNext() {
    return current != null;
  }

}
```

3. The client code. The client uses iterator methods to access the elements of the collection class.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.iterator.Array;
import com.jaypeesoft.dp.iterator.Collection;
import com.jaypeesoft.dp.iterator.Iterator;
import com.jaypeesoft.dp.iterator.LinkedList;

public class IteratorClient {

  public static void main(String[] args) {

    Collection arr = new Array(4);
    arr.insert(1);
    arr.insert(2);
    arr.insert(3);
    arr.insert(4);
    iterate(arr.getIterator());

    Collection list = new LinkedList();
    list.insert(11);
    list.insert(22);
    list.insert(33);
    iterate(list.getIterator());

  }

  public static void iterate(Iterator it) {
    while (it.hasNext())
      System.out.print(it.next() + " ");
    System.out.println();
  }
}
```

Output

```
1 2 3 4
11 22 33
```

#### Benefits

The iterator class can be extended to add new functionalities without having to alter the actual object it iterates over.
The original class need not be cluttered with interfaces that are specific to traversal.
Iterator can support multiple traversals because it keeps track of its own traversal state.

#### Real World Examples

Television Remote Control. User can access the required channel by pressing the 'next' or 'previous' button. User does not need to know about the internal details of the channel like frequency, band etc. Remote Control provides a simplified interface to iterate through the channels.

#### Software Examples

Iterators provided for various types of collections in SDKs

#### Java SDK Examples

All implementations of java.util.Iterator
All implementations of java.util.Enumeration

### Mediator

#### When to use

To facilitate interactions between a set of objects where the communications are complex and hard to maintain.
To have a centralized control for the object interactions.

#### Intent

Define an object that encapsulates how a set of objects interact. Mediator promotes loose coupling by keeping objects from referring to each other explicitly, and it lets you vary their interaction independently.

#### Components

Mediator interface – an interface that defines the communication rules between objects
Concrete mediator – a mediator object which will enables communication between participating objects
Colleagues – objects communicating with each other through mediator object

#### Implementation

Mediator enables decoupling of objects by introducing a layer in between so that the interaction between objects happen via the layer. ATC (Air Traffic Controller) is a perfect example for the mediator design pattern. A typical airport is a complex system that involves complex communications between flights, airport vehicles, and other airport systems. Direct communication between all these participants is error prone and practically impossible. Hence we need a system that helps in communication between flights, airport vehicle and coordinates landing, take-off, etc.

1. Create an interface that defines the communication rules between objects

```java
package com.jaypeesoft.dp.mediator;

public interface AtcMediator {

  public void registerRunway(Runway runway);
  public void registerGate(Gate gate);
  public boolean getLandingPermission(Flight flight);
  public boolean getTakeoffPermission(Flight flight);
  public void enterRunway(Runway runway);
  public void exitRunway(Runway runway);
  public void enterGate(Gate gate);
  public void exitGate(Gate gate);

}
```

2. Create a mediator object which will enables communication between the participating objects.

```java
package com.jaypeesoft.dp.mediator;

import java.util.ArrayList;
import java.util.List;

public class AtcMediatorImpl implements AtcMediator {

  private Runway runway;
  private List gates;

  public AtcMediatorImpl() {
    gates = new ArrayList();
  }

  public void registerRunway(Runway runway) {
    this.runway = runway;
  }

  public void registerGate(Gate gate) {
    gates.add(gate);
  }

  public boolean getLandingPermission(Flight flight) {

    /* Check if the runway is free */
    if (runway.isInUse() == false) {

      /* Find an available gate */
      for (Gate gate : gates) {

        if (gate.isInUse() == false) {

          /* reserve the gate and runway */
          flight.allocateRunway(runway);
          flight.allocateGate(gate);
          return true;

        }

      }

      System.out.println("[ATC Mediator] All gates in use");

    }
    else
      System.out.println("[ATC Mediator] Runway in use");

    return false;
  }

  public boolean getTakeoffPermission(Flight flight) {
    return runway.isInUse() == false;
  }

  public void exitRunway(Runway runway) {
    runway.setInUse(false);
  }

  public void exitGate(Gate gate) {
    gate.setInUse(false);
  }

  public void enterRunway(Runway runway) {
    runway.setInUse(true);
  }

  public void enterGate(Gate gate) {
    gate.setInUse(true);
  }

}
```

3. Define colleagues. Colleagues keep a reference to its Mediator object.

```java
package com.jaypeesoft.dp.mediator;

public class Flight {
  private AtcMediator atc;
  private String flightNum;
  private Runway runway;
  private Gate gate;

  public Flight(AtcMediator atc, String flightNum) {
    this.atc = atc;
    this.flightNum = flightNum;
  }

  public String getName() {
    return flightNum;
  }

  public Runway getRunway() {
    return runway;
  }

  public void allocateRunway(Runway runway) {
    this.runway = runway;
  }

  public Gate getGate() {
    return gate;
  }

  public void allocateGate(Gate gate) {
    this.gate = gate;
  }

  public void landAndTakeOff() throws InterruptedException {

    System.out.println(flightNum + " is requesting landing permission");

    while (false == atc.getLandingPermission(this)) {
      Thread.sleep(1000);
    }
    // Landing permission granted, land now
    land();

    while (false == atc.getTakeoffPermission(this)) {
      Thread.sleep(1000);
    }
    // Take off permission granted, take off now
    takeOff();
  }

  public void land() {

    atc.enterRunway(runway);
    System.out.println(this.flightNum + " is landing, gate is "
        + gate.getGateNum());

    /* Exit the runway after 1 second */
    new java.util.Timer().schedule(new java.util.TimerTask() {
      @Override
      public void run() {
        atc.exitRunway(runway);
        atc.enterGate(gate);
      }
    }, 1000);

  }

  public void takeOff() {

    /* Takeoff after 5 seconds */
    new java.util.Timer().schedule(new java.util.TimerTask() {

      @Override
      public void run() {
        System.out.println(flightNum + " is taking off");
        atc.exitGate(gate);
        atc.enterRunway(runway);

        /* Exit the runway after 1 second */
        new java.util.Timer().schedule(new java.util.TimerTask() {
          @Override
          public void run() {
            atc.exitRunway(runway);
          }
        }, 1000);

      }
    }, 5000);

  }

}
```

```java
package com.jaypeesoft.dp.mediator;

public class Runway {

  private String runwayNum;
  private boolean inUse;

  public Runway(String runwayNum, boolean inUse) {
    this.runwayNum = runwayNum;
    this.inUse = inUse;
  }

  public String getRunwayNum() {
    return runwayNum;
  }

  public void setRunwayNum(String runwayNum) {
    this.runwayNum = runwayNum;
  }

  public boolean isInUse() {
    return inUse;
  }

  public void setInUse(boolean inUse) {
    this.inUse = inUse;
  }

}
```

```java
package com.jaypeesoft.dp.mediator;

public class Gate {

  private String gateNum;
  private boolean inUse;

  public Gate(String gateNum, boolean inUse) {
    this.gateNum = gateNum;
    this.inUse = inUse;
  }

  public String getGateNum() {
    return gateNum;
  }

  public boolean isInUse() {
    return inUse;
  }

  public void setInUse(boolean inUse) {
    this.inUse = inUse;
  }

}
```

4. The client code. The client creates mediator and all the colleagues register with the mediator. When the objects want to interact with other objects, it uses the mediator,

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.mediator.AtcMediator;
import com.jaypeesoft.dp.mediator.AtcMediatorImpl;
import com.jaypeesoft.dp.mediator.Flight;
import com.jaypeesoft.dp.mediator.Gate;
import com.jaypeesoft.dp.mediator.Runway;

public class MediatorClient {

  public static void main(String args[]) throws InterruptedException {

    AtcMediator atcMediator = new AtcMediatorImpl();

    /* Create a runway & register with the mediator */
    atcMediator.registerRunway(new Runway("RW-1", false));

    /* Create gates & register gates with the mediator */
    atcMediator.registerGate(new Gate("G-1", false));
    atcMediator.registerGate(new Gate("G-2", false));
    atcMediator.registerGate(new Gate("G-3", false));

    /* Many flights are arriving and they want to land and take off.
     * They just contact the mediator instead of
     * directly communicating with individual objects. */

    for(int i=0; i<10; i++) {
      Flight flight = new Flight(atcMediator, "F-00"+(i+1));
      flight.landAndTakeOff();
    }

  }
}
```

Output

```
F-001 is requesting landing permission
F-001 is landing, gate is G-1
F-002 is requesting landing permission
F-002 is landing, gate is G-2
F-001 is taking off
F-003 is requesting landing permission
F-003 is landing, gate is G-1
F-004 is requesting landing permission
F-004 is landing, gate is G-3
F-002 is taking off
F-003 is taking off
F-005 is requesting landing permission
[ATC Mediator] Runway in use
F-005 is landing, gate is G-1
F-004 is taking off
F-006 is requesting landing permission
F-006 is landing, gate is G-2
F-005 is taking off
F-007 is requesting landing permission
F-007 is landing, gate is G-1
F-006 is taking off
F-008 is requesting landing permission
F-008 is landing, gate is G-2
F-009 is requesting landing permission
F-009 is landing, gate is G-3
F-007 is taking off
F-0010 is requesting landing permission
F-0010 is landing, gate is G-1
F-008 is taking off
F-009 is taking off
F-0010 is taking off
```

#### Benefits

A mediator promotes loose coupling between colleagues.
Promotes one-to-many relationships that are easier to understand, implement and maintain than many-to-many relationships.
A mediator simplifies the communication and provides a centralized control.

#### Drawbacks

A Mediator class may become complex if not designed carefully.
All communications are routed by the Mediator which may impact the performance.

#### Real World Examples

Air Traffic Controller
Head Speaker of the Parliament - members do not communicate with each other

#### Java SDK Examples

java.util.Timer (all scheduleXXX() methods)
java.util.concurrent.Executor execute()
java.util.concurrent.ExecutorService (the invokeXXX() and submit() methods)
java.util.concurrent.ScheduledExecutorService (all scheduleXXX() methods)
java.lang.reflect.Method invoke()

### Memento

#### When to use

To take snapshots and restore an object back to its previous state (e.g. "undo" or "rollback" operations).

#### Intent

Without violating encapsulation, capture and externalize an object's internal state so that the object can be restored to this state later.

#### Components

Originator - the object that knows how to save itself.
Caretaker - the object that knows why and when the Originator needs to save and restore itself.
Memento - the lock box that is written and read by the Originator, and shepherded by the Caretaker.

#### Implementation

Memento design pattern is used when the object state needs to be saved so that it can be restored later. Memento pattern implements this in a way that the saved state data of the object is not accessible outside of the object, thus protecting the integrity of saved state data.
We will see an OS Recovery tool that saves the good configuration of the OS so that it can be restored later if there is any data corruption.

1. Create an Originator class. In our example, the operating system s/w is the originator which creates/restores a memento.

```java
package com.jaypeesoft.dp.memento;

public class OS {
  private StringBuilder installedSw;

  public OS(String os) {
    installedSw = new StringBuilder(os);
  }

  public void install(String sw) {
    installedSw.append(" + " + sw);
    System.out.println(installedSw);
  }

  public RecoveryImage saveImage() {
    System.out.println("--Saved OS Image--");
    return new RecoveryImage(installedSw.toString());
  }

  public void restoreImage(RecoveryImage m) {
    installedSw = new StringBuilder(m.getSystemImage());
    System.out.println("--Restored OS Image--");
    System.out.println(installedSw);
  }
}
```

2. Create a Memento class. In this case, the RecoveryImage class is the memento which represents the object that can be saved and restored.

```java
package com.jaypeesoft.dp.memento;

public class RecoveryImage {
  private String image;

  public RecoveryImage(String image) {
    this.image = image;
  }

  public String getSystemImage() {
    return image;
  }
}
```

3. Create a Caretaker class which manages the mementos. The caretaker class cannot modify the contents of memento.

```java
package com.jaypeesoft.dp.memento;

import java.util.ArrayList;

public class RecoveryTool {
  private ArrayList mementos = new ArrayList<>();

  public void addImage(RecoveryImage m) {
    mementos.add(m);
  }

  public void deleteLastImage() {
    mementos.remove(mementos.size() - 1);
  }

  public RecoveryImage getLastGoodImage() {
    return mementos.get(mementos.size() - 1);
  }
}
```

4. The client code. The client uses the Caretaker (RecoveryTool) to "rollback" to the previous good state.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.memento.RecoveryTool;
import com.jaypeesoft.dp.memento.OS;

public class MementoClient {

  public static void main(String[] args) {

    /* Create an OS */
    OS os = new OS("Windows 10");

    /* Install basic s/w */
    os.install("Antivirus");

    /* Create an OS recovery tool */
    RecoveryTool recoveryTool = new RecoveryTool();
    /* Save image for future restoration */
    recoveryTool.addImage(os.saveImage());

    /* Install more s/w */
    os.install("Tomcat Server");
    /* Save image for future restoration */
    recoveryTool.addImage(os.saveImage());

    /* Install more s/w */
    os.install("MySql");

    /* OS CORRUPTED */
    /* Restore the last good configuration */
    os.restoreImage(recoveryTool.getLastGoodImage());

    /* OS CORRUPTED AGAIN */
    /* Delete the last image & restore
       the previous good configuration */
    recoveryTool.deleteLastImage();
    os.restoreImage(recoveryTool.getLastGoodImage());
  }
}
```

Output

```
Windows 10 + Antivirus
--Saved OS Image--
Windows 10 + Antivirus + Tomcat Server
--Saved OS Image--
Windows 10 + Antivirus + Tomcat Server + MySql
--Restored OS Image--
Windows 10 + Antivirus + Tomcat Server
--Restored OS Image--
Windows 10 + Antivirus
```

#### Benefits

Internal state of the memento object cannot be changed
Easy to implement recoverable states

#### Drawbacks

Too many objects may be created by the Originator which makes the maintenance expensive.

#### Software Examples

Undo functionality in text editors
Snapshots of softwares

#### Java SDK Examples

java.util.Date (the setter methods do that, Date is internally represented by a long value)
All implementations of java.io.Serializable
All implementations of javax.faces.component.StateHolder

### Observer

#### When to use

When there is one to many relationship between objects such as if one object is modified, its dependent objects are to be notified automatically and corresponding changes are done to all dependent objects.

#### Intent

Define a one-to-many dependency between objects so that when one object changes state, all its dependents are notified and updated automatically.

#### Implementation

Consider a simple Point Of Sale (POS) system which has many peripherals attached to it. Each peripheral(observer) registers itself to the interested topics(subjects) so that it can update its status when that topic/event occurs. For e.g., when an item or payment is added to the order, the cashier display and customer display need to be updated with the new details. Similarly when the order is completed, the receipt printer will act on it by printing the receipt.

1. Create an observer interface and its concrete implementations. These observers subscribe to specific topics(subjects) so that it can display the updates.

```java
package com.jaypeesoft.dp.observer;

public abstract class Observer {
  public abstract void update(String str);
}
```

```java
package com.jaypeesoft.dp.observer;

public class CustomerDisplay extends Observer {

  public void update(String str) {
    System.out.println("[CustomerDisplay] " + str);
  }

}
```

```java
package com.jaypeesoft.dp.observer;

public class CashierDisplay extends Observer {

  public void update(String str) {
    System.out.print("[CashierDisplay] " + str);
  }

}
```

2. Create an interface for the subject and its concrete implementations. These subjects contain lists to keep track of subscribed observers that need to be notified.

```java
package com.jaypeesoft.dp.observer;

public interface Topic {
  public void register(Observer obj);
  public void notifyObservers(String line);
}
```

```java
package com.jaypeesoft.dp.observer;

import java.util.ArrayList;
import java.util.List;

public class AddItemTopic implements Topic {

  List addItemObservers = new ArrayList();

  public void notifyObservers(String line) {
    for(Observer o : addItemObservers) {
      o.update(line);
    }
  }

  public void register(Observer o) {
    addItemObservers.add(o);
  }

}
```

```java
package com.jaypeesoft.dp.observer;

import java.util.ArrayList;
import java.util.List;

public class AddPaymentTopic implements Topic {

  List addPaymentObservers = new ArrayList();

  public void notifyObservers(String line) {
    for(Observer o : addPaymentObservers) {
      o.update(line);
    }
  }

  public void register(Observer o) {
    addPaymentObservers.add(o);
  }

}
```

```java
package com.jaypeesoft.dp.observer;

import java.util.ArrayList;
import java.util.List;

public class CompleteOrderTopic implements Topic {

  List orderCompletedObservers = new ArrayList();

  public void notifyObservers(String line) {
    for(Observer o : orderCompletedObservers) {
      o.update(line);
    }
  }

  public void register(Observer o) {
    orderCompletedObservers.add(o);
  }

}
```

3. Create Item, Payment, and Order classes. The observers subscribe to various subjects(topics) in the Order class constructor.

```java
package com.jaypeesoft.dp.observer;

public class Item {
  public final String name;
  public final double price;

  public Item(String name, double price) {
    this.name = name;
    this.price = price;
  }

}
```

```java
package com.jaypeesoft.dp.observer;

public class Payment {

  public final String type;
  public final double amount;

  public Payment(String type, double amount) {
    super();
    this.type = type;
    this.amount = amount;
  }

}
```

```java
package com.jaypeesoft.dp.observer;

import java.util.ArrayList;
import java.util.List;

public class Order {
  List cart = new ArrayList();
  List payments = new ArrayList();

  private Topic addItemTopic;
  private Topic addPaymentTopic;
  private Topic completeOrderTopic;

  public Order() {
    // create observers (devices)
    Observer cashierDisplay = new CashierDisplay();
    Observer customerDisplay = new CustomerDisplay();

    // create subjects (events)
    addItemTopic = new AddItemTopic();
    addPaymentTopic = new AddPaymentTopic();
    completeOrderTopic = new CompleteOrderTopic();

    // Cashier display subscribed to all topics
    addItemTopic.register(cashierDisplay);
    addPaymentTopic.register(cashierDisplay);
    completeOrderTopic.register(cashierDisplay);

    // Customer display subscribed to all topics
    addItemTopic.register(customerDisplay);
    addPaymentTopic.register(customerDisplay);
    completeOrderTopic.register(customerDisplay);
  }

  public void addItem(Item item) {
    cart.add(item);
    String line = item.name + " $" + item.price;
    addItemTopic.notifyObservers(line);
  }

  public void makePayment(Payment payment) {
    payments.add(payment);
    String line = payment.type + " $" + payment.amount;
    addPaymentTopic.notifyObservers(line);

  }

  public void completeOrder() {
    String line = "Order completed";
    completeOrderTopic.notifyObservers(line);
  }

}
```

4. The client code. When an item or payment is added to the order, the corresponding topic notifies its observers.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.observer.Item;
import com.jaypeesoft.dp.observer.Order;
import com.jaypeesoft.dp.observer.Payment;

public class ObserverClient {

  public static void main(String[] args) {

    /* Create an order and add items */
    Order order = new Order();
    order.addItem(new Item("Pizza", 6.99));
    order.addItem(new Item("Wine", 9.99));
    order.addItem(new Item("Beer", 5.99));
    order.addItem(new Item("Apple", 1.49));

    System.out.println("---------------------------------");

    /* Create payments and make payments */
    order.makePayment(new Payment("CASH", 20.00));
    order.makePayment(new Payment("CREDIT", 10.00));
    order.makePayment(new Payment("DEBIT", 10.00));
    System.out.println("---------------------------------");

    /* Complete the order */
    order.completeOrder();
  }

}
```

Output

```
[CashierDisplay] Pizza  $6.99	[CustomerDisplay] Pizza  $6.99
[CashierDisplay] Wine   $9.99	[CustomerDisplay] Wine   $9.99
[CashierDisplay] Beer   $5.99	[CustomerDisplay] Beer   $5.99
[CashierDisplay] Apple  $1.49	[CustomerDisplay] Apple  $1.49
[CashierDisplay] CASH 	$20.0	[CustomerDisplay] CASH   $20.0
[CashierDisplay] CREDIT $10.0	[CustomerDisplay] CREDIT $10.0
[CashierDisplay] DEBIT 	$10.0	[CustomerDisplay] DEBIT  $10.0
[CashierDisplay] Order completed [CustomerDisplay] Order completed
```

#### Benefits

Loose coupling between Subject and Observer allows you vary subjects and observers independently.
Supports broadcast communication.

#### Real World Examples

Auction - The bidders act as observers and raise the paddle to accept the bid. When the bid is accepted, the others are notified by the auctioneer.

#### Java SDK Examples

java.util.Observer, java.util.Observable (rarely used in real world though)
All implementations of java.util.EventListener (practically all over Swing thus)
javax.servlet.http HttpSessionBindingListener
javax.servlet.http HttpSessionAttributeListener
javax.faces.event PhaseListener

### State

#### When to use

To encapsulate varying behavior for the same object based on its internal state.
To have the ability to change the behavior at run time.

#### Intent

Allow an object to alter its behavior when its internal state changes. The object will appear to change its class.

#### Implementation

Consider the ATM scenario. The ATM can be in various states/modes like working, no cash, error, etc. When the customer interacts with the ATM, it should respond based on the current state. For example, when the ATM is in "No Cash" or "Error" state, the withdrawal should not be allowed. Typically, conditional statements are used to implement this kind of behavior. Also the same conditional statements may have to be repeated in different functions to support the possible interactions. This makes the code fragile and supporting a new state/operation may become difficult.
State pattern can be used here to simplify the design. For e.g, when a new ATM state needs to be added, it can be done easily by adding a new state. The other classes will remain unchanged.

1. Define a State abstract base class and represent the different "states" of the state machine as derived classes of the State base class. Define state-specific behavior in the appropriate State derived classes.

```java
package com.jaypeesoft.dp.state;

public interface AtmState {
  public void withdraw(int amount);
  public void refill(int amount);
}
```

```java
package com.jaypeesoft.dp.state;

public class Working implements AtmState {

  Atm atm;

  Working(Atm atm) {
    this.atm = atm;
  }

  public void withdraw(int amount) {

    int cashStock = atm.getCashStock();
    if(amount > cashStock) {
      /* Insufficient fund.
       * Dispense the available cash */
      amount = cashStock;
      System.out.print("Partial amount ");
    }
    System.out.println(amount + "$ is dispensed");
    int newCashStock = cashStock - amount;
    atm.setCashStock(newCashStock);
    if(newCashStock == 0) {
      atm.setState(new NoCash(atm));
    }

  }

  public void refill(int amount) {
    System.out.println(amount + "$ is loaded");
    atm.setCashStock(atm.getCashStock()+amount);
  }

}

public class NoCash implements AtmState {

  Atm atm;

  NoCash(Atm atm) {
    this.atm = atm;
  }

  public void withdraw(int amount) {
    System.out.println("Out of cash");
  }

  public void refill(int amount) {
    System.out.println(amount + "$ is loaded");
    atm.setState(new Working(atm));
    atm.setCashStock(atm.getCashStock()+amount);
  }

}
```

2. Define a "context" class to present a single interface to the outside world. Maintain a pointer to the current "state" in the "context" class. To change the state of the state machine, change the current "state" pointer.

```java
package com.jaypeesoft.dp.state;

public class Atm implements AtmState {

  int cashStock;
  AtmState currentState;

  public Atm() {
    currentState = new NoCash(this);
  }

  public int getCashStock() {
    return cashStock;
  }

  public void setCashStock(int CashStock) {
    this.cashStock = CashStock;
  }

  public void setState(AtmState state) {
    currentState = state;
  }

  public AtmState getState() {
    return currentState;
  }

  public void withdraw(int amount) {
    currentState.withdraw(amount);
  }

  public void refill(int amount) {
    currentState.refill(amount);
  }

}
```

3. The client code. The client interacts with the context.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.state.Atm;

public class StateClient {

  public static void main(String [] args) {
    Atm atm = new Atm();
    atm.refill(100);
    atm.withdraw(50);
    atm.withdraw(30);
    atm.withdraw(30); // overdraft
    atm.withdraw(20); // overdraft
    atm.refill(50);
    atm.withdraw(50);
  }
}
```

Output

```
100$ is loaded
50$ is dispensed
30$ is dispensed
Partial amount 20$ is dispensed
Out of cash
50$ is loaded
50$ is dispensed
```

#### Benefits

New states can be added easily as the state specific behavior is encapsulated in that state class.
Avoids too many conditional statements.
State class aggregates the state specific behavior which results in increased cohesion.

#### Drawbacks

State classes must know about each other so that the state can be changed.
May result in duplicate objects if state classes are not defined as singletons.

#### Real World Examples

Electro-machanical machines (ATM machine, Gear box, Microwave oven) which can have many internal states.

#### Software Examples

FSM - Finite State Machine

#### Java SDK Examples

javax.faces.lifecycle.LifeCycle execute() (controlled by FacesServlet, the behaviour is dependent on current phase (state) of JSF lifecycle)

### Strategy

#### When to use

To switch out different implementations for different situations.
To support different variants of the algorithm.

#### Intent

Define a family of algorithms, encapsulate each one, and make them interchangeable. Strategy lets the algorithm vary independently from clients that use it.

#### Implementation

Assume that you want to create a shopping cart application. One of the requirements is to allow different types of payments.

1. Define the interface of an interchangeable family of algorithms and move algorithm implementation details in subclasses.

```java
package com.jaypeesoft.dp.strategy;

public interface PaymentStrategy {

  public void pay();

}

public class CardPayment implements PaymentStrategy {

  public String cardType;
  public String issuer;
  public double amount;

  public CardPayment(String cardType, String issuer,
      double amount) {
    super();
    this.cardType = cardType;
    this.issuer = issuer;
    this.amount = amount;
  }

  @Override
  public void pay() {
    System.out.println(issuer + " " + cardType + " " + amount + "$" );
  }

}

public class CashPayment implements PaymentStrategy {

  public double amount;

  public CashPayment(double amount) {
    this.amount = amount;
  }

  @Override
  public void pay() {
    System.out.println("Cash " + amount + "$" );
  }

}
```

2. Interface method is used to invoke the algorithm.

```java
package com.jaypeesoft.dp.strategy;

public class Item {
  private String name;
  private double price;

  public Item(String name, double price) {
    this.name = name;
    this.price = price;
  }

  public String getName() {
    return name;
  }

  public double getPrice() {
    return price;
  }
}
```

```java
package com.jaypeesoft.dp.strategy;

import java.util.ArrayList;
import java.util.List;

public class Order {
  List cart = new ArrayList();
  List payments =
      new ArrayList();

  private final String FORMAT = "%-20s %s";

  public void addItem(Item item) {
    cart.add(item);
    System.out.println(String.format(FORMAT,
        item.getName(), item.getPrice()));
  }

  public void makePayment(PaymentStrategy pm) {
    payments.add(pm);
    pm.pay();
  }

}
```

3. The client code. The client chooses the algorithm/strategy at runtime.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.strategy.CardPayment;
import com.jaypeesoft.dp.strategy.CashPayment;
import com.jaypeesoft.dp.strategy.Item;
import com.jaypeesoft.dp.strategy.Order;

public class StrategyClient {

  public static void main(String[] args) {

    /* Create an order and add items */
    Order order = new Order();
    order.addItem(new Item("Italian Pizza", 6.99));
    order.addItem(new Item("Wine", 9.99));
    order.addItem(new Item("Beer", 5.99));
    order.addItem(new Item("Red Apple", 1.49));
    order.addItem(new Item("Almonds", 11.99));

    System.out.println("---------------------------------");
    /* Create payment strategies and make payment */
    order.makePayment(new CashPayment(20.00));
    order.makePayment(new CardPayment("CREDIT", "VISA", 10.00));
    order.makePayment(new CardPayment("DEBIT", "AMEX", 10.00));
    System.out.println("---------------------------------");
  }
}
```

Output

```
Italian Pizza         6.99
Wine                  9.99
Beer                  5.99
Red Apple             1.49
Almonds              11.99
Cash 20.0$
VISA CREDIT 10.0$
AMEX DEBIT 10.0$
```

#### Benefits

Too many conditional statements can be avoided with different strategy classes.
Allows runtime selection of algorithms from the same algorithm family.
Improves extensibility with 3rd party implementations of the algorithms.
Improves readability by avoiding too many if/else or switch statements.
Enforces Open/Close principle.

#### Drawbacks

May increase the number of classes as each strategy needs to be defined in its own class.

#### Real World Examples

Choose a game strategy (attack/defend) based on the opponent.

#### Software Examples

Sorting (We want to sort these numbers, but we don't know if we are gonna use BrickSort, BubbleSort or some other sorting)
Validation (We need to check items according to "Some rule", but it's not yet clear what that rule will be, and we may think of new ones.)
Games (We want player to either walk or run when he moves, but maybe in the future, he should also be able to swim, fly, teleport, burrow underground, etc.)
Storing information (We want the application to store information to the Database, but later it may need to be able to save a file, or make a webcall)
Outputting (We need to output X as a plain string, but later may be a CSV, XML, Json, etc.)

#### Java SDK Examples

java.util.Comparator compare(), executed by among others Collections sort().
javax.servlet.http.HttpServlet, the service() and all doXXX() methods take HttpServletRequest and HttpServletResponse and the implementor has to process them
javax.servlet.Filter doFilter()

### Template Method

#### When to use

To define a skeleton of an algorithm or an operation. Allow the sub-classes to re-define part of the logic.

#### Intent

Define the skeleton of an algorithm in an operation, deferring some steps to subclasses. Template Method lets subclasses redefine certain steps of an algorithm without changing the algorithm's structure.

#### Components

An abstract class that defines the template method (public final)
Concrete implementations that override the steps defined in the above template method

#### Implementation

Assume that you want to have a standard procedure to cook pizza and you do not want the subclasses to change this procedure. This can be implemented with the help of a template method which defines the skeleton of the algorithm. You can create subclasses that redefine certain steps in the algorithm.

1. Create an abstract base class that defines the template method. Note that the method is declared as 'final' to avoid the subclasses from overriding and changing the logic. Provide default implementations and also the abstract methods for the steps to be overridden.

```java
package com.jaypeesoft.dp.templatemethod;

public abstract class Pizza {

  /* Template method exposed to client */
  public final void preparePizza() {
    selectCrust();
    addIngredients();
    addToppings();
    cook();
  }

  /* Define abstract steps to be overridden */

  protected abstract void addToppings();
  protected abstract void addIngredients();

  /* Define default implementations */

  protected void selectCrust() {
    System.out.println("Selected default Crust");
  }

  protected void cook() {
    System.out.println("Cooked for 5 minutes");
  }

}
```

2. Create subclasses which redefine certain steps in the algorithm.

```java
package com.jaypeesoft.dp.templatemethod;

public class MeatPizza extends Pizza {

  protected void addIngredients() {
    System.out.println("Added Meat Pizza ingredients");
  }

  protected void addToppings() {
    System.out.println("Added Meat Pizza toppings");
  }

  protected void cook() {
    System.out.println("Cooked for 15 minutes");
  }

}
```

```java
package com.jaypeesoft.dp.templatemethod;

public class CheesePizza extends Pizza {

  protected void addIngredients() {
    System.out.println("Added Cheese Pizza ingredients");
  }

  protected void addToppings() {
    System.out.println("Added Cheese Pizza toppings");
  }

  protected void cook() {
    System.out.println("Cooked for 10 minutes");
  }

}
```

3. The client code. The client instantiates appropriate subclass object and invokes the template method.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.templatemethod.CheesePizza;
import com.jaypeesoft.dp.templatemethod.MeatPizza;
import com.jaypeesoft.dp.templatemethod.Pizza;

public class TemplateMethodClient {

  public static void main(String[] args) {
    System.out.println("Preparing a Cheese Pizza");
    Pizza pizza1 = new CheesePizza();
    pizza1.preparePizza();

    System.out.println("Preparing a Meat Pizza");
    Pizza pizza2 = new MeatPizza();
    pizza2.preparePizza();
  }

}
```

Output

```
Preparing a Cheese Pizza
Selected default Crust
Added Cheese Pizza ingredients
Added Cheese Pizza toppings
Cooked for 10 minutes

Preparing a Meat Pizza
Selected default Crust
Added Meat Pizza ingredients
Added Meat Pizza toppings
Cooked for 15 minutes
```

#### Benefits

Avoids code duplication.
Subclasses can decide how to implement steps in an algorithm.
Steps of the algorithm or the operation can be changed without changes in the subclasses.

#### Drawbacks

Inadequate documentation may confuse developers about the flow in the template method.

#### Software Examples

Most of the software frameworks define template methods. Framework make callbacks into methods implemented in child classes.

#### Java SDK Examples

All non-abstract methods of java.io.InputStream, java.io.OutputStream, java.io.Reader and java.io.Writer.
All non-abstract methods of java.util.AbstractList, java.util.AbstractSet and java.util.AbstractMap.
javax.servlet.http.HttpServlet, all the doXXX() methods by default sends a HTTP 405 "Method Not Allowed" error to the response. You're free to implement none or any of them.

### Visitor

#### When to use

To perform similar operations on objects of different types grouped in a structure (a collection or a more complex structure).
To perform distinct and unrelated operations on objects without polluting their classes with these operations.
To run different methods based on concrete type without instanceof or typeof operators.
To perform double dispatching.

#### Intent

Represent an operation to be performed on the elements of an object structure. Visitor lets you define a new operation without changing the classes of the elements on which it operates.

#### Components

A Visitable Interface
Concrete classes that implement Visitable interface
A Visitor Interface
Concrete classes that implement Visitor interface

#### Implementation

Consider a scenario where we need to perform a certain kind of operations without modifying the actual classes. For example, while creating an order, we want to perform a set of operations on all items (for e.g. applying discount, calculate tax etc). We can add getTax() and getDiscount() methods to the actual classes, but this pollutes the class and requires a change in the class signature every time an operation needs to added or removed. We can avoid these issues by using the double delegation feature of Visitor pattern.

1. Create a Visitable interface and the concrete classes that implement this.

```java
package com.jaypeesoft.dp.visitor;

public interface Visitable {

  public void apply(Visitor visitor);

}
```

```java
package com.jaypeesoft.dp.visitor;

public class FoodItem implements Visitable {

  public int id;
  public String name;
  public double price;

  public FoodItem(int id, String name, double price) {
    this.id = id;
    this.name = name;
    this.price = price;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  @Override
  public void apply(Visitor visitor) {
    visitor.visit(this);
  }

}
```

```java
package com.jaypeesoft.dp.visitor;

public class LiquorItem implements Visitable {

  public int id;
  public String name;
  public double price;

  public LiquorItem(int id, String name, double price) {
    this.id = id;
    this.name = name;
    this.price = price;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  @Override
  public void apply(Visitor visitor) {
    visitor.visit(this);
  }

}
```

2. Create a Visitor interface and the concrete classes that implement this.

```java
package com.jaypeesoft.dp.visitor;

public interface Visitor {

  void visit(FoodItem item);
  void visit(LiquorItem item);

}
```

```java
package com.jaypeesoft.dp.visitor;

public class DiscountVisitor implements Visitor {

  private double totalDiscount;

  public void visit(FoodItem item) {
    /* apply 30% off for food items */
    double discount = item.getPrice() * 0.3;
    totalDiscount += discount;
    item.setPrice(item.getPrice()-discount);
  }

  public void visit(LiquorItem item) {
    /* apply 10% off for liquor items */
    double discount = item.getPrice() * 0.1;
    totalDiscount += discount;
    item.setPrice(item.getPrice()-discount);
  }

  public double getTotalDiscount() {
    return totalDiscount;
  }

}

package com.jaypeesoft.dp.visitor;

public class TaxVisitor implements Visitor {

  private double totalTax;

  public void visit(FoodItem item) {
    /* apply 2% tax on food items */
    double tax = item.getPrice() * 0.02;
    totalTax += tax;
    item.setPrice(item.getPrice()+tax);
  }

  public void visit(LiquorItem item) {
    /* apply 30% tax on liquor items */
    double tax = item.getPrice() * 0.20;
    totalTax += tax;
    item.setPrice(item.getPrice()+tax);
  }

  public double getTotalTax() {
    return totalTax;
  }

}
```

3. The client code. With this approach, new operations can be performed easily by adding only the Visitor class without changing the signature of the Visitable concrete classes.

```java
package com.jaypeesoft.dp.client;

import java.util.ArrayList;
import java.util.List;

import com.jaypeesoft.dp.visitor.DiscountVisitor;
import com.jaypeesoft.dp.visitor.FoodItem;
import com.jaypeesoft.dp.visitor.LiquorItem;
import com.jaypeesoft.dp.visitor.TaxVisitor;
import com.jaypeesoft.dp.visitor.Visitable;

public class VisitorClient {

  public static void main(String[] args) {

    /* Create an order and add items */
    List order = new ArrayList();
    order.add(new FoodItem(1, "Italian Pizza", 6.99));
    order.add(new LiquorItem(1, "Wine", 9.99));
    order.add(new LiquorItem(1, "Beer", 5.99));
    order.add(new FoodItem(1, "Red Apple", 1.49));
    order.add(new FoodItem(1, "Almonds", 11.99));

    /* Create visitors to be applied */
    DiscountVisitor discountVisitor = new DiscountVisitor();
    TaxVisitor taxVisitor = new TaxVisitor();

    /* Apply visitors on items */
    for(Visitable item : order) {
      item.apply(discountVisitor);
      item.apply(taxVisitor);
    }

    System.out.println("Total Discount = " +
        discountVisitor.getTotalDiscount());
    System.out.println("Total Tax = " +
        taxVisitor.getTotalTax());
  }

}
```

Output

```
Total Discount = 7.73
Total Tax = 3.16
```

#### Benefits

Separate data structures from the operations on them.
New operations can be added easily by creating a new visitor.

#### Drawbacks

Adding a new type to the type hierarchy requires changes to all visitors.
Encapsulation principle is broken as we need to provide setter methods which allows access to the object's internal state.

#### Real World Examples

Tax Consultants visiting a campus to assist employees in tax filing

#### Java SDK Examples

javax.lang.model.element AnnotationValue and AnnotationValueVisitor
javax.lang.model.element Element and ElementVisitor
javax.lang.model.type TypeMirror and TypeVisitor
java.nio.file FileVisitor and SimpleFileVisitor
javax.faces.component.visit VisitContext and VisitCallback

## Creational Patterns

### Abstract Factory

#### When to use

- To support families of related or dependent objects.
- To encapsulate platform dependencies to make an application portable.
- To prevent client code from using the 'new' operator.
- To easily swap the underlying platform with minimal changes.

#### Intent

Provide an interface for creating families of related or dependent objects without specifying their concrete classes.

#### Components

An Abstract Factory class (public)

Factory Implementations for various familes (protected)

Interfaces for various products (public)

Set of product implementations for various families (protected)

#### Implementation

1. Define interfaces for different types products/objects. Each family will have all these parts.

```java
package com.jaypeesoft.dp.abstractfactory;

public interface Engine {

  public void design();
  public void manufacture();
  public void test();

}
```

```java
package com.jaypeesoft.dp.abstractfactory;

public interface Tyre {

  public void design();
  public void manufacture();

}
```

2. Create sets of implementation subclasses for the above interfaces. Classes are access protected to prohibit instantiations in client modules using the 'new' operator.

```java
package com.jaypeesoft.dp.abstractfactory;

class CarEngine implements Engine {

  @Override
  public void design() {
    System.out.println("Designing Car Engine");
  }

  @Override
  public void manufacture() {
    System.out.println("Manufacturing Car Engine");
  }

  @Override
  public void test() {
    System.out.println("Testing Car Engine");
  }

}
```

```java
package com.jaypeesoft.dp.abstractfactory;

class CarEngine implements Engine {

  @Override
  public void design() {
    System.out.println("Designing Car Engine");
  }

  @Override
  public void manufacture() {
    System.out.println("Manufacturing Car Engine");
  }

  @Override
  public void test() {
    System.out.println("Testing Car Engine");
  }

}
```

```java
package com.jaypeesoft.dp.abstractfactory;

class CarTyre implements Tyre {

  @Override
  public void design() {
    System.out.println("Designing Car Tyre");
  }

  @Override
  public void manufacture() {
    System.out.println("Manufacturing Car Tyre");
  }

}
```

```java
package com.jaypeesoft.dp.abstractfactory;

class TruckTyre implements Tyre {

  @Override
  public void design() {
    System.out.println("Designing Truck Tyre");
  }

  @Override
  public void manufacture() {
    System.out.println("Manufacturing Truck Tyre");
  }

}
```

3. Create a Abstract Factory class with factory method 'getFactory()'. Clients can use this method to get an object the required factory. This example uses both Singleton and Factory Method patterns for better design.

```java
package com.jaypeesoft.dp.abstractfactory;

public abstract class Factory {

  /* Singleton Factory objects */
  private static Factory carFactory = null;
  private static Factory truckFactory = null;

  public abstract Engine getEngine();
  public abstract Tyre getTyre();

  /*
   * This is the factory method exposed to the client.
   * Client requests for a factory instance by passing the type.
   * Client does not need to know about which & how
   * object is created internally.
   */
  public static Factory getFactory(String vehicleType)
      throws UnknownVehicleException {

    if (vehicleType == null) {
      return null;
    }

    Factory factory = null;
    switch (vehicleType) {
      case "car":
        if (carFactory == null)
          carFactory = new CarFactory();
        factory = carFactory;
        break;
      case "truck":
        if (truckFactory == null)
          truckFactory = new TruckFactory();
        factory = truckFactory;
        break;
      default:
        throw new UnknownVehicleException();
    }

    return factory;
  }
}
```

4. Create Factory implementations. Classes are protected to prohibit direct access in client modules.

```java
package com.jaypeesoft.dp.abstractfactory;

class CarFactory extends Factory {

  @Override
  public Engine getEngine() {
    return new CarEngine();
  }

  @Override
  public Tyre getTyre() {
    return new CarTyre();
  }

}
```

```java
package com.jaypeesoft.dp.abstractfactory;

public class TruckFactory extends Factory {

  TruckFactory() {}

  @Override
  public Engine getEngine() {
    return new TruckEngine();
  }

  @Override
  public Tyre getTyre() {
    return new TruckTyre();
  }

}
```

5. The client code. Client is exposed to only the Abstract Factory class and the interfaces.

```java
package com.jaypeesoft.dp.client;

import java.util.Scanner;

import com.jaypeesoft.dp.abstractfactory.Engine;
import com.jaypeesoft.dp.abstractfactory.Factory;
import com.jaypeesoft.dp.abstractfactory.Tyre;
import com.jaypeesoft.dp.abstractfactory.UnknownVehicleException;

public class AbstractFactoryClient {

  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);
    String vehicleType = in.nextLine().toLowerCase();

    /* Get the factory instance */
    Factory factory;
    try {
      factory = Factory.getFactory(vehicleType);

      /* Get the Engine from the factory */
      Engine engine = factory.getEngine();
      engine.design();
      engine.manufacture();
      engine.test();

      /* Get the Tyre from the factory */
      Tyre tyre = factory.getTyre();
      tyre.design();
      tyre.manufacture();

    } catch (UnknownVehicleException e) {
      System.out.println("Invalid vehicle type entered!");
    }

    in.close();
  }
}
```

Output

```
[input1]
    Car
[output1]
    Designing Car Engine
    Manufacturing Car Engine
    Testing Car Engine
    Designing Car Tyre
    Manufacturing Car Tyre
```

```
[input2]
    Bus
[output2]
    Invalid vehicle type entered!
```

#### Benefits

Loosely coupled code.
Abstract Factory provides a single point of access for all products in a family.
New product family can be easily supported.

#### Drawbacks

More layers of abstraction increases complexity.
If there are any changes to any underlying detail of one factory, the interface might need to be modified for all the factories.

#### Real World Examples

Providing data access to two different data sources (e.g. a SQL Database and a XML file). You have two different data access classes (a gateway to the datastore). Both inherit from a base class that defines the common methods to be implemented (e.g. Load, Save, Delete). Which data source shall be used shouldn't change the way client code retrieves it's data access class. Your Abstract Factory knows which data source shall be used and returns an appropriate instance on request. The factory returns this instance as the base class type.

#### Software Examples

Dependency Injection

Java SDK Examples

javax.xml.parsers.DocumentBuilderFactory newInstance()

javax.xml.transform.TransformerFactory newInstance()

javax.xml.xpath.XPathFactory newInstance()

### Builder

#### When to use

To avoid dealing with inconsistent object when the object needs to be created over several steps.

To avoid too many constructor arguments.

To construct an object that should be immutable.

To encapsulate the complete creation logic.

#### Intent

Separate the construction of a complex object from its representation so that the same construction process can create different representations.

#### Components

The Builder class specifies an abstract interface for creating parts of a Product object.

The ConcreteBuilder constructs and puts together parts of the product by implementing the Builder interface. It defines and keeps track of the representation it creates and provides an interface for saving the product.

The Director class constructs the complex object using the Builder interface.

The Product represents the complex object that is being built.

#### Implementation

1. Define the Product (House) that gets assembled in the builder pattern.

```java
package com.jaypeesoft.dp.builder;

/* The house is the object that gets assembled in the builder pattern. */
public class House {

  private String floorType;
  private String wallType;
  private String roofType;

  public String getFloorType() {
    return floorType;
  }

  public void setFloorType(String floorType) {
    this.floorType = floorType;
  }

  public String getWallType() {
    return wallType;
  }

  public void setWallType(String wallType) {
    this.wallType = wallType;
  }

  public String getRoofType() {
    return roofType;
  }

  public void setRoofType(String roofType) {
    this.roofType = roofType;
  }

  public String toString() {
    return new String("\nConstructing House \n FloorType: " + floorType
        + "\n WallType:  " + wallType + "\n RoofType:  " + roofType );
  }
}
```

2. Define the Builder interface (or abstract class) along with Concrete Builders. The Builder interface contains methods for the step by step construction of the product. It also has a build method for retrieving the product object.

```java
package com.jaypeesoft.dp.builder;

public interface HouseBuilder {

  public HouseBuilder buildFloor();
  public HouseBuilder buildWall();
  public HouseBuilder buildRoof();
  public House build();

}
```

3. Concrete Builders implement the Builder interface. A Concrete Builder is responsible for creating and assembling a Product object. Different Concrete Builders create and assemble Product objects differently.

```java
package com.jaypeesoft.dp.builder;

public class ConcreteHouseBuilder implements HouseBuilder {

  private House house;

  public ConcreteHouseBuilder() {
    house = new House();
  }

  public HouseBuilder buildFloor() {
    house.setFloorType("concrete");
    return this;
  }

  public HouseBuilder buildWall() {
    house.setWallType("concrete");
    return this;
  }

  public HouseBuilder buildRoof() {
    house.setRoofType("concrete");
    return this;
  }

  public House build() {
    return house;
  }

}
```

```java
package com.jaypeesoft.dp.builder;

public class WoodenHouseBuilder implements HouseBuilder {

  private House house;

  public WoodenHouseBuilder() {
    house = new House();
  }

  public HouseBuilder buildFloor() {
    house.setFloorType("wood");
    return this;
  }

  public HouseBuilder buildWall() {
    house.setWallType("wood");
    return this;
  }

  public HouseBuilder buildRoof() {
    house.setRoofType("wood");
    return this;
  }

  public House build() {
    return house;
  }

}
```

4. A Director object is responsible for constructing a Product. It does this via the Builder interface to a Concrete Builder. It constructs a Product via the various Builder methods. The director class ensures that all the required operations are performed before the object is returned to the client in a 'consistent' state.

```java
package com.jaypeesoft.dp.builder;

public class HouseBuildDirector {
  private HouseBuilder builder;

  public HouseBuildDirector(final HouseBuilder builder) {
    this.builder = builder;
  }

  public House construct() {
    /* call the necessary methods and return the consistent object*/
    return builder.buildFloor().buildWall().buildRoof().build();
  }

}
```

5. The client code. The Client uses different builder objects to create different types of products. However, the construction process is same.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.builder.ConcreteHouseBuilder;
import com.jaypeesoft.dp.builder.HouseBuildDirector;
import com.jaypeesoft.dp.builder.HouseBuilder;
import com.jaypeesoft.dp.builder.WoodenHouseBuilder;

public class BuilderClient {

  public static void main(final String[] arguments) {

    /* Construct a concrete house */
    HouseBuilder builder = new ConcreteHouseBuilder();
    HouseBuildDirector carBuildDirector = new HouseBuildDirector(builder);
    System.out.println(carBuildDirector.construct());

    /* Construct a wooden house */
    builder = new WoodenHouseBuilder();
    carBuildDirector = new HouseBuildDirector(builder);
    System.out.println(carBuildDirector.construct());
  }
}
```

Output

```
Constructing House
 FloorType: concrete
 WallType:  concrete
 RoofType:  concrete

Constructing House
 FloorType: wood
 WallType:  wood
 RoofType:  wood
```

#### Benefits

Construction process can be controlled by the director.
Useful when many operations have to be done to build an object.
Avoids Telescoping Constructor Pattern.

#### Drawbacks

Not suitable if a mutable object is required.

#### Real World Examples

Building a house - We need to tell the architect what all we want as part of the building. The Architect then designs and constructs the building. It will be handed over only when everything is implemented. We do not get a 'partially' built house (which is unsafe).

#### Java SDK Examples

java.lang.StringBuilder append()
java.lang.StringBuffer append()
java.nio.ByteBuffer put()
javax.swing.GroupLayout.Group addComponent()
java.lang.Appendable implementations

### Factory Method

#### When to use

To enforce coding for interface rather than implementation
To transfer the responsibility of instantiation from the client class to the factory method
To decouple the implementation from the client program

#### Intent

Define an interface for creating an object, but let subclasses decide which class to instantiate. Factory Method lets a class defer instantiation to subclasses.

#### Components

An Interface (or) Abstract class (public)
Set of implementation subclasses (private)
A Factory Method (public)

#### Implementation

1. Create an interface. Clients can code for this interface without worrying about the internal implementation.

```java
package com.jaypeesoft.dp.factorymethod;

public interface Vehicle {
  void design();
  void manufacture();
}
```

2. Create a set of implementation subclasses. Constructors are protected to prohibit instantiations in clients modules using the 'new' operator.

```java
package com.jaypeesoft.dp.factorymethod;

public class Car implements Vehicle {

  Car() {
    /* constructor is protected.
       clients need to use the factory method */
  }

  @Override
  public void design() {
    System.out.println("Designing Car");
  }

  @Override
  public void manufacture() {
    System.out.println("Manufacturing Car");
  }

}
```

```java
package com.jaypeesoft.dp.factorymethod;

public class Truck implements Vehicle {

  Truck() {
    /* constructor is protected.
       clients need to use the factory method */
  }

  @Override
  public void design() {
    System.out.println("Designing Truck");
  }

  @Override
  public void manufacture() {
    System.out.println("Manufacturing Truck");
  }

}
```

```java
package com.jaypeesoft.dp.factorymethod;

public class Motorcycle implements Vehicle {

  Motorcycle() {
    /* constructor is protected.
       clients need to use the factory method */
  }

  @Override
  public void design() {
    System.out.println("Designing Motorcycle");
  }

  @Override
  public void manufacture() {
    System.out.println("Manufacturing Motorcycle");
  }

}
```

3. Create a class with method 'getVehicle()'. Clients can use this method to create an object instead of using 'new' operator.

```java
package com.jaypeesoft.dp.factorymethod;

public class VehicleFactory {

  /* This is the factory method exposed to the client.
     Client requests for an object by passing the type.
     Client does not need to know about which & how object
     is created internally.
     */
  public Vehicle getVehicle(String vehicleType)
      throws VehicleTypeNotFoundException {

    if (vehicleType == null) {
      return null;
    }

    Vehicle vehicle = null;

    switch (vehicleType) {
      case "car":
        vehicle = new Car();
        break;
      case "truck":
        vehicle = new Truck();
        break;
      case "motorcycle":
        vehicle = new Motorcycle();
        break;
      default:
        throw new VehicleTypeNotFoundException();
    }

    return vehicle;
  }

}
```

4. The client code. Client knows only the factory method and the interface. Client code does not use 'new' hence decoupled from implementation

```java
package com.jaypeesoft.dp.client;

import java.util.Scanner;

import com.jaypeesoft.dp.factorymethod.Vehicle;
import com.jaypeesoft.dp.factorymethod.VehicleFactory;
import com.jaypeesoft.dp.factorymethod.VehicleTypeNotFoundException;

public class FactoryMethodClient {

  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);
    String vehicleType = in.nextLine().toLowerCase();

    /* Create a factory instance */
    VehicleFactory factory = new VehicleFactory();

    try {

      /* Create an appropriate vehicle based on the input */
      Vehicle vehicle = factory.getVehicle(vehicleType);

      /* Design and manufacture the vehicle */
      vehicle.design();
      vehicle.manufacture();

    } catch (VehicleTypeNotFoundException e) {
      System.out.println("Invalid vehicle type entered!");
    }

    in.close();
  }

}
```

Output

```
[input1]
    MotorCycle
[output1]
    Designing Motorcycle
    Manufacturing Motorcycle
```

```
[input2]
    Car
[output2]
    Designing Car
    Manufacturing Car
```

```
[input3]
    Bus
[output3]
    Invalid vehicle type entered!
```

#### Benefits

Loose coupling allows changing the internals without impacting the customer code
Factory method provides a single point of control for multiple products
Number of instances and their reusability can be controlled with Singleton or Multiton

#### Drawbacks

An extra level of abstraction makes the code more difficult to read

#### Real World Examples

Renting Vehicles. Customer needs to specify only the type of vehicle (car, truck, etc.) that is needed. Customer need not know about the internal details of the vehicle.

#### Software Examples

Memcache
Filecache
Code for SQL standard without worrying about the underlying DB

#### Java SDK Examples

java.util.Calendar.getInstance()
java.util.ResourceBundle.getBundle()
java.text.NumberFormat.getInstance()
java.nio.charset.Charset.forName()
java.util.EnumSet.of()
javax.xml.bind.JAXBContext.createMarshaller()

### Prototype

#### When to use

To improve the performance when object creation is costly and time consuming.
To simplify and optimize multiple objects creation that will have mostly the same data

#### Intent

Specify the kinds of objects to create using a prototypical instance, and create new objects by copying this prototype.

#### Components

A class that implements Cloneable interface (public)

#### Implementation

Most of the cloud platforms behave like Prototype pattern to create instances quickly upon requests. This is achieved with the help of pre-installed Machine Images. When a customer wants a Windows or Linux instance, the cloud software just loads the already created machine image on to a server hardware (rather than going through the complete installation process). This saves a lot of time.

1. Create a base machine image by installing the specified OS and antivirus software. Provide a 'clone' method so that client can create an object without using a time consuming 'new' operator every time.

```java
package com.jaypeesoft.dp.prototype;

public class MachineImage implements Cloneable {

  StringBuilder image;

  public MachineImage(String os, String antivirusSW) {
    image = new StringBuilder();
    image.append(os)
         .append(" + " + antivirusSW);
  }

  private MachineImage(String sw) {
    image = new StringBuilder(sw);
  }

  public void install(String sw) {
    image.append(sw);
  }

  public void printSw() {
    System.out.println(image);
  }

  @Override
  public MachineImage clone()
      throws CloneNotSupportedException {
    return new MachineImage(this.image.toString());
  }
}
```

2. The client code. The client creates a base image and clones it to create other images.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.prototype.MachineImage;

public class PrototypeClient {
  public static void main(String [] args)
      throws CloneNotSupportedException {

    /* Create base Machine Images */
    MachineImage linuxVM= new MachineImage("Linux", "Symantec");
    MachineImage windowsVM = new MachineImage("Windows", "MaAfee");

    /* Clone Linux VM and Install Web Server */
    MachineImage webServer= linuxVM.clone();
    webServer.install(" + Web Server S/W");

    /* Create a copy of the Web Server and
     * install Application server on top of it */
    MachineImage webAppServer = webServer.clone();
    webAppServer.install(" + App Server S/W");

    /* Clone Linux VM and Install DB Server */
    MachineImage dbServer= linuxVM.clone();
    dbServer.install(" + Database Server S/W");

    /* Create a test machine from Windows Image */
    MachineImage testMachine= windowsVM.clone();

    System.out.print("Web Server Configuration: ");
    webServer.printSw();
    System.out.print("App Server Configuration: ");
    webAppServer.printSw();
    System.out.print("DB Server Configuration: ");
    dbServer.printSw();
    System.out.print("Test Machine Configuration: ");
    testMachine.printSw();
  }
}
```

Output

```
Web Server Configuration: Linux OS + Antivirus S/W + Web Server S/W
App Server Configuration: Linux OS + Antivirus S/W + Web Server S/W + App Server S/W
DB Server Configuration: Linux OS + Antivirus S/W + Database Server S/W
Test Machine Configuration: Linux OS + Antivirus S/W
```

#### Benefits

Performance: Cloning (using MemberwiseClone) is considerably less expensive than creating a new object afresh (with new operator).
Objects can be cloned very dynamically, without any insistence on up-front instantiation. The first created object can be created at any time in the application execution, and further duplication can take place at any time ahead.

#### Drawbacks

Deep copy has to be handled carefully.

#### Real World Examples

Biological Cell splitting

#### Software Examples

Virtual Machine Images - Have one image per OS which has all the required s/w installed.
DVD duplication - Duplication of the master dvd to create several copies.

#### Java SDK Examples

java.lang.Object clone()

### Singleton

#### When to use

Application needs “only one instance” of a class.
To have complete control over the instance creation.

#### Intent

Ensure a class has only one instance, and provide a global point of access to it.

#### Components

Singleton Class

#### Implementation

Create a Singleton class with a static instance and a private constructor. Provide a static method to allow access to this 'only' instance.

1. Create a Singleton Class

```java
package com.jaypeesoft.dp;

public class Singleton {

  /* the singleton obj */
  private static Singleton instance = null;

  /* private constructor to avoid
     external instantiation of this class */
  private Singleton() {
  }

  /* method to get the singleton obj */
  public static Singleton getInstance() {
    if (instance == null) {
      synchronized (Singleton.class) {
        /* double checked locking */
        if (instance == null) {
          instance = new Singleton();
        }
      }
    }
    return instance;
  }

  public void printObj() {
    System.out.println("Unique Id of the obj: "
    + System.identityHashCode(this));
  }

}
```

2. Access the singleton instance

```java
class Demo {

  public static void main(String[] args) {
    Singleton obj1 = Singleton.getInstance();
    obj1.printObj();
    Singleton obj2 = Singleton.getInstance();
    obj2.printObj();
  }

}
```

Output

```
Unique Id of the obj: 1916222108
Unique Id of the obj: 1916222108
```

#### Benefits

Controlled instantiation
Supports both EAGER and LAZY initializations
Singletons can be converted to Multitons (to support limited number of instances identified by keys)

#### Drawbacks

Singleton is often seen as 'not-so-good' design as it resembles global variables
Special handling is required if the Singleton object needs to be deleted
Singletons that maintain global state may cause issues

#### Real World Examples

The Office of the President (there can be only one President at any given time)

#### Software Examples

Logger classes
Window Manager
Printer Spooler

#### Java SDK Examples

java.lang.Runtime.getRuntime( )
java.awt.Toolkit.getDefaultToolkit( )
java.awt.Desktop.getDesktop( )
java.util.logging.LogManager.getLogManager( )
java.lang.System#getSecurityManager( )

## Structural Patterns

### Adapter

#### When to use

To wrap an existing class with a new interface.
To perform impedance matching

#### Intent

Convert the interface of a class into another interface clients expect. Adapter lets classes work together that couldn't otherwise because of incompatible interfaces.

#### Components

Target - defines the domain-specific interface that Client uses.
Adapter - adapts the interface Adaptee to the Target interface.
Adaptee - defines an existing interface that needs adapting.
Client - collaborates with objects conforming to the Target interface.

#### Implementation

Assume that you have an e-commerce application which is serving your customers for a long time. This e-commerce application is using a Legacy Order Management System (OMS). Due to the high maintenance cost and degraded performance of the legacy OMS software, you have decided to use a cheap and efficient OMS software which is readily available in the market. However, you realize that the interfaces are different in the new software and it requires a lot of code change in the existing e-commerce application.
Adapter design pattern can be very useful in these situations. Instead of modifying your e-commerce application to use the new interfaces, you can write a 'wrapper' class that acts as a bridge between your e-commerce application and the new OMS software. With this approach, the e-commerce application can still use the old interface.
Adapter design pattern can be implemented in two ways. One using the inheritance method (Class Adapter) and second using the composition (Object Adapter). The following example depicts the implementation of Object adapter.

1. Below is the code that uses the LegacyOMS.

```java
package com.jaypeesoft.dp.adapter;

public class Item {
  private String name;
  private double price;

  public Item(String name, double price) {
    this.name = name;
    this.price = price;
  }

  public String getName() {
    return name;
  }

  public double getPrice() {
    return price;
  }
}
```

```java
package com.jaypeesoft.dp.adapter;

public class Payment {

  public String type;
  public double amount;

  public Payment(String type, double amount) {
    super();
    this.type = type;
    this.amount = amount;
  }

  public void pay() {
    System.out.println(type + " " + amount + "$");
  }

}
```

```java
package com.jaypeesoft.dp.adapter;

import java.util.ArrayList;
import java.util.List;

public class LegacyOMS {

  /* The Legacy OMS accepts input in XML format */

  List cart = new ArrayList();
  List payments = new ArrayList();

  public void addItem(Item itemXml) {
    cart.add(itemXml);
    System.out.println(itemXml.getName() + " " + itemXml.getPrice());
  }

  public void makePayment(Payment paymentXml) {
    payments.add(paymentXml);
    paymentXml.pay();
  }
}
```

2. The client code.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.adapter.Item;
import com.jaypeesoft.dp.adapter.OMSAdapter;
import com.jaypeesoft.dp.adapter.Payment;

public class AdapterClient {

  public static void main(String[] args) {

    /* Create an order and add items */

    LegacyOMS oms = new LegacyOMS();

    oms.addItem(new Item("Italian Pizza", 6.99));
    oms.addItem(new Item("Wine", 9.99));
    oms.addItem(new Item("Beer", 5.99));
    oms.addItem(new Item("Red Apple", 1.49));
    oms.addItem(new Item("Almonds", 11.99));

    System.out.println("---------------------------------");
    /* Create payment and make payment */
    oms.makePayment(new Payment("CASH", 20.00));
    oms.makePayment(new Payment("CREDIT", 10.00));
    oms.makePayment(new Payment("DEBIT", 10.00));
    System.out.println("---------------------------------");

  }
}
```

3. When the OMS needs to be swapped, you can simply create an Adapter class with same interface that the client uses. This adapter/wrapper class "maps" the client interface to the adaptee (New OMS) interface.

```java
package com.jaypeesoft.dp.adapter;

import java.util.ArrayList;
import java.util.List;

public class NewOMS {

  /* The new OMS accepts input in JSON format */

  List cart = new ArrayList();
  List payments = new ArrayList();

  public void addToBasket(Item itemJson) {
    cart.add(itemJson);
    System.out.println(itemJson.getName() + " " + itemJson.getPrice());
  }

  public void pay(Payment paymentJson) {
    payments.add(paymentJson);
    paymentJson.pay();
  }
}
```

```java
package com.jaypeesoft.dp.adapter;

public class OMSAdapter {

  /* Object Adapter uses composition */
  private NewOMS newOMS;

  public OMSAdapter() {
    newOMS = new NewOMS();
  }

  public void addItem(Item item) {
    convertXmlToJson(item);
    newOMS.addToBasket(item);
  }

  public void makePayment(Payment p) {
    convertXmlToJson(p);
    newOMS.pay(p);
  }

  /* The new OMS accepts only Json input.
   * Convert the client requests from XML to Json*/
  private void convertXmlToJson(Object o) {
    System.out.println("Converted from XML to JSON");
  }
}
```

4. The new client code. The client interacts in the same way as before.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.adapter.Item;
import com.jaypeesoft.dp.adapter.OMSAdapter;
import com.jaypeesoft.dp.adapter.Payment;

public class AdapterClient {

  public static void main(String[] args) {

    /* Create an order and add items */

    //LegacyOMS oms = new LegacyOMS();
    /* Use Adapter class with the same interface */
    OMSAdapter oms = new OMSAdapter();

    oms.addItem(new Item("Italian Pizza", 6.99));
    oms.addItem(new Item("Wine", 9.99));
    oms.addItem(new Item("Beer", 5.99));
    oms.addItem(new Item("Red Apple", 1.49));
    oms.addItem(new Item("Almonds", 11.99));

    System.out.println("---------------------------------");
    /* Create payment and make payment */
    oms.makePayment(new Payment("CASH", 20.00));
    oms.makePayment(new Payment("CREDIT", 10.00));
    oms.makePayment(new Payment("DEBIT", 10.00));
    System.out.println("---------------------------------");

  }
}
```

Output

```
Italian Pizza   6.99
Wine            9.99
Beer            5.99
Red Apple       1.49
Almonds        11.99
CASH    20.0$
CREDIT  10.0$
DEBIT   10.0$
```

#### Benefits

Class adapter can override adaptee's behavior.
Objects adapter allows a single adapter to work with many adaptees.
Helps achieve reusability and flexibility.
Client class is not complicated by having to use a different interface and can use polymorphism to swap between different implementations of adapters.

#### Drawbacks

Object adapter involves an extra level of indirection.

#### Real World Examples

Power adapters
Memory card adapters

#### Software Examples

Wrappers used to adopt 3rd parties libraries and frameworks.

#### Java SDK Examples

java.util.Arrays asList()
java.util.Collections list()
java.util.Collections enumeration()
java.io.InputStreamReader(InputStream) (returns a Reader)
java.io.OutputStreamWriter(OutputStream) (returns a Writer)

### Bridge

#### When to use

When run-time binding of the implementation is required.
To support a proliferation of classes resulting from a coupled interface and numerous implementations,
To share an implementation among multiple objects and to map orthogonal class hierarchies.

#### Intent

Decouple an abstraction from its implementation so that the two can vary independently.

#### Implementation

The Bridge pattern is an application of the old advice, "prefer composition over inheritance". It becomes handy when you must subclass different times in ways that are orthogonal with one another. Say you must implement a hierarchy of colored shapes. You wouldn't want to subclass Shape with Rectangle and Circle and then subclass Rectangle with RedRectangle, BlueRectangle and GreenRectangle and the same for Circle. You would prefer to say that each Shape has a Color and to implement a hierarchy of colors, and that is the Bridge Pattern.

1. Design a color-oriented interface that is minimal, necessary, and sufficient. Its goal is to decouple the abstraction from the color. Define a derived class of that interface for each color.

```java
package com.jaypeesoft.dp.bridge;

public interface Color {

  void applyColor();

}
```

2. Define a derived class of that interface for each color.

```java
package com.jaypeesoft.dp.bridge;

public class Red implements Color {

  public void applyColor() {

    System.out.println("Red");

  }

}
```

```java
package com.jaypeesoft.dp.bridge;

public class Green implements Color {

  public void applyColor() {

    System.out.println("Green");

  }

}
```

3. Create the abstraction base class that "has a" color object and delegates the color-oriented functionality to it.

```java
package com.jaypeesoft.dp.bridge;

public abstract class Shape {

  protected Color color;

  public Shape(Color c) {
    color = c;
  }

  public abstract void draw();

}
```

4. Define specializations of the abstraction class.

```java
package com.jaypeesoft.dp.bridge;

public class Circle extends Shape {

  public Circle(Color color) {
    super(color);
  }

  public void draw() {
    System.out.print("Draw Circle in ");
    color.applyColor();
  }

}
```

```java
package com.jaypeesoft.dp.bridge;

public class Rectangle extends Shape {
  public Rectangle(Color color) {
      super(color);
  }

  public void draw() {
    System.out.print("Draw Rectangle in ");
    color.applyColor();
  }

}
```

```java
package com.jaypeesoft.dp.bridge;

public class Square extends Shape {

  public Square(Color color) {
      super(color);
  }

  public void draw() {
    System.out.print("Draw Square in ");
    color.applyColor();
  }

}
```

5. The Client code. The Bridge Pattern allows one to mix and match without needing to create a rigid hierarchy.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.bridge.Circle;
import com.jaypeesoft.dp.bridge.Green;
import com.jaypeesoft.dp.bridge.Rectangle;
import com.jaypeesoft.dp.bridge.Red;
import com.jaypeesoft.dp.bridge.Shape;
import com.jaypeesoft.dp.bridge.Square;

public class BridgeClient {

  public static void main(String[] args) {

    Shape[] shapes = {
        new Circle(new Red()),
        new Square(new Red()),
        new Rectangle(new Green())
        };

    for (Shape shape : shapes) {
      shape.draw();
    }

  }

}
```

Output

```
Draw Circle in Red
Draw Square in Red
Draw Rectangle in Green
```

#### Benefits

Decoupling allows us to choose the implementations at runtime.
Compile-time dependencies on the implementation are eliminated.
Improved extensibility and flexibility.

#### Drawbacks

The delegation from the Entities to the Behaviors can degrade performance.

#### Real World Examples

The display of different image formats on different operating systems is a good example of the Bridge pattern. You might have different image abstractions for both jpeg and png images. The image structure is the same across all operating systems, but the how it's viewed (the implementation) is different on each OS. This is the type of decoupling that the Bridge pattern allows.

#### Software Examples

UnifiedPOS or UPOS that provides vendor-neutral APIs for numerous Point Of Sale peripherals.
OS specific Device Driver interfaces that define common standards for various devices.

### Composite

#### When to use

To have a hierarchical collection of primitive and composite entities.
To create a structure in a way that the objects in the structure can be treated the same way.

#### Intent

Compose objects into tree structures to represent part-whole hierarchies. Composite lets clients treat individual objects and compositions of objects uniformly.

#### Components

An interface for all objects in the composition
A leaf element which is the building block of the composition
A composite element which can contain leaf elements and/or composites

#### Implementation

Typical example is the File system which contains directories and files. A directory can contain files or sub directories but both have to be handled in the same way.
In the following example, boxes and products are implemented using the Composite pattern. A box can contain many products and boxes. We need a common way to interact with both entities.

1. Create a common interface which declares a method to print the item.

```java
package com.jaypeesoft.dp.composite;

public interface Item {

  public void print(int level);

}
```

2. Create the leaf object that implements the interface.

```java
package com.jaypeesoft.dp.composite;

public class Product implements Item {

  int id;

  public Product(int id) {
    this.id = id;
  }

  public void print(int level) {
    for (int i = 0; i < level; i++)
      System.out.print("   ");
    System.out.println("Product" + id);
  }
}
```

3. Create the composite object that implements the interface.

```java
package com.jaypeesoft.dp.composite;

import java.util.ArrayList;
import java.util.List;

public class Box implements Item {

  int id;

  private List items = new ArrayList();

  public Box(int id) {
    this.id = id;
  }

  public void print(int level) {
    for (int i = 0; i < level; i++)
      System.out.print("   ");
    System.out.println("Box" + id);
    for (Item item : items) {
      item.print(level + 1);
    }
  }

  public void add(Item item) {
    items.add(item);
  }

  public void remove(Item item) {
    items.remove(item);
  }
}
```

4. The client code. The client creates leaf elements and composite elements and interact with them using the common interface.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.composite.Box;
import com.jaypeesoft.dp.composite.Product;

public class CompositeClient {

  public static void main(String[] args) {

    // Initialize four products
    Product product1 = new Product(1);
    Product product2 = new Product(2);
    Product product3 = new Product(3);
    Product product4 = new Product(4);

    // Initialize three boxes
    Box box1 = new Box(1);
    Box box2 = new Box(2);
    Box box3 = new Box(3);

    // Put 3 items in box1
    box1.add(product1);
    box1.add(product2);
    box1.add(product3);

    // Put item4 in box2
    box2.add(product4);

    // Put box1 and box2 in box3
    box3.add(box1);
    box3.add(box2);

    // Print the contents of box3
    box3.print(0);
  }
}
```

Output

```
Box3
   Box1
      Product1
      Product2
      Product3
    Box2
        Product4
```

#### Benefits

Simplifies the representation of part-whole hierarchies.
Clients can treat all objects in the composite structure uniformly.

#### Drawbacks

Strict restrictions need to be enforced otherwise the tree structure may become overly generalized.

#### Real World Examples

Organization structure with manager and reportees. The reportees could be managers who may have their own reportees.

#### Software Examples

File system (directories and files)

#### Java SDK Examples

java.awt Container#add(Component) (practically all over Swing thus)
javax.faces.component UIComponent#getChildren() (practically all over JSF UI thus)

### Decorator

#### When to use

To dynamically change the functionality of an object at runtime without impacting the existing functionality of the objects.
To add functionalities that may be withdrawn later.
To combine multiple functionalities where it is impractical to create a subclass for every possible combination.

#### Intent

Attach additional responsibilities to an object dynamically. Decorators provide a flexible alternative to subclassing for extending functionality.

#### Problem

Assume that you need to prepare a pizza that may have multiple combinations of toppings, cooking type, etc. Though this can be achieved by Inheritance, it is not practical to create subclasses for every possible combination. Alternatively you can use Composition and add the required functionalities. Since all concrete implementations conform to the same interface, we can mix and match any number of classes to create a variety of combinations.

#### Implementation

1. Define Item interface and create a Pizza class which is an implementation of the Item interface.

```java
package com.jaypeesoft.dp.decorator;

public interface Item {
  void prepare();
}
```

```java
package com.jaypeesoft.dp.decorator;

public class Pizza implements Item {
  public void prepare() {
      System.out.print("Pizza");
  }
}
```

2. Create an abstract Decorator class which implements the item interface. This class contains the Item object which will be decorated with new functionalities.

```java
package com.jaypeesoft.dp.decorator;

abstract class PizzaDecorator implements Item {
  private Item pizza;

  public PizzaDecorator(Item item) {
    pizza = item;
  }

  public void prepare() {
    pizza.prepare();
  }
}
```

3. Create Concrete Decorators.

```java
package com.jaypeesoft.dp.decorator;

public class DeepFried extends PizzaDecorator {
  public DeepFried(Item inner) {
      super(inner);
  }

  public void prepare() {
      super.prepare();
      System.out.print(" + Deep Fried");
  }

}
```

```java
package com.jaypeesoft.dp.decorator;

public class DoubleCheese extends PizzaDecorator {
  public DoubleCheese(Item inner) {
    super(inner);
  }

  public void prepare() {
    super.prepare();
    System.out.print(" + Double Cheese");
  }

}
```

```java
package com.jaypeesoft.dp.decorator;

public class Spicy extends PizzaDecorator {
  public Spicy(Item inner) {
      super(inner);
  }

  public void prepare()  {
      super.prepare();
      System.out.print(" + Spicy");
  }

}
```

4. The Client code. The Decorator Pattern allows one to mix and match without needing to create a rigid hierarchy.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.decorator.DeepFried;
import com.jaypeesoft.dp.decorator.DoubleCheese;
import com.jaypeesoft.dp.decorator.Item;
import com.jaypeesoft.dp.decorator.Pizza;
import com.jaypeesoft.dp.decorator.Spicy;

public class DecoratorClient {
  public static void main( String[] args ) {
      Item[] order = {
          new DeepFried(new Pizza()),
          new DeepFried(new DoubleCheese(new Pizza())),
          new DoubleCheese(new Spicy(new DeepFried(new Pizza())))
        };
      for (Item item : order) {
          item.prepare();
          System.out.println("  ");
      }
  }
}
```

Output

```
Pizza + Deep Fried
Pizza + Double Cheese + Deep Fried
Pizza + Deep Fried + Spicy + Double Cheese
```

#### Benefits

Decorator allows us to mix and match features instead of creating concrete implementations for all possible combinations.
Decorator allows us to modify an object in a much more modular and less fundamental way than inheritance would.
New functionalities can be easily supported.

#### Drawbacks

Multiple small objects are created in the process of creating an object.
Complexity is increased.

#### Programming Examples

File Stream implementations

#### Java SDK Examples

All subclasses of java.io.InputStream, OutputStream, Reader and Writer have a constructor taking an instance of same type.
java.util.Collections, the checkedXXX(), synchronizedXXX() and unmodifiableXXX() methods.
javax.servlet.http.HttpServletRequestWrapper and HttpServletResponseWrapper

### Facade

#### When to use

To provide a simplified interface to the overall functionality of a complex subsystem.
To promote subsystem independence and portability.

#### Intent

Provide a unified interface to a set of interfaces in a subsystem. Facade defines a higher-level interface that makes the subsystem easier to use.

#### Implementation

Assume that we have set of interfaces for a system that includes many subsystems. The client application can use these interfaces to perform the required operation. But when the complexity increases, client application will find it difficult to manage it. By using the Facade pattern, we can hide the complexities of the system and provide an interface to the client using which the client can access the system.

1. A typical example for the Facade pattern is the wedding planner who hides from you the complexity of a large subsystem. The wedding planner orders flowers, makes reservations, organizes everything for you.

```java
package com.jaypeesoft.dp.facade;

class Hall {

  public void book() {
    System.out.println("Book Marriage Hall");
  }

}

class Restaurant {

  public void placeOrder() {
    System.out.println("Order food");
  }

}

class Photographer {

  public void book() {
    System.out.println("Book photographer");
  }

}

class Vehicle {

  public void reserve() {
    System.out.println("Reserve vehicle");
  }

}

public class WeddingPlanner {

  /* Facade class to hide the complexity */

  private Hall hall;
  private Restaurant restaurant;
  private Photographer photographer;
  private Vehicle limousine;

  public WeddingPlanner() {
    hall = new Hall();
    photographer = new Photographer();
    restaurant = new Restaurant();
    limousine = new Vehicle();
  }

  /* simplified interface exposed to the client */
  public void organize() {
    hall.book();
    restaurant.placeOrder();
    photographer.book();
    limousine.reserve();
  }
}
```

2. The client code. The client contacts the facade class to perform the required operation.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.facade.WeddingPlanner;

public class FacadeClient {

  public static void main(String[] args) {
    WeddingPlanner planner = new WeddingPlanner();
    planner.organize();
  }

}
```

Output

```
Book Marriage Hall
Order food
Book photographer
Reserve vehicle
```

#### Benefits

Number of objects the client interact with is minimal which reduces the compilation complexity.
Promotes loose coupling.
Facade still allows the client to use the subsystem interfaces.

#### Drawbacks

One more layer is introduced in the system which may impact the performance.

#### Real World Examples

Customer Support Desk which hides all complexities of the system that involves various departments.
Event Planner who does everything including like making reservations, organizing activities, etc.

#### Java SDK Examples

javax.faces.context.FacesContext, it internally uses among others the abstract/interface types LifeCycle, ViewHandler, NavigationHandler and many more without that the enduser has
javax.faces.context.ExternalContext, which internally uses ServletContext, HttpSession, HttpServletRequest, HttpServletResponse, etc.

### Flyweight

#### When to use

To improve the performance when large number of objects need to be created.
When most of the object attributes can be made external and shared.

#### Intent

Use sharing to support large numbers of fine-grained objects efficiently.

#### Implementation

Assume that you want to create a car racing game where there will be one player, few opponents, and a bunch of traffic vehicles. These traffic vehicles need to appear throughout the game and there can be hundreds of such vehicles appearing during the course of the race. All these traffic vehicles have a common mission (that is creating traffic on the road) but the type, color, size of the vehicles can vary. Creating each and every traffic vehicle will increase the load on the memory. Flyweight pattern can be applied here to improve the performance and reduce the memory usage. It is achieved by segregating object properties into two types: intrinsic and extrinsic.
In a race, a traffic vehicle needs to appear only for a shorter time. So, it is not required to create all the traffic vehicles at once. Instead, a bunch of traffic vehicles can be created. These vehicles will have intrinsic state like the task and vehicle id. Other properties like the vehicle type, color, size, etc. can be made extrinsic so that different types of vehicles can be created based on the input. When a traffic vehicle needs to appear, it is retrieved from the vehicle pool and the extrinsic properties are applied to change the appearance of the vehicle. When the traffic vehicle goes out of the scene, it can be returned to the pool so that it can be reused.

1. Create the vehicle class. Intrinsic and extrinsic characteristics are carefully chosen.

```java
package com.jaypeesoft.dp.flyweight;

import java.util.Random;
import java.util.Vector;

class Vehicle {

  private String name;
  private final String task;
  private String type;
  private String color;
  private int speed;
  private boolean active;
  private int duration;

  public Vehicle(String name) {
    /* Intrinsic state of the object */
    task = "Obstruct the racers";
    this.name = name;
  }

  public void setProperties(String type, String color, int speed, int duration) {
    /* Extrinsic state of the object */
    this.type = type;
    this.color = color;
    this.speed = speed;
    this.duration = duration;
  }

  public boolean isActive() {
    return active;
  }

  public void addToTraffic() {

    /* add the vehicle to the traffic */
    System.out.println("->" + name + "-" + type + "-" + color + "-" + speed
        + "mph-" + duration + "seconds");

    /* Create a timer task to take the vehicle out
     * from the traffic after the duration */
    new java.util.Timer().schedule(new java.util.TimerTask() {
      @Override
      public void run() {
        active = false;
        System.out.println(name+"->out" );
      }
    }, duration*1000);
    active = true;
  }

}
```

2. Create the vehicle factory class. A bunch of vehicles are created and added to the vehicle pool. When a traffic vehicle is requested, it is retrieved from the pool and the required properties are set.

```java
class VehicleFactory {

  private Vector pool = new Vector();

  public VehicleFactory() {
    for (int i = 0; i < 5; i++) {
      /* Create traffic vehicles and add it to the vehicle pool */
      pool.add(new Vehicle("v" + (i + 1)));
    }
  }

  /* Retrieve a vehicle from the pool and set the properties */
  public Vehicle getVehicle(String type, String color, int speed, int duration) {
    for (Vehicle v : pool) {
      if (!v.isActive()) {
        /* set the extrinsic properties */
        v.setProperties(type, color, speed, duration);
        return v;
      }
    }
    return null;
  }
}
```

3. The client code. The client requests a vehicle by passing the required extrinsic properties.

```java
public class FlyweightClient {
  static Random r = new Random();

  private static String[] types = { "bus", "truck", "car" };
  private static String[] colors = { "red", "green", "blue" };
  private static int[] speeds = { 50, 30, 80 };

  public static void main(String args[]) throws InterruptedException {

    VehicleFactory factory = new VehicleFactory();

    /* Create traffic vehicles */
    for (int i = 0; i < 20; i++) {
      Vehicle v = factory.getVehicle(getRandType(),
          getRandColor(), getRandSpeed(), (r.nextInt(5)+1));
      if (v != null) {
        /* free vehicle object found, add to the traffic */
        System.out.print("vehicle" + (i+1) );
        v.addToTraffic();
      }
      else {
        i--;
        /* all objects in use, wait and try again */
        Thread.sleep(1000);
      }
    }
  }

  public static String getRandType() {
    return types[r.nextInt(types.length)];
  }

  public static String getRandColor() {
    return colors[r.nextInt(colors.length)];
  }

  public static int getRandSpeed() {
    return speeds[r.nextInt(speeds.length)];
  }

}
```

Output

```
vehicle1->v1-truck-red-50mph-2seconds
vehicle2->v2-bus-blue-30mph-2seconds
vehicle3->v3-bus-blue-80mph-4seconds
vehicle4->v4-truck-red-30mph-2seconds
vehicle5->v5-car-red-80mph-3seconds
v4->out
v1->out
v2->out
vehicle6->v1-truck-red-50mph-5seconds
vehicle7->v2-car-red-50mph-5seconds
vehicle8->v4-bus-red-30mph-4seconds
v5->out
vehicle9->v5-bus-blue-50mph-1seconds
v3->out
vehicle10->v3-car-green-30mph-4seconds
v5->out
vehicle11->v5-car-blue-80mph-3seconds
v4->out
vehicle12->v4-truck-blue-80mph-2seconds
v1->out
v2->out
v5->out
vehicle13->v1-car-red-50mph-2seconds
vehicle14->v2-truck-blue-50mph-2seconds
vehicle15->v5-car-green-30mph-5seconds
v3->out
v4->out
vehicle16->v3-car-red-30mph-2seconds
vehicle17->v4-truck-red-80mph-4seconds
v1->out
v2->out
vehicle18->v1-truck-red-30mph-2seconds
vehicle19->v2-car-blue-50mph-5seconds
v3->out
vehicle20->v3-bus-blue-50mph-4seconds
v1->out
v5->out
v4->out
v2->out
v3->out
```

#### Benefits

The total number of instances can be reduced
Objects sharing reduces the total memory used

#### Drawbacks

May introduce run-time costs associated with transferring, finding, and/or computing extrinsic state, especially if it was formerly stored as intrinsic state.

#### Java SDK Examples

java.lang.Integer valueOf(int) (also on Boolean, Byte, Character, Short, Long and BigDecimal)

### Proxy

#### When to use

To provide controlled access to a sensitive master object
To provide a local reference to a remote object
To improve performance when an object needs to be accessed frequently

#### Intent

Provide a surrogate or placeholder for another object to control access to it.

#### Implementation

Let us see the example of a Proxy Server. A proxy server, also known as a "proxy" or "application-level gateway", is a computer that acts as a gateway between a local network (e.g., all the computers at one company or in one building) and a larger-scale network such as the Internet. Proxy servers provide increased performance and security. A proxy server can act as an intermediary between the user's computer and the Internet to prevent from attack and unexpected access.
A proxy server will behave just like the real server and implements the same interface.

1. Create an interface. Both the real subject and the proxy subject will implement this interface.

```java
package com.jaypeesoft.dp.proxy;

public interface Server {
  public void authenticate();
  public void get();
  public void post();
  public void put();
  public void delete();
  public void logout();
}
```

2. Implement the real subject and the proxy subject. Note that the RealServer class is protected so that the client cannot access it directly.

```java
package com.jaypeesoft.dp.proxy;

class RealServer implements Server{

  public void authenticate() {
    System.out.println("Logged into the Real Server");
  }

  public void get() {
    System.out.println("GET command executed");
  }

  public void post() {
    System.out.println("POST command executed");
  }

  public void put() {
    System.out.println("PUT command executed");
  }

  public void delete() {
    System.out.println("DELETE command executed");
  }

  public void logout() {
    System.out.println("Logged out from the Real Server");
  }
}
```

```java
package com.jaypeesoft.dp.proxy;

public class ProxyServer implements Server{

  /* Reference to RealServer */
  private RealServer realServer;
  private boolean sessionActive;

  public ProxyServer() {
    this.realServer = new RealServer();
    sessionActive = false;
  }

  public void authenticate() {

    /* Get the user credentials and login */
    realServer.authenticate();

    /* Track the session */
    sessionActive = true;

  }

  public void get() {

    if(sessionActive)
      realServer.get();
    else
      System.out.println("Invalid Session");

  }

  public void post() {

    if(sessionActive)
      realServer.post();
    else
      System.out.println("Invalid Session");

  }

  public void put() {

    if(sessionActive)
      realServer.put();
    else
      System.out.println("Invalid Session");

  }

  public void delete() {

    if(sessionActive)
      realServer.delete();
    else
      System.out.println("Invalid Session");

  }

  public void logout() {

    realServer.logout();

    /* Deactivate the session */
    sessionActive = false;

  }

}
```

3. The client code. The client does not have access to the real subject. Every request is performed via a 'surrogate' object called proxy.

```java
package com.jaypeesoft.dp.client;

import com.jaypeesoft.dp.proxy.ProxyServer;
import com.jaypeesoft.dp.proxy.Server;

public class ProxyClient {
  public static void main(String[] args) {

    /* Client can access only the proxy server*/
    Server server = new ProxyServer();

    /* Client works with the same interface */
    server.authenticate();
    server.get();
    server.post();
    server.put();
    server.delete();
    server.logout();

  }
}
```

Output

```
Logged into the Real Server
GET command executed
POST command executed
PUT command executed
DELETE command executed
Logged out from the Real Server
```

#### Benefits

A proxy can mask the life-cycle and state of a volatile resource from its client.
Easy to manage the access to the real subject.
Proxies can help in creating objects on-demand.

#### Drawbacks

Performance may be impacted due to the extra level of indirection.

#### Real World Examples

Agents with Power of Attorney
Representatives/Brokers

#### Software Examples

SSH client

#### Java SDK Examples

java.lang.reflect.Proxy
java.rmi.\*
javax.ejb.EJB
javax.inject.Inject
javax.persistence.PersistenceContext

## Resources

http://web.archive.org/web/20150527185017/https://msdn.microsoft.com/en-us/magazine/cc188707.aspx